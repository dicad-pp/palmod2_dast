CREATE TABLE `CMORvar_hist` (
   `flag` integer check(flag=1 or flag=0 or flag=2),
   `changes` VARCHAR(8000),
   `curruid` VARCHAR(255),
   `cv` varchar(255),
   `dv` varchar(255),
   `uid` varchar(255),
   `label` VARCHAR(100),
   `miptable` varchar(20),
   `title` VARCHAR(255),
    primary key (uid)
);

CREATE TABLE CMORvar_Mapping_MODEL_delete (
   `recipe` VARCHAR( 255 ),
   `modvarname` VARCHAR(500),
   `standardname` VARCHAR(255),
   `shortname` VARCHAR(100),
   `miptable` varchar(20),
   `cell_methods` varchar(100),
   `modvarcode` varchar(500),
   `modvarunits` VARCHAR(20),
   `comment` varchar( 511 ),
   `note` varchar( 511 ),
   `username` varchar( 100 ),
   `changed` timestamp,
   `frequency` varchar( 100 ),
   `chardim` varchar( 100 ),
   `zaxis` varchar ( 255 ),
   `positive` string check(positive="up" or positive="down" or positive=NULL or positive=0),
   `dec_check` varchar ( 50 ),
   `availability` integer check(availability=-1 or availability=1 or availability=NULL or availability=0),
   `uid` varchar(255),
   `id` varchar(255),
   `tid` varchar(255),
   primary key (id, tid)
);

CREATE TABLE `CMORvar_Mapping_MODEL` (
   `recipe` VARCHAR( 255 ),
   `flag` integer check(flag=1 or flag=0 or flag=2 or flag=3),
   `flagby` varchar( 100 ),
   `flagt` timestamp,
   `flagh` integer check(flagh=1 or flagh=0),
   `flaguid`varchar(255),
   `modvarname` VARCHAR(500),
   `standardname` VARCHAR(255),
   `shortname` VARCHAR(100),
   `miptable` varchar(20),
   `cell_methods` varchar(100),
   `modvarcode` varchar(500),
   `modvarunits` VARCHAR(20),
   `comment` varchar( 511 ),
   `note` varchar( 511 ),
   `username` varchar( 100 ),
   `changed` timestamp,
   `frequency` varchar( 100 ),
   `chardim` varchar( 100 ),
   `zaxis` varchar ( 255 ),
   `positive` string check(positive="up" or positive="down" or positive=NULL or positive=0),
   `dec_check` varchar ( 50 ),
   `availability` integer check(availability=-1 or availability=1 or availability=NULL or availability=0),
   `uid`varchar(255),
   FOREIGN KEY(uid) references CMORvar(uid),
   primary key (uid)
);

CREATE TABLE CMORvar_Mapping_MODEL_history (
   `recipe` VARCHAR( 255 ),
   `modvarname` VARCHAR(500),
   `standardname` VARCHAR(255),
   `shortname` VARCHAR(100),
   `miptable` varchar(20),
   `cell_methods` varchar(100),
   `modvarcode` varchar(500),
   `modvarunits` VARCHAR(20),
   `comment` varchar( 511 ),
   `note` varchar( 511 ),
   `username` varchar( 100 ),
   `changed` timestamp,
   `frequency` varchar( 100 ),
   `chardim` varchar( 100 ),
   `zaxis` varchar ( 255 ),
   `positive` string check(positive="up" or positive="down" or positive=NULL or positive=0),
   `dec_check` varchar ( 50 ),
   `availability` integer check(availability=-1 or availability=1 or availability=NULL or availability=0),
   `uid` varchar(255),
   `id` varchar(255),
   primary key (id)
);
