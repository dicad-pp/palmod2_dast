#!/usr/bin/python
# -*- coding: utf-8 -*-

# Martin Schupfner, DKRZ 2017

#######################
# Idea:
# 4 possibilities:
##################
#1# Variables altered in new version
######
# - CMORvar_Mapping_Model: add flagh=1
# - CMORvar_hist:          add Info about changes
#  -> show in red, as long as not edited again!
###
#2# Variables change UID (manual mapping needed!)
######
# - CMORvar_Mapping_Model: add flagh=1
#                          delete old UID entry and
#                          move its info to new UID entry
# - CMORvar_Mapping_Model_history: delete old UID entries and
#                                  move its info to new UID entry
# - CMORvar_hist:          add Info about changes and changed UID
#  -> show in red as long as not edited again
### 
#3# Variables entirely removed
######
# - CMORvar_hist:          add flag=1
# - CMORvar_Mapping_Model_del: add info from CMORvar_Mapping_Model and CMORvar_Mapping_Model_history (ID=CMORvar_hist->ID)
#  -> show no info for users about deleted variables
###
#4# New Variables added with new version
######
# - CMORvar_hist:          add flag=2
#  -> show in yellow, as long as no entry in CMORvar_Mapping_Model
########################



#############
# Imports
#############

from cStringIO import StringIO
import sys, json, os, io
import dill, shutil, datetime
import collections as c
import sqlite3 as lite

#Redirect stdout (not needed here)
#old_stdout = sys.stdout
#sys.stdout = mystdout = StringIO()



############
# Functions
############
def jsonwrite(jarg, outfile):
        """
        Write output to file read by webserver
        """
        with io.open(outfile, 'w', encoding='utf-8') as f:
                f.write(unicode(json.dumps(jarg, ensure_ascii=False)))

def jsonappend(jarg, outfile):
        """
        Append output to file read by webserver
        """
        if os.path.isfile(outfile):
            with io.open(outfile, 'r', encoding='utf-8') as f:
                data=json.load(f)
                data.update(jarg)
                jarg=data
        with io.open(outfile, 'w', encoding='utf-8') as f:
            f.write(unicode(json.dumps(jarg, ensure_ascii=False)))

def jsonread(infile):
    """
    Read json file
    """
    if os.path.isfile(infile):
        with io.open(infile, 'r', encoding='utf-8') as f:
            content=json.load(f)
        return content
    else:
        return {}

def correct_none(invar):
    """
    In: Invar
    Out: Returns empty string if invar is of Type NoneType.
         Else returns invar.
    """

    if invar is None: return ""
    else: return str(invar).strip()
           

 
###########
# Defaults
###########
#Produce Mapping between two versions:
vold="01.00.03"
vnew="01.00.04"
fixedmapping="False" #"True" #"True"

models=["mpiesm12echam6", "mpiesm12mpiom", "mpiesm12jsbach", "mpiesm12vilma", "mpiesm12ebm", "mpiesm12mpism"]

# Read CMORvar_hist DB
db="palmod2db.sqlite"
backupdir="BackupDB"
nowdate="_".join(str(datetime.datetime.now()).split(" "))
phptime=int((datetime.datetime.now() - datetime.datetime(1970,1,1)).total_seconds())

# Check readability and SQLITE
print "Using SQLITE in version", lite.sqlite_version
print "\nLoading Database ..."
try:
    con = lite.connect(db)
    cur = con.cursor()
    cur.execute('SELECT SQLITE_VERSION()')
    data=cur.fetchone()
    print "SQLite version of Database: %s" % data
except lite.Error, e:
    print "Error: %s" % e.args[0]
    sys.exit(1)

# Read variable info from DB
try:
    cur.execute("select * from CMORvar")
    dqcol=cur.fetchall()
    cur.execute("select dreq_version from Dreq_Version")
    dreqversion=cur.fetchone()
except lite.Error, e:
    print "Error: %s" % e.args[0]
    sys.exit(1)



#If comparing two specified versions (Dills have to be present!!)
if fixedmapping!="True":

    MDO="./MappingDic_latest.dill"
    MDN="./MappingDic_"+str(dreqversion[0])+".dill"
    VM="./VersionMapping.json"
    VO=""
    VN=str(dreqversion[0])
    old="False"
    
else:
    MDO="./MappingDic_"+vold+".dill"
    MDN="./MappingDic_"+vnew+".dill"
    VM="./VersionMapping.json"
    VO=vold
    VN=vnew 
    old="True"

answers=list()
errors=list()
tasks=list()
varchanges=list()
varmappings=list()
vardel=list()
varcre=list()



##################
# Old Pickles
##################
if os.path.isfile(MDO):
    old="True"
    with open(MDO, "rb") as f:
        MappingDicOld=dill.load(f)
    VO=MappingDicOld["v"]
else:
    if fixedmapping=="True":
        print "File not found!"
        sys.exit(1)

if VO==VN:
	print "Same version. No changes."
	sys.exit(0)


        
####################
# New Pickles
####################
#A Dic of Dics (each variable has a dic of its attributes)
if fixedmapping!="True":
    MappingDic={}
    MappingDic["v"]=VN
    for item in dqcol:
            Dic={}
            Dic['uid'] = str(item[0])
            Dic['label'] = str(item[2])
            Dic['title'] = str(item[3])
            Dic['sn']    = str(item[4])
            Dic['cm']    = str(item[23])
            Dic['cms']   = str(item[24])
            Dic['dims']  = str(item[26])
            Dic['positive'] = str(item[9])
            Dic['mipTable'] = str(item[5])
            Dic['units'] = str(item[8])
            Dic['vproc'] = str(item[18])
            Dic['vdesc'] = str(item[17])
            Dic['comment']=str(item[19])
            Dic['realm']=str(item[20])
            Dic['description'] = str(item[15])
            Dic['processing']  = str(item[16])
            Dic['frequency']   = str(item[7])
            Dic['structure']   = str(item[25])

            MappingDic[str(item[0])]=Dic
else:
    if os.path.isfile(MDN):
        with open(MDN, "rb") as f:
            MappingDic=dill.load(f)
    else:
        print "File not found!"
        sys.exit(1)
    
    
    
    
if fixedmapping!="True":
    #if old = False 
    with open (MDN, "wb") as f:
    		dill.dump(MappingDic, f)

    with open (MDO, "wb") as f:
  		dill.dump(MappingDic, f)
  		
if old=="False":
	#Write JSON
        print "No old version dill found to compare."
	sys.exit(0)


######################
# Compare Pickles
######################
#Create DB Backup
shutil.copyfile(db, "/".join([backupdir, db+"_"+nowdate]))

# Create Changes (CMORvar_hist entries)
#    if olduid in CMORvar_hist as curruid: append to entry
#    else: create new entry VN_uid
# Append new keys to DicADD and removed keys to DicRM
# Append altered variables (only if the main monitored attributes change!) to DicChanged
# Append all matched/mapped keys to DicMapped (all old variables that have been mapped successfully to a new one)
RemovedKeys=list() #List of all old UIDs of deleted variables
DicRM=list()
DicADD=list()
DicMapped=list()
DicChanged={}

rmcount=0
complement=0
addcount=0
mapcount=0
uccount=0
for key in MappingDicOld.keys():
    #print key
    if key not in MappingDic.keys():
        DicRM.append(key)
        rmcount+=1
        #print "----",rmcount,"removed"
    else:
        #print "-else"
        complement+=1
        EntryDic={}
        if MappingDicOld[key]!=MappingDic[key] and key!="v":
		#print "\nReplaced  %s -> %s" %(MappingDicOld[key]['label'],MappingDic[key]['label'])
		#Will see if following filter stands the test, mb add filter for frequency as well and filter more strict:
		if MappingDicOld[key]['label']!=MappingDic[key]['label'] and MappingDicOld[key]['title']!=MappingDic[key]['title'] and MappingDicOld[key]['sn']!=MappingDic[key]['sn'] and MappingDicOld[key]['mipTable']!=MappingDic[key]['mipTable']:
                    #Assuming that this key is now occupied by a different variable
                    # -> adding key to the mapping queue    
                    #print "  -> Adding to Mapping-queue"
                    DicADD.append(key)
                    DicRM.append(key)
                    rmcount+=1
                    addcount+=1
                    #print "---",addcount ,"uid altered"
                else:
                    mapcount+=1
                    #print "--",mapcount,"mapped"
                    DicMapped.append(key)
                    for kex in MappingDicOld[key]:
			if MappingDic[key][kex]!=MappingDicOld[key][kex]:
                            EntryDic[kex]=MappingDicOld[key][kex].replace(";",",")+";"+MappingDic[key][kex].replace(";",",")
                            #print kex+":", MappingDicOld[key][kex], "!=", MappingDic[key][kex]
        else:
                DicMapped.append(key)
                uccount+=1
                
        #Append Entry Dic to Hist Dict
        if EntryDic!={}: 
            #print EntryDic
            DicChanged[key]=EntryDic

#Summary
print
print addcount, "UIDs used for a different variable now."
#New Keys
for key in MappingDic.keys():
    if key not in MappingDicOld.keys() and key not in DicADD:
        DicADD.append(key)
        addcount+=1
        
print len(MappingDic)-1, "Variables in new version."
print len(MappingDicOld)-1,"Variables in old version."
print uccount, "unchanged variables."
print len(DicMapped)-uccount, "changed variables."
print len(DicADD), "new added Variables (or altered UID)."
print rmcount, "removed Variables (or altered UID)."


#Check
for key in MappingDic.keys():
        if key not in DicMapped and key not in DicADD and key!="v":
                print "Error: key (add) not found", key
                sys.exit(1)
for key in MappingDicOld.keys():
        if key not in DicMapped and key not in DicRM and key !="v":
                print "Error: key (rm) not found", key
                sys.exit(1)     
for key in DicMapped:
        if key not in MappingDic or key not in MappingDicOld:
                print "Error: key mapped but not found:", key
                sys.exit(1)
for key in DicMapped:
        if key in DicADD or key in DicRM:
                print "Error: key mapped but also new/rm?!", key
                sys.exit(1)


                
#########################
# 1 # Altered Variables #
#########################
# Set flagh in Mapping DB for every changed uid to '1'
counter=1
for key, dic in DicChanged.items():
        for model in models:
                try:
                        cur.execute("Update CMORVAR_Mapping_"+model+" SET flagh=1 WHERE uid=? and username is not NULL", [key])
                except lite.Error, e:
                        print "Error: %s" % e.args[0]
                        #finally restore backup, set latest to VO
                        if fixedmapping!="True":
                                print "Restoring Version Info..."
                                shutil.copyfile("./MappingDic_"+VO+".dill", "./MappingDic_latest.dill")
                        print "Restoring Database backup..."
                        shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)   
                        sys.exit(1)                        
        counter+=1
        
#Select old entry in DB, if present and append to it       
print "\n____________________\nAltered Variables (%i):" % (counter-1) 
counter=1
for key, dic in DicChanged.items():
    try:
        #changed/removed 'and flag!=2' as it caused double entries when previously introduced entries receive an update in the consecutive version! Will see on next execution what flag!=2 was good for again -.-
        cur.execute("select uid, curruid, changes, flag, cv from CMORvar_hist where curruid=? and flag!=1", [key])
        data=cur.fetchall() 
    except lite.Error, e:
        print "Error: %s" % e.args[0]
        #finally restore backup, set latest to VO
        if fixedmapping!="True":
            print "Restoring Version Info..."
            shutil.copyfile("./MappingDic_"+VO+".dill", "./MappingDic_latest.dill")
        print "Restoring Database backup..."
        shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)   
        sys.exit(1)
    
    if len(data)>1:
        print "Error: Error found in DreqPy Changes history DB:"
        for entry in data: print entry
        sys.exit(1)
    elif len(data)==1:
        print counter, key, dic
        #Append new version changes to changelog
        try:
            olddic=json.loads(data[0][2])
            olddic[VN]=dic
        except TypeError:
            olddic={}
            olddic[VN]=dic
        cv=data[0][4]
        try:
            cur.execute("INSERT OR REPLACE INTO CMORvar_hist (uid, curruid, changes, flag, cv, label, title, miptable) VALUES (?,?,?,?,?,?,?,?)", [data[0][0], key, json.dumps(olddic), 0, cv, MappingDic[key]['label'], MappingDic[key]['title'], MappingDic[key]['mipTable']])            
        except lite.Error, e:
            print "Error: %s" % e.args[0]
            #finally restore backup, set latest to VO
            if fixedmapping!="True":
                print "Restoring Version Info..."
                shutil.copyfile("./MappingDic_"+VO+".dill", "./MappingDic_latest.dill")
            print "Restoring Database backup..."
            shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)   
            sys.exit(1)        
    else:
        print counter,key,dic
        try:
            uid=VN+"_"+key
            olddic={}
            olddic[VN]=dic
            cur.execute("INSERT INTO CMORvar_hist (uid, curruid, changes, flag, label, title, miptable) VALUES (?,?,?,?,?,?,?)", [uid, key, json.dumps(olddic), 0, MappingDic[key]['label'], MappingDic[key]['title'], MappingDic[key]['mipTable']])               
        except lite.Error, e:
            print "Error: %s" % e.args[0]
            #finally restore backup, set latest to VO
            if fixedmapping!="True":
                print "Restoring Version Info..."
                shutil.copyfile("./MappingDic_"+VO+".dill", "./MappingDic_latest.dill")
            print "Restoring Database backup..."
            shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)
            sys.exit(1)
    counter+=1
con.commit()



#########################
# 2 # New UIDs Part 1/2 #
#########################
DicMappedNew=DicMapped
mappingfile="False"
# Create Mapping Suggestions (Map variables that changed its uid!)
if os.path.isfile(VO+"-"+VN+"_Mapping.json"):
        mappingfile="True"
        choices=jsonread(VO+"-"+VN+"_Mapping.json")        
else:
        choices={}
print "\n__________________\nKeys added:",len(DicADD)
for key in DicRM:
    if key in choices.keys():
            inv_choices = {v: k for k, v in choices.iteritems()}
            DicMapped.append(key)
            DicMappedNew.append(choices[key])
            print "Skipping", key, "(previously stored Mapping found:",choices[key],") ..."
            continue
    # Display suggestions for not yet mapped Variables:   
    sug=list()
    suggreat=list()
    sugless=list()
    sugautomap=list()
    keysnot=["UID", "uid", "v"]
    for i in DicADD:        
        if i not in DicMappedNew and i != "v" and i not in choices.values():
            automap="True"
            for kez in MappingDicOld[key].keys():
                if kez not in keysnot:
                    if MappingDicOld[key][kez]!=MappingDic[i][kez]: automap="False"
            if automap=="True":
                sugautomap.append(i)
            if MappingDicOld[key]['label']==MappingDic[i]['label'] and MappingDicOld[key]['title']==MappingDic[i]['title'] and MappingDicOld[key]['sn']==MappingDic[i]['sn'] and (MappingDicOld[key]['mipTable']==MappingDic[i]['mipTable'] or MappingDicOld[key]['frequency']==MappingDic[i]['frequency']):
               suggreat.append(i)
               continue
            if MappingDicOld[key]['label']==MappingDic[i]['label'] and MappingDicOld[key]['title']==MappingDic[i]['title'] and MappingDicOld[key]['sn']==MappingDic[i]['sn']:
               sug.append(i)
               continue
            if MappingDicOld[key]['label']==MappingDic[i]['label'] and MappingDicOld[key]['sn']==MappingDic[i]['sn']:
               sug.append(i)
               continue
            if MappingDicOld[key]['title']==MappingDic[i]['title'] and MappingDicOld[key]['sn']==MappingDic[i]['sn']:
               sugless.append(i)
               continue
            if MappingDicOld[key]['label']==MappingDic[i]['label'] or MappingDicOld[key]['title']==MappingDic[i]['title'] or MappingDicOld[key]['sn']==MappingDic[i]['sn']:
               sugless.append(i)
               continue

    if len(sugautomap)==1:
        DicMapped.append(key)
        DicMappedNew.append(sugautomap[0])
        choices[key]=sugautomap[0]
        print "Automatically mapped", key, "(old) to", sugautomap[0], "(new)"
        continue

    sug=suggreat+sug+sugless
    if len(sug)==0:
            continue
    print "-----------------------\nMapping Suggestions for:\n\033[1m%s\033[0m (%s, %s):" % (MappingDicOld[key]['label'], MappingDicOld[key]['title'], MappingDicOld[key]['sn'])
    for a, b in MappingDicOld[key].items():
        if a not in ['label', 'title', 'sn']: print "   \033[1m%-13s\033[0m: %s" %(a, b)

    for ix in range(0, len(sug)):
        kex=sug[ix]
        prelabel=""
        pretitle=""
        presn=""
        postlabel=""
        posttitle=""
        postsn=""
        if MappingDicOld[key]['label'] != MappingDic[kex]['label']:
            prelabel="\033[93m\033[1m"
            postlabel="\033[0m"
        if MappingDicOld[key]['title'] != MappingDic[kex]['title']:
            pretitle="\033[93m\033[1m"
            posttitle="\033[0m"
        if MappingDicOld[key]['sn'] != MappingDic[kex]['sn']:
            presn="\033[93m\033[1m"
            postsn="\033[0m"

        print "\033[1m\033[92m%i\033[0m:  \033[1m%s%s%s\033[0m (%s%s%s, %s%s%s):" % (ix+1, prelabel, MappingDic[kex]['label'], postlabel, pretitle, MappingDic[kex]['title'], posttitle, presn, MappingDic[kex]['sn'],postsn)
        for a, b in MappingDic[kex].items():
            if a not in ['label', 'title', 'sn']:
                if  MappingDicOld[key][a] != MappingDic[kex][a]:
                    print "   \033[1m\033[93m%-13s:  %s\033[0m" %(a, b)
                else:
                    print "   \033[1m%-13s\033[0m:  %s" %(a, b)

    print "\033[1m\033[92m%i\033[0m: None of the above fits. This variable apparently has been removed!" % 0

    #User Input
    choice="False"
    ok="False"
    while ok=="False":
        choice=raw_input("\nMake your choice:")
        try:
            if int(choice) in range(0, len(sug)+1):
                if int(choice)!=0:
                    choices[key]=sug[int(choice)-1]
                    DicMapped.append(key)
                    DicMappedNew.append(sug[int(choice)-1])
                ok="True"
        except ValueError:
            continue

jsonwrite(choices, VO+"-"+VN+"_Mapping.json")

print "\nYour Mapping:"
#choices[keyold]=keynew
counter=1
for a, b in choices.items():
        if b not in DicMappedNew:
                print "ERROR:",b, "not yet mapped but chosen?!"
                sys.exit(1)
        if a not in DicMapped:
                print "ERROR:",a,"not yet mapped but chosen?!"
                sys.exit(1)

for a, b in choices.items():  
    print "\n#%i Variable (old) %s (->%s): %s (-> %s):" % (counter, MappingDicOld[a]['label'], MappingDic[b]['label'], MappingDicOld[a]['title'], MappingDic[b]['title'])
    for c,d in MappingDicOld[a].items():
        if c not in ['label', 'title', 'sn']: print "   ",c, ":", d, "->", MappingDic[b][c]
    counter+=1
    
#Check
for key in choices.keys():
        if key not in DicMapped:
                print "Error: Mapped key still in RM-Dic", key
                sys.exit(1)
        if choices[key] not in DicMappedNew:
                print "Error: Mapped key still in DicADD", key
                sys.exit(1)


                
#########################
# 3 # Deleted Variables #
#########################
#Write latest entry to history Database and move entire entries to Mapping delete database with ID=hist_ID
counter=1
print "\n_________________\nKeys no longer present:"
for i in DicRM:
        if i not in DicMapped:
                RemovedKeys.append(i)
                print counter, ": ", i, MappingDicOld[i]["label"], MappingDicOld[i]["title"], "(", MappingDicOld[i]["mipTable"], ")"               
                counter+=1
                for model in models:                        
                        try:
                                cur.execute("INSERT INTO CMORvar_Mapping_"+model+"_history (recipe, modvarname, standardname, shortname, miptable, cell_methods, modvarcode, modvarunits, comment, note, username, changed, frequency, chardim, positive, availability, uid, id, zaxis) SELECT cm.recipe, cm.modvarname, cm.standardname, cm.shortname, cm.miptable, cm.cell_methods, cm.modvarcode, cm.modvarunits, cm.comment, cm.note, cm.username, cm.changed, cm.frequency, cm.chardim, cm.positive, cm.availability, cm.uid, ?, cm.zaxis FROM CMORVAR_Mapping_"+model+" cm WHERE cm.uid=?", [str(i)+str(phptime), str(i)])
                        except lite.Error, e:
                                print "Error: %s" % e.args[0]
                                #finally restore backup, set latest to VO
                                if fixedmapping!="True":
                                        print "Restoring Version Info..."
                                        shutil.copyfile("./MappingDic_"+VO+".dill", "./MappingDic_latest.dill")
                                print "Restoring Database backup..."
                                shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)
                                sys.exit(1)       
                con.commit()    
                                
                #Update CMORvar_hist DB		
		EntryDic={}
		for kex in MappingDicOld[i]: 
			if not  MappingDicOld[i][kex]=="":  			
           			EntryDic[kex]=MappingDicOld[i][kex].replace(";",",")+";"
                try:
                        cur.execute("select uid, curruid, changes, flag from CMORvar_hist where curruid=? and flag!=1", [i])
                        data=cur.fetchall()
                        if len(data)==1:
                                newdic={}
			        olddic=json.loads(data[0][2])
                                if type(newdic)==type(olddic):
					olddic[VN]=EntryDic
                                else: olddic={VN:EntryDic}
                                cur.execute("Update CMORvar_hist SET flag=1, dv=?, changes=?, label=?, title=?, miptable=? where curruid=? and flag!=1", [VN, json.dumps(olddic), MappingDicOld[i]["label"], MappingDicOld[i]["title"], MappingDicOld[i]["mipTable"], i])           
                        elif len(data)==0:
                                olddic={VN:EntryDic}
                                cur.execute("INSERT INTO CMORvar_hist (uid, curruid, flag, dv, changes, label, title, miptable) VALUES (?,?,?,?,?,?,?,?)", [VN+"_"+i, i, 1, VN, json.dumps(olddic), MappingDicOld[i]["label"], MappingDicOld[i]["title"], MappingDicOld[i]["mipTable"]])                     
                        else:
                           print "Error: Error found in DreqPy Changes history DB:"
                           for entry in data: print entry
                           sys.exit(1)

                except lite.Error, e:
                        print "Error: %s" % e.args[0]
                        #finally restore backup, set latest to VO
                        if fixedmapping!="True":
                            print "Restoring Version Info..."
                            shutil.copyfile("./MappingDic_"+VO+".dill", "./MappingDic_latest.dill")
                        print "Restoring Database backup..."
                        shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)
                        sys.exit(1)              
                con.commit()

                for model in models:                        
                        try:
                                cur.execute("INSERT INTO CMORvar_Mapping_"+model+"_delete (recipe, modvarname, standardname, shortname, miptable, cell_methods, modvarcode, modvarunits, comment, note, username, changed, frequency, chardim, positive, availability, uid, id, tid, zaxis) SELECT recipe, modvarname, standardname, shortname, miptable, cell_methods, modvarcode, modvarunits, comment, note, username, changed, frequency, chardim, positive, availability, uid, id, ?, zaxis FROM CMORVAR_Mapping_"+model+"_history WHERE uid=?",[VN+"_"+str(i)+"_"+str(phptime), str(i)] )
                                cur.execute("DELETE FROM CMORVAR_Mapping_"+model+"_history WHERE uid=?", [str(i)])
                                cur.execute("DELETE FROM CMORVAR_Mapping_"+model+" WHERE uid=?",[str(i)])
                        except lite.Error, e:
                                print "Error: %s" % e.args[0]
                                #finally restore backup, set latest to VO
                                if fixedmapping!="True":
                                        print "Restoring Version Info..."
                                        shutil.copyfile("./MappingDic_"+VO+".dill", "./MappingDic_latest.dill")
                                print "Restoring Database backup..."
                                shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)
                                sys.exit(1)       
                con.commit()
                
                

########################
# 4 # Added Variables  #
########################           

print "\n_________________\nKeys added in the newest version", VN,":"
counter=1
try:
                        cur.execute("Update CMORvar_hist SET flag=0 where flag=2")
except lite.Error, e:
                        print "Error: %s" % e.args[0]
                        #finally restore backup, set latest to VO
                        if fixedmapping!="True":
                            print "Restoring Version Info..."
                            shutil.copyfile("./MappingDic_"+VO+".dill", "./MappingDic_latest.dill")
                        print "Restoring Database backup..."
                        shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)
                        sys.exit(1)

for i in DicADD:
        if i not in DicMappedNew:
                print counter, ": ", i, MappingDic[i]["label"], MappingDic[i]["title"],"(", MappingDic[i]["mipTable"],")"
                counter+=1
                EntryDic={}
		for kex in MappingDic[i]: 
			if not  MappingDic[i][kex]=="":  			
           			EntryDic[kex]=";"+MappingDic[i][kex].replace(";",",")
                try:
                        cur.execute("select uid, curruid, changes, flag from CMORvar_hist where curruid=? and flag!=1", [i])
                        data=cur.fetchall()
                        if len(data)!=0:
                           print "Error: Error found in DreqPy Changes history DB:"
                           for entry in data: print entry
                           sys.exit(1)
                        cur.execute("INSERT INTO CMORvar_hist (uid, curruid, changes, flag, cv, label, title, miptable) VALUES (?,?,?,?,?,?,?,?)", [VN+"_"+i, i, json.dumps({VN:EntryDic}), 2, VN, MappingDic[i]["label"],MappingDic[i]["title"],MappingDic[i]["mipTable"]])                       
                except lite.Error, e:
                        print "Error: %s" % e.args[0]
                        print "ERROR for UID: "+VN+"_"+i
                        #print [VN+"_"+i, i, json.dumps({VN:EntryDic}), 2, VN, MappingDic[i]["label"],MappingDic[i]["title"],MappingDic[i]["mipTable"]]
                        #finally restore backup, set latest to VO
                        if fixedmapping!="True":
                            print "Restoring Version Info..."
                            shutil.copyfile("./MappingDic_"+VO+".dill", "./MappingDic_latest.dill")
                        print "Restoring Database backup..."
                        shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)
                        sys.exit(1)
con.commit()
                

                
#########################
# 2 # New UIDs Part 2/2 #
#########################                
#Alter entries in history DB

#Write Mapping to DB:
print "\n-------------------------\nUpdating Database with manually mapped Variables..."
#Delete old history entries and rewrite them with updated UID and ID
for model in models:
        for keyold, keynew in choices.items():
                try:
                        cur.execute("SELECT cm.recipe, cm.modvarname, cm.standardname, cm.shortname, cm.miptable, cm.cell_methods, cm.modvarcode, cm.modvarunits, cm.comment, cm.note, cm.username, cm.changed, cm.frequency, cm.chardim, cm.positive, cm.availability,uid, id, zaxis FROM CMORVAR_Mapping_"+model+"_history cm WHERE cm.uid=?", [keyold])                        
                        data=cur.fetchall()
                        cur.execute("delete from CMORvar_Mapping_"+model+"_history where uid=?", [keyold])
                        con.commit()
                        if len(data)>0:
                                for history_entry in data:
                                        cur.execute("INSERT INTO CMORvar_Mapping_"+model+"_history (recipe, modvarname, standardname, shortname, miptable, cell_methods, modvarcode, modvarunits, comment, note, username, changed, frequency, chardim, positive, availability, uid, id, zaxis) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", list(history_entry[:-3])+[keynew]+[keynew+history_entry[-2].replace(keyold, "")]+[history_entry[-1]])
                                        con.commit()
                                        
                except lite.Error, e:
                        print "Error: %s" % e.args[0]
                        #finally restore backup, set latest to VO
                        if fixedmapping!="True":
                                print "Restoring Version Info..."
                                shutil.copyfile("./MappingDic_"+VO+".dill", "./MappingDic_latest.dill")
                        print "Restoring Database backup..."
                        shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)
                        sys.exit(1)

        #Delete Mapping Entry and rewrite with new UID and updated attribute-values
        for keyold,keynew  in choices.items():
                #Get old entry
                try:
                        cur.execute("select uid, recipe, modvarname, modvarcode, modvarunits, comment, note, username, changed, availability, positive from CMORvar_Mapping_"+model+" where uid=?", [keyold])
                        data=cur.fetchall() 
                        cur.execute("delete from CMORvar_Mapping_"+model+" where uid=?", [keyold])
                        cur.execute("select cmt, cdim from CMORvar where uid=?", [keynew])
                        cmtx=cur.fetchall()
                except lite.Error, e:
                        print "Error: %s" % e.args[0]
                        #finally restore backup, set latest to VO
                        if fixedmapping!="True":
                            print "Restoring Version Info..."
                            shutil.copyfile("./MappingDic_"+VO+".dill", "./MappingDic_latest.dill")
                        print "Restoring Database backup..."
                        shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)
                        sys.exit(1)               
                cmt=cmtx[0][0]       
                cdim=cmtx[0][1]
                if len(data)>0:
                        if len(data)>1:
                                print "Error: Too many variables found for uid", keyold
                                print data
                                #finally restore backup, set latest to VO
                                if fixedmapping!="True":
                                    print "Restoring Version Info..."
                                    shutil.copyfile("./MappingDic_"+VO+".dill", "./MappingDic_latest.dill")
                                print "Restoring Database backup..."
                                shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)
                                sys.exit(1)
                        else:
                                data=data[0]
                        try:
                                cur.execute("INSERT OR REPLACE INTO CMORvar_Mapping_"+model+" (recipe, flag, flagby, flagt, flagh, modvarname, standardname, shortname, miptable, cell_methods, modvarcode, modvarunits, comment, note, username, changed, frequency, chardim, positive, availability, uid) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [data[1], 0, '', 0, 1, data[2], MappingDic[keynew]['sn'], MappingDic[keynew]['label'], MappingDic[keynew]['mipTable'], cmt, data[3], data[4], data[5], data[6], data[7], data[8], MappingDic[keynew]['frequency'], cdim, data[10], data[9], keynew])
                                data=cur.fetchall() 
                        except lite.Error, e:
                                print "Error: %s" % e.args[0]
                                #finally restore backup, set latest to VO
                                if fixedmapping!="True":
                                    print "Restoring Version Info..."
                                    shutil.copyfile("./MappingDic_"+VO+".dill", "./MappingDic_latest.dill")
                                print "Restoring Database backup..."
                                shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)
                                sys.exit(1)
                                
#Add Info about changed ids and attributes to hist db
for keyold, keynew in choices.items():
    cv=None
    EntryDic={}
    for kex in MappingDicOld[keyold]:
       if MappingDic[keynew][kex]!=MappingDicOld[keyold][kex]:
           EntryDic[kex]=MappingDicOld[keyold][kex].replace(";",",")+";"+MappingDic[keynew][kex].replace(";",",") 
    try:
        cur.execute("select uid, curruid, changes, flag, cv from CMORvar_hist where curruid=? and flag!=1", [keyold])
        data=cur.fetchall() 
    except lite.Error, e:
        print "Error: %s" % e.args[0]
        #finally restore backup, set latest to VO
        if fixedmapping!="True":
            print "Restoring Version Info..."
            shutil.copyfile("./MappingDic_"+VO+".dill", "./MappingDic_latest.dill")
        print "Restoring Database backup..."
        shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)   
        sys.exit(1)
    
    if len(data)>1:
        print "Error: Error found in DreqPy Changes history DB:"
        print data
        for entry in data: print entry
        sys.exit(1)
    elif len(data)==1:
        #Append new version changes to changelog
        try:
            olddic=json.loads(data[0][2])
        #Type error resulting that json.loads argument is None type
        # it is None type because variables that have cv defined but not changes defined
        # have NULL as changes!
        except TypeError:
            olddic={}
        cv=data[0][4]
        newdic={}
        if type(olddic)==type(newdic):
                olddic[VN]=EntryDic
                #{"UID":keyold+";"+keynew}
        else:
                olddic={VN:EntryDic}
                #{VN:{"UID":keyold+";"+keynew}}
        flag=0
        curruid=keynew
        uid=data[0][0]
    else:
        flag=0
        curruid=keynew
        uid=VN+"_"+keynew
        olddic={VN: EntryDic}
        #{VN:{"UID":keyold+";"+keynew}}
    try:
        cur.execute("INSERT OR REPLACE INTO CMORvar_hist (uid, curruid, changes, flag, cv, label, title, miptable) VALUES (?,?,?,?,?,?,?,?)", [uid, curruid, json.dumps(olddic), flag, cv,MappingDic[keynew]["label"],MappingDic[keynew]["title"],MappingDic[keynew]["mipTable"]])        
    except lite.Error, e:
        print "Error: %s" % e.args[0]
        #finally restore backup, set latest to VO
        if fixedmapping!="True":
           print "Restoring Version Info..."
           shutil.copyfile("./MappingDic_"+VO+".dill", "./MappingDic_latest.dill")
        print "Restoring Database backup..."
        shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)   
        sys.exit(1)       
   
con.commit()



####################################
# Copy up to date dreq information
# over CMORvar_Mapping_model tables
####################################
print "Updating CMORvar_Mapping_MODEL tables with up-to-date Dreq attributes..."
for model in models:
    #Select all UIDs that have been mapped for this MODEL
    cur.execute("SELECT uid FROM CMORvar_Mapping_"+model+" where username IS NOT NULL AND username!=''")
    data_uid=cur.fetchall()
    if len(data_uid)==0: continue
    print " ... %s variables for model %s" % (str(len(data_uid)), model)
    
    #For every Variable/UID, select up-to-date info from CMORvar and var tables
    for i in range(0, len(data_uid)):
        uid=data_uid[i][0]            
        cur.execute("SELECT uid,vid,label,mipTable,cmt,zaxis,cdim,frequency,standardname FROM CMORvar WHERE uid=?", [uid])
        data_cm_uid=cur.fetchall()        
           
        if len(data_cm_uid)!=1:
            print "Error: Not exactly one variable found for UID %s" % uid
            sys.exit(1)       
        
        #...and update the mapped information with this up-to-date info  
        try:
            cur.execute("Update CMORvar_Mapping_"+model+" SET shortname=?, miptable=?, cell_methods=?, zaxis=?, chardim=?, frequency=?, standardname=? where uid=?", list(data_cm_uid[0][2::])+[uid])     	   
        except lite.Error, e:
            print "Error: %s" % e.args[0]
            #finally restore backup, set latest to VO
            print "Restoring DB Backup ..."
            shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)
            sys.exit(1)       
        con.commit()    




        
#######################
# Update entries of
# the White-/Blacklist
#######################
#Select all users' white-/blacklists
print "Updating user defined White/-Blacklists..."
try:
        cur.execute("SELECT user,WL,BL FROM WBList")
        data_uid=cur.fetchall()
except lite.Error, e:
        print "Error: %s" % e.args[0]
        #finally restore backup, set latest to VO
        print "Restoring DB Backup ..."
        shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)
        sys.exit(1)
        
print " ... %s users have defined lists" % str(len(data_uid))

for i in range(0, len(data_uid)):
        username=data_uid[i][0]
        userwl=data_uid[i][1]
        userbl=data_uid[i][2]

        wlist=correct_none(userwl).split(",")
        blist=correct_none(userbl).split(",")

        if len(wlist)>0 or len(blist)>0:
        
                userwl=list()
                userbl=list()
        
                for listentry in wlist:
                        if listentry in RemovedKeys: continue
                        elif listentry in choices.keys():
                                userwl.append(choices[listentry])
                        else: userwl.append(listentry)
                userwl=",".join(userwl)
        
                for listentry in blist:
                        if listentry in RemovedKeys: continue
                        elif listentry in choices.keys():
                                userbl.append(choices[listentry])
                        else: userbl.append(listentry)
                userbl=",".join(userbl)
        else:
                userwl=""
                userbl=""
        try:
                cur.execute("update WBList Set WL=?,BL=? WHERE user=?", [username, userwl, userbl])
        except lite.Error, e:
                print "Error: %s" % e.args[0]
                #finally restore backup, set latest to VO
                print "Restoring DB Backup ..."
                shutil.copyfile("/".join([backupdir, db+"_"+nowdate]), db)
                sys.exit(1)        
        con.commit()    




           
print "\nDone."
sys.exit(0)
