Update the palmod2db.sqlite database

- Delete old data from tables
sqlite3 palmod2db.sqlite
> delete from CMORvar;
> delete from experiment; 
> delete from grids;       
> delete from mip;   
> delete from miptable;   
> .quit

- Load new data into the database
 (make sure all json files are listed in the python script)
(python3)
python Read_CMORvars_xlsx_csv_json.py

- Create Update infos
(python2, dill needs to be installed - "pip install dill -t .")
python Version_Mapping_DB.py 


