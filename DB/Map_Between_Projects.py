#!/usr/bin/python
# -*- coding: utf-8 -*-

# Martin Schupfner, DKRZ 2021

#########################################################
# Map mapping information of the same model between
# different projects with different mipTable names,
# variable names, frequencies.
##########################################################

from __future__  import print_function
import sqlite3 as lite
import sys, collections
import datetime
import shutil

def getcode(code):
        """
        Separate parameter/code/variable name from input file.
        123.321|ECHAM.nc,234.321|JSBACH.nc --> 123.321,234.321
        Input: modvarcode / modvarname entry
        Output: code / name
        """
        codestring=list()
        code=code.replace(";",",").split(",")
        for i in code:
                if "|" in i:
                        codestring.append(i.split("|")[1].lstrip("0"))
                else:
                        codestring.append(i.lstrip("0"))
        #if leading zeros are needed for codes
        """
        for i in range(0, len(codestring)):
                try:
                        codestring[i]="{:03}".format(int(codestring[i]))
                except ValueError:
                        pass
        """
        return ",".join(codestring)

def enc(string):
    if string is None:
        return "''"
    elif not (isinstance(string, int) or isinstance(string, float)):
        return "'"+string+"'"
    else:
        return str(string)

def insertTable(con, cur, table, values):
    """
    Insert values into table of active sqlite3 database connection cursor.

    Parameters
    ----------
    con : object
        connection object of sqlite3 database
    cur : object
        cursor of sqlite3 database connection.
    table : string
        existing table of the sqlite3 database.
    values :
        dict of keys and values to be inserted or list of values.

    Returns
    -------
    None.
    """
    keystr=""
    valuestr=""
    if isinstance(values, collections.Mapping):
        for key,value in values.items():
            keystr+=","+key
            valuestr+=","+enc(value) #.replace(",", "\,")
    elif isinstance(values, list):
        #valuestr=",".join([val.replace(",", "\,") for val in values])
        valuestr=",".join([enc(val) for val in values])
        #valuestr=",".join(values)
    if keystr!="": keystr="("+keystr.strip(",")+")"
    valuestr=valuestr.strip(",")
    exestr="INSERT OR REPLACE INTO "+table+" "+keystr+" VALUES ("+valuestr+")"
    #print(exestr)
    try:
        cur.execute(exestr)
    except lite.Error as e:
        print("ERROR (sqlite) %s:" % e.args[0])
        if con: con.close()
        sys.exit(1)

######################################
# Create DB Backup
######################################
dba="cmip6db.sqlite"
dbb="palmod2db.sqlite"
modela="mpiesm12echam6"
modelb="mpiesm12echam6"
modela="mpiesm12mpiom"
modelb=modela
backupdir="BackupDB"
nowdate="_".join(str(datetime.datetime.now()).split(" "))
phptime=int((datetime.datetime.now() - datetime.datetime(1970,1,1)).total_seconds())

print("Creating DB Backup: %s" % backupdir+"/"+dbb+"_"+nowdate)
shutil.copyfile(dbb, "/".join([backupdir, dbb+"_"+nowdate]))

# Short list of keys from mapping info table
dbKeys_light=["recipe",
              "modvarname",
              "standardname",
              "shortname",
              "miptable",
              "modvarcode",
              "modvarunits",
              "comment",
              "note",
              "username",
              "changed",
              "frequency",
              "positive",
              "availability",
              "uid",
              "dec_check"]

# Long list of keys from mapping info table
dbKeys=["recipe",
        "flag",
        "flagby",
        "flagt",
        "flagh",
        "modvarname",
        "standardname",
        "shortname",
        "miptable",
        "cell_methods",
        "modvarcode",
        "modvarunits",
        "comment",
        "note",
        "username",
        "changed",
        "frequency",
        "chardim",
        "positive",
        "availability",
        "zaxis",
        "uid",
        "dec_check"]

# Only mapping info keys from mapping info table
dbKeys_map=["recipe",
            "modvarname",
            "modvarcode",
            "modvarunits",
            "comment",
            "note",
            "dec_check",
            "availability",
            "positive"]

# Only dreq info keys from CMORvar table required for mapping info table
dbKeys_cmv=["uid",
            "label",
            "mipTable",
            "standardname",
            "frequency",
            "cmt",
            "cdim",
            "zaxis"]

#Read DB
print("\nLoading Database A '%s' - Mapping ..." % dba)
try:
    con = lite.connect(dba)
    cur = con.cursor()
    #Select all UIDs that have been mapped for this MODEL
    cur.execute("SELECT "+",".join(dbKeys)+" FROM CMORvar_Mapping_"+modela+" where username IS NOT NULL AND username!=''")#+" AND availability!=-1")
    dataa=cur.fetchall()
    print(" ... read mapping information for %s CMORvars for model %s in DB A" % (str(len(dataa)), modela))
    if len(dataa)==0:
        sys.exit(0)
except lite.Error as e:
    print("ERROR %s:" % e.args[0])
    sys.exit(1)
finally:
    if con: con.close()

# Create dictionary to simplify the processing
dicta={}
for entry in dataa:
    entrydict={dbKeys[i]:entry[i] for i in range(0, len(dbKeys))}
    dicta[entrydict["uid"]]=entrydict

#Read DB
print("\nLoading Database B '%s' - CMORvar ..." % dba)
try:
    con = lite.connect(dbb)
    cur = con.cursor()
    #Select all UIDs that have been mapped for this MODEL
    cur.execute("SELECT "+",".join(dbKeys_cmv)+" FROM CMORvar")
    datab=cur.fetchall()
    print(" ... read %s CMORvars in DB B" % str(len(datab)))
    if len(datab)==0:
        sys.exit(0)
except lite.Error as e:
    print("ERROR %s:" % e.args[0])
    sys.exit(1)
finally:
    if con: con.close()

# Create dictionary to simplify the processing
dictb={}
for entry in datab:
    entrydict={dbKeys_cmv[i]:entry[i] for i in range(0, len(dbKeys_cmv))}
    dictb[entrydict["uid"]]=entrydict

# Now find a good fitting for each variable in dictb
print("Mapping process ...")
mappingBtoA={}
numperfect=0
numalmost=0
nummeh=0
numhmm=0
for keyb in dictb:
    varlist=[keya for keya in dicta if dicta[keya]["miptable"]==dictb[keyb]["mipTable"] and dicta[keya]["shortname"]==dictb[keyb]["label"]]
    if varlist!=[]:
        mappingBtoA[keyb]=varlist[0]
        numperfect+=1
        #print("+",keyb)
    else:
        varlist=[keya for keya in dicta if dicta[keya]["frequency"]==dictb[keyb]["frequency"] and dicta[keya]["shortname"]==dictb[keyb]["label"]]
        if varlist!=[]:
            mappingBtoA[keyb]=varlist[0]
            numalmost+=1
            #print("=",keyb)
        else:
            varlist=[keya for keya in dicta if dictb[keyb]["frequency"] in dicta[keya]["frequency"] and (dicta[keya]["shortname"].startswith(dictb[keyb]["label"]) or dicta[keya]["shortname"].endswith(dictb[keyb]["label"]))]
            if varlist!=[]:
                mappingBtoA[keyb]=varlist[0]
                nummeh+=1
                #print("-",keyb)
            else:
                varlist=[keya for keya in dicta if dicta[keya]["shortname"].startswith(dictb[keyb]["label"]) or dicta[keya]["shortname"].endswith(dictb[keyb]["label"])]
                if varlist!=[]:
                    mappingBtoA[keyb]=varlist[0]
                    numhmm+=1
                    #print("--",keyb)
print(numperfect,"... perfect matches (same miptable, varname)")
print(numalmost,"... almost perfect matches (same frequency, varname)")
print(nummeh,"... 'meh' matches (similar frequency, varname)")
print(numhmm,"... 'hmm' matches (similar varname)")
print("="*10)
print(sum([numperfect,numalmost,nummeh,numhmm]), "of", len(dictb.keys()), "variables with mapped mapping information.\n")

#for key in sorted(mappingBtoA.keys()): print(key,":",mappingBtoA[key],"(",dicta[mappingBtoA[key]]["shortname"], dicta[mappingBtoA[key]]["miptable"],")")
#print(mappingBtoA)

# Write mapping information to DB B
con = lite.connect(dbb)
cur = con.cursor()
for key in sorted(mappingBtoA.keys()):
    mapinfo={}
    # Pure mapping info
    for kex in dbKeys_map:
        mapinfo[kex]=dicta[mappingBtoA[key]][kex]
    mapinfo["uid"]=key
    mapinfo["username"]=dicta[mappingBtoA[key]]["username"]
    mapinfo["changed"]=0
    # Dreq info
    mapinfo["shortname"]=dictb[key]["label"]
    mapinfo["miptable"]=dictb[key]["mipTable"]
    mapinfo["standardname"]=dictb[key]["standardname"]
    mapinfo["frequency"]=dictb[key]["frequency"]
    mapinfo["cell_methods"]=dictb[key]["cmt"]
    mapinfo["chardim"]=dictb[key]["cdim"]
    mapinfo["zaxis"]=dictb[key]["zaxis"]
    # Write
    insertTable(con, cur, "CMORvar_mapping_"+modelb, mapinfo)

con.commit()
if con: con.close()
print("Complete.")
sys.exit()
