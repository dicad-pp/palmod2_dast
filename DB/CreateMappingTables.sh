#! /usr/bin/bash

#for MODEL in mpiesm12echam6 mpiesm12jsbach mpiesm12mpiom mpiesm12ebm mpiesm12mpism mpiesm12vilma
for MODEL in awiesmjsbach awiesmecham6 awiesmpism awiesmfesom1 awiesmfesom2
do
        echo $MODEL
        sed "
                   s;MODEL;${MODEL};g
        " <createmappingdb.sql> CreateMappingModel.sql
        sqlite3 palmod2db.sqlite < CreateMappingModel.sql
        rm CreateMappingModel.sql
done

