# PalMod2_DaSt

The data standard of the PalMod project phase 2, 
holding Controlled Vocabulary, CMOR tables and post processing scripts.

External information in GoogleDrive: [Requested variables excel table](https://docs.google.com/spreadsheets/d/1zQJvdCVgIp0otWtw3rpcbVIxL7Lirmlz/edit?usp=sharing&ouid=111185713730729217006&rtpof=true&sd=true)

# Controlled Vocabulary (CV)

All project-, experiment- and institution/model-related metadata are collected in the [Controlled Vocabulary (CV)](cmor_tables/PalMod2_CV.json). Please check if your institution, model(s) and experiment(s) are already registered and if the information is up-to-date.

## Register a new institution

Please [open an issue](https://gitlab.dkrz.de/dicad-pp/palmod2_dast/-/issues/new) to request the addition of your institution to the CV.

The `institution_id` and the `institution` have to be provided, eg.
```
institution_id: DKRZ
institution: Deutsches Klimarechenzentrum, Hamburg 20146, Germany
```

<a name="RegisterModel"></a>
## Register a new model 

Please [open an issue](https://gitlab.dkrz.de/dicad-pp/palmod2_dast/-/issues/new) to request the addition of your model to the CV.
The following details have to be provided:
- `source_id`, eg. "MPI-ESM1-2-CR"
- associated `institution_id`, eg. "MPI-M"
- a complete and formatted `source` description, eg.:
    ```
    MPI-ESM1.2-CR (2017):
    aerosol: none, prescribed Kinne (2010)
    atmos: ECHAM6.3 (spectral T31; 96 x 48 longitude/latitude; 31 levels; top level 10 hPa)
    atmosChem: none
    land: JSBACH3.20
    landIce: mPISM 0.7 (10 km x 10 km (NH), 15 km x 15 km (SH), 121 levels)
    ocean: MPIOM1.63 (bipolar GR3.0, approximately 300km; 122 x 101 longitude/latitude; 40 levels; top grid cell 0-15 m)
    ocnBgchem: none
    seaIce: unnamed (thermodynamic (Semtner zero-layer) dynamic (Hibler 79) sea ice model)
    solidLand: none/prescribed"
    ```

<a name="RegisterExperiment"></a>
## Register a new experiment

Please [open an issue](https://gitlab.dkrz.de/dicad-pp/palmod2_dast/-/issues/new) to request the addition of the experiment.
The following information has to be provided:
- `experiment_id`, eg. "transient-deglaciation-prescribed-glac1d"
- brief `experiment description`, eg. "transient deglaciation with GLAC-1D ice sheets"
- [required model components](https://github.com/WCRP-CMIP/CMIP6_CVs/blob/master/CMIP6_source_type.json), eg. "AOGCM" or "AGCM"
- [additional allowed model components](https://github.com/WCRP-CMIP/CMIP6_CVs/blob/master/CMIP6_source_type.json), eg. "BGC", "CHEM", "AER"
- if applicable: the `experiment_id` of the `parent experiment`
- optionally provide:
  - experiment length, start and end dates
  - variables requested from the experiment (in case not all variables of the PalMod2 variable list are required)
  - Paper / Website / etc. where this experiment is described / referenced
  - more detailed description of the experiment
  - information regarding forcing data (eg. Link)


## Provide simulation specific information

For us to start working on the processing / CMORisation of your simulations, we require some simulation specific information:
- Where to find the simulation specific model output on disk?
- What [experiment](#RegisterExperiment) was simulated? What `variant_label` (eg. member id) should be given to that simulation (eg. r1i1p1f1).
- Branch information, `references` and `variant_info` (initialisation, forcing and general realisation or simulation specific information to be added to the netCDF metadata of the CMORised files). 
- etc.

This information can be provided via a GoogleSpreadsheet: [LINK](https://docs.google.com/spreadsheets/d/10WIxv98KMEIFQXSCVnesfly5u9ymsJW72u_WUamlJq8/edit?usp=sharing)
Please request editing access for this GoogleSpreadsheet, select your model ...

![SimulationSpreadsheet](docs/source/SimulationSpreadsheet.png)

... and fill in the required information for each simulation. Please use already added simulations as example.

![SimulationModelingGroups](docs/source/SimulationSpreadsheet_ModelingGroups.png)

Please notify us in your [Register a new experiment](#RegisterExperiment) GitLab Issue when you added all simulation specific information or assistance is required.


# CMOR tables - variable metadata

Initially, the Palmod2 variable list has been put together in form of an excel document. Out of this document, the CMOR tables have been created, holding all necessary information of the variable metadata to be written to the data files by CMOR or a similar program.
Yet, there are a few open questions regarding some of the variables (namely the PalMod-specific variables that did not exist in CMIP6). Before these variables can be added to the CMOR tables, this questions will be adressed in form of [issues](https://gitlab.dkrz.de/dicad-pp/palmod2_dast/-/issues) in the near future.

# Variable Mapping and CMORisation

To be able to make use of tools (such as [`cdo cmor`](https://code.mpimet.mpg.de/projects/cdo/wiki/CDO_CMOR_Operator)), that are able to rewrite climate model output formatted according to a specified project standard, a mapping has to be set up, linking the variables in the model output to their counterparts in the data standard (i.e. the CMOR variables). This mapping can be put together using the WebGUI:

## [Variable Mapping WebGUI](https://c6dreq.dkrz.de)

The entered variable mapping information can be automatically compiled into script fragments helping to perform the diagnostics and CMORisation of the model output (example [diagnostic](https://gitlab.dkrz.de/dicad-pp/palmod2_dast/-/blob/main/cmor/transient-deglaciation-prescribed/scripts/palmod2_diagnostic_MPI-ESM1-2_echam6_auto.h), [CMORisation](https://gitlab.dkrz.de/dicad-pp/palmod2_dast/-/blob/main/cmor/transient-deglaciation-prescribed/scripts/palmod2_cmor-rewrite_MPI-ESM1-2_echam6_auto.h)). The aggregation and overall run-script, however, have to be manually created. We are happy to assist with this. Building on previous efforts / examples should also help.

### Register

Please register at the given [website](https://c6dreq.dkrz.de). Please also make sure, that the model you want to enter the mapping information for is registered in the [CV](#RegisterModel).

### Accessing the variable mapping database

- Open the `Variable Mapping` tab on the [main page of the website](https://c6dreq.dkrz.de)
- Select the project `PalMod2` and click on `Select`
- Select the `submodel` you want to add mapping information for and click on `Edit`

![Access](docs/source/PalMod2Mapping1.png)

### Filters 

![Filters](docs/source/PalMod2Mapping2.png)

- `Filters` help to find the variables of the PalMod2 variable list you want to map to your model output
- Most filters allow the usage of the wildcard character `%`, which is SQL's expression of the asterisk (`*`) character you are familiar with from Linux shells.

### Entering variable mapping information

- For models that have been registered for CMIP6, the CMIP6 mapping information has been imported and can be adjusted to the PalMod2 output namelist.
- Quickly editing a variable is possible by clicking anywhere on the row of interest, which expands a form.
- Clicking instead on the rows `Edit` button opens the form in a new tab, with more detailed information about the CMOR variable, editing history, etc.

![Editing1](docs/source/PalMod2Mapping3.png)

- The post processing is split into:
  - **aggregation**: temporal averaging, vertical interpolation, diagnostic variables serving as input for more than one CMOR variable
  - **diagnostic**: derivation of the CMOR variable out of one or multiple model variables
  - **CMORisation**: Rewrite in the specified project format, incl. all required metadata
- The WebGUI covers only the diagnostic and CMORisation part of the post processing. An aggregation script has to be set up manually ([example](https://gitlab.dkrz.de/dicad-pp/palmod2_dast/-/blob/main/cmor/transient-deglaciation-prescribed/scripts/palmod2_aggregation_MPI-ESM1-2_jsbach.h)).
- The model variable information related to the model output filenames / variable names to be entered, is thus the state after a possible `aggregation`!
- The following information has to be provided:
  - `filename patterns` along with `variable names (netCDF) or codes (GRIB)` for all source variables of the CMOR variable (`DATE` serves as placeholder for the file timestamp, see examples) - multiple input files / variables can be specified if required (for the diagnostic recipe or to be part of a character dimension)
  - _optionally_ a diagnostic recipe that can be interpreted by the [`cdo expr` operator](https://code.mpimet.mpg.de/projects/cdo/embedded/index.html#x1-3240002.7.1) (enter `CUSTOM` as recipe, if the diagnostic is not suitable for `cdo expr`, which allows to bind in an external diagnostic script for this variable, [like this one](https://gitlab.dkrz.de/dicad-pp/palmod2_dast/-/blob/main/cmor/transient-deglaciation-prescribed/scripts/incl_mod_jsbach/c6_diag_Lmon_landCoverFrac_jsbach.h), `CUSTOM` is not meant to allow aggregation)
  - the `units` in the state after a possible diagnostic
  - if applicable: the `positive` flux direction in the state after a possible diagnostic
  - if applicable: `decadal bounds` - can be left blank or be set to `???9` for now
  - _optionally_ provide a `comment` which is written to the file by `cdo cmor` - usually done if the model variable differs in some way from the requested CMOR variable
  - _optionally_ add an `Editor's note` for example with information about the required aggregation
  - `Submit`

![Editing2](docs/source/PalMod2Mapping4.png)


In the following a few examples of entered diagnostic recipes:
- temporary variables (denoted by the leading underscore) can be part of the recipes, different recipes have to be separated by a semi-colon:
```
_TS=ln((298.15-thetao)/(thetao+273.15));o2sat=0.0446596*exp(2.00907+3.22014*_TS+4.05010*_TS^2+4.94457*_TS^3-0.256847*_TS^4+3.88767*_TS^5-so*(0.00624523+_TS*0.00737614+0.0103410*_TS^2+0.00817083*_TS^3+0.000000488682*so))
```
- the output variable name does not have to be specified, as long as there is only one resulting variable
- variables from multiple input files can be part of the diagnostic recipe, you do not need to worry about the merging of the input files
- `cdo expr` supports a variety of operator functions, for example level selection and vertical integration/summing 
```
1000*(sellevidx(var84,1)+lay2factor*sellevidx(var84,2))
```
```
0.01201*vertsum(sellevidxrange(var32,1,4)+sellevidxrange(var34,1,4)+sellevidxrange(var36,1,4)+sellevidxrange(var38,1,4)+sellevidxrange(var39,1,4)+sellevidxrange(var42,1,4)+sellevidxrange(var44,1,4)+sellevidxrange(var46,1,4)+sellevidxrange(var48,1,4)+sellevidxrange(var49,1,4))/vertsum(sellevidxrange(var12,1,4))
```

### Further information

Feel free to contact us via email or open an issue if there are any questions.
If necessary, we can also organize brief introductions via zoom. 
More detailed **HowTos** are available here:
- https://c6dreq.dkrz.de/info/howto.php
- https://docs.google.com/presentation/d/1BaSHpF6I1DIbNnl2sKyiYmzAg8PqsGJFK2ILlty-pYI/edit?usp=sharing
