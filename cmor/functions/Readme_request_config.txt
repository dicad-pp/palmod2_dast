Functions for use within the CMIP6 DICAD Post Processing
written by Martin Schupfner, DKRZ

 Readme if_requested functionality
#################################

 The if_requested function turns on/off the post processing of variables depending
  on data request and user specifications

 The if_requested functionality consists of 3 parts:
  1) Configuration file where the Data Request Settings and User Settings are stored
  2) Section in the post processing script where "timeslices" 
     and "user setting containers" are defined
  3) 2 functions to read the configuration file (Read_request_conf) 
     and to check whether to process the variable for the 
     current timestep, experiment, etc (if_requested)

##################################

1) Configuration file

##################################

There are several types of settings to control the variable processing
that have a different priority/hierarchy when being evaluated:
- Data Request Settings: Depending on the MIPs being supported and the current experiment,
                         the processing of (groups of) variables will be switched off/on
                         by automatically generated settings. But variables may also be requested
                         for a certain period of the experiment.
                         Therefore there are the TimeSlice settings, that will be automatically
                         generated out of the data request: 
 - TimeSlices/slice: timeslices have to be defined in the main post processing script,
               or in the configuration file. When being defined in the main post processing script,
               they can be referenced in the configuration file for a group of variables.
               That can be done automatically for the data request or it can be set manually 
               by the user (see examples below).
               If the timestep does not match the timeslice, the (group of) variable(s) will not be
               processed.

- User Settings: The user may overwrite the data request settings for an experiment simply 
                 by stating his settings in the 'USERSETTINGS' section below the 'DREQSETTINGS' 
                 section in the configuration file.
                 Every experiment has its own section, consisting of separated data request 
                 and user settings.
                 + The user can also define "SettingsContainer" (within the script), which are 
                   containers of settings, and can be switched off or on in a dynamical way:
                     An example would be an 'aqua planet' experiment. One could define the 
                     Options container 'AquaPlanet' in the post processing
                     script, which, if enabled in the configuration file, would exclude several 
                     variables of the land model from the post processing chain:
                        'SettingsContainer[AquaPlanet]+=([Lmon]=(mrso:False mrro:False mrros:False nbp:False fFire:False))
                         SettingsContainer[AquaPlanet]+=([LImon]=(snwmm:False snm:False snw:False))'
                     The enabling of this container in the configuration file, however, could happen 
                     dynamically:
                       instead of, 'Option:AquaPlanet=True' the configuration file could include 
                                   'Option:AquaPlanet=aquaplanet', with 'aquaplanet' being set 
                       in the processing script as either True or False, and could be read in directly 
                       from an experiment setup file for the climate model.
                       In this way the settings to turn off the processing for land variables 
                       will be automatically activated when necessary.
                + User Settings defined directly and not via setting containers have a higher priority:
                  for example a User Setting 'Lmon:mrso=True' would have higher priority than the above 
                  defined container setting for AquaPlanets '...[Lmon]=mrso:False...'
                + The user may also use dynamic switches for other settings in the configuration file.
                  It is not exclusively possible for SettingsContainer.
                + TimeSlices: all time slices of USERSETTINGS and DREQSETTINGS are merged.

- How to define settings:
  + Standard: 'Field:Key=True/False/varname' with varname being a variable in the post processing 
      script that is either 'True' or 'False',
      'Field' and 'Key' each being a variable label, miptable name, submodel name or realisation.
      For example 'temp2m', 'Amon', 'echam', 'r2i1p1f1', respectively.
      'Lmon:mrso=False' would say the following: For miptable 'Lmon' do not process variable 'mrso'.
      It is neither possible nor necessary to specify whether the field or key used is a miptable, 
      variable, etc, as the labels are different.
  + TimeSlice: 'Field:Key=slice:[<op>]timeslice1[,timeslice2,timeslice3,...]' with 'Field' and 'Key' as above.
      And 'timeslice1/2/3' being the labels of the time slices defined in the processing script,
      timeslice ranges (eg. 18500101-18591231) or 'TOTAL', which refers to the entire timeline of the current experiment.
      <op> is an optional parameter:
        '+' or '' (empty) to add time slice settings (eg. 'Field:Key=slice:Slice1,Slice2,...' 
            or 'Field:Key=slice:+Slice1,Slice2,...')
        '-' to remove previously defined time slice settings (eg. Field:Key=slice:-Slice1,Slice2,...)"
        '!' to add a slice setting for the inverted timeslice (=setting the processing to False for 
            the given time slice, eg. 'Field:Key=slice:!Slice1')."
            When using the '!'-parameter, only ONE time slice is allowed, which has to be in the format of a timeslice range!
            Also the variables 'iniyear' and 'finyear' have to be defined in the script that calls this function, 
            specifying the first and final year of the experiment!
 
      timeslice ranges: It is also possible to define timeslices directly in the configuration file.
      'r2i1p1f1:Amon=slice:piControl010,1950010100-1959123124' would say the following: 
        For realisation 'r2i1p1f1' process variables from the miptable 'Amon' only,
        when the current timestep lies within the timeslice 'piControl010' 
        (which is defined in the post processing script and just referenced here)
        or between January 1st, 1950 at 00 o'clock and December 31st, 1959 at 24 o'clock 
        (with this timeslice being defined directly in the configuration file).
      The timeslice boundaries are evaluated as follows: 'lower bound <= timestep < upper bound'.
      Bounds have to be given with at least 10 digits ('[...Y]YYYYMMDDHH'), with the lower
        and upper bound having the same number of digits.
    
  + Options: 'Option:optname=Active/True/Inactive/False/varname' with varname being a variable in 
      the post processing script that is either 'True'/'Active' or 'False'/'Inactive',
      and 'optname' being an entry of the SettingsContainer in the processing script. 
      With this entry in the configuration file, the option 'optname' will be either activated 
      or deactivated.

- Example for an experiment section in the configuration file:
#########################################################################


##################################################
#EXP=piControl#
##################################################

#DREQSETTINGS#                                                      
   # Data Request Settings for this experiment start here
3hr        : 3hr        = slice: piControl030a,piControl200 
   # set timeslices for miptable 3hr
prc        : 3hr        = slice: TOTAL
   # specify exceptions for variable prc, pr and rlds
pr         : 3hr        = slice: TOTAL
rlds       : 3hr        = slice: piControl030a,piControl200,piControl001
tas        : 3hr        = False                                     
   # tas for 3hr is not requested and will not be processed
   # though it would match one of the timeslices defined above
#USERSETTINGS#
# ---> Specify your settings for Experiment piControl here    
   # User specifications for this experiment start here
Amon:tas=False
   # variable tas from miptable Amon not desired 
Amon:Amon=slice:1980010100-1989123124,2010010100-2014123124
   # miptable Amon only desired for certain timeslices
3hr:3hr=slice: piControl040                                         
   # Additonally to piControl030a and piControl200, 
   # MIPtable 3hr will be processed for the timeslice piControl040
Option:AquaPlanet=AquaPlanet  
  # Aqua planet options will be dynamically switched off/on
Option:6hrx=True
  # The settings collected in the option container '6hrx' are being enabled 

#########################################################################

- Priority of the settings:
  Example:
 + The processing of a variable for a certain timestep, model and realisation is
   enabled per default. Thereby the settings are being used to exclude variables
   from processing OR to specify exceptions after excluding a larger group of variables
   from processing.
 + TimeSlice setting may disable processing for a certain timestep,
   also when Standard settings generally allow processing for this variable.
 + Standard setting may generally disable the processing for a variable,
   also when it would match a TimeSlice setting.
 + User settings have higher priority than data request settings.
   TimeSlices defined by the user will be ADDED to TimeSlices requested
   by the Data Request.
   -> User says: timeslice1, Dreq says: timeslice2.
      Thus, the variable(s) will be processed for both slices!
 + Standard User Settings have a higher priority than settings activated via SettingsContainer
   (settings containers).

- Priority inside of Settings:
 + Default Priority Order is (high to low): VAR MIPTABLE MODEL REALISATION (TOTAL)
 + A custom Priority Order can be defined by specifying
   PRIORITYORDER=MIPTABLE MODEL REALISATION VAR 
   with an arbitrary order of this 4 keywords 
   (MIPTABLE, MODEL, REALISATION, VAR)
   for an experiment in the configuration file 
 + The keyword TOTAL will be added as last member of the PRIORITYORDER
   which cannot be influenced by the user
 + Settings will be searched through by field and then by key,
   starting from the highest priority.
 + The first matching setting will be applied!
 Examples (for default priority):
 + 'Var:Model=False' has higher priority than 'Model:Var=True'
 + 'Var:Var=True' has higher priority than 'Var:Realisation=False'
 + 'Var:MIPtable=False' has higher priority than 'MIPtable:MIPtable=True'
 + 'TOTAL:TOTAL=False' has the least priority. This is helpful when wanting to test the processing 
     of a single variable -> turn off the processing of all variables with this setting and then 
     explicity turn on the processing of single variables/variable groups with other settings
     (eg. 'Var:Var=True' or 'MIPtable:MIPtable=True')
- Format:
 + TimeSlice-labels may not contain blanks! E.g. labeling the slice 'pi Control20' is not supported.
 + You may use blanks/tabs to format the settings,
   BUT the timeslice identifier has to be 'slice:' and not 'slice  :' for example.
 + Do not alter the DREQSETTINGS as they are set by the official Data Request.
   Use the USERSETTINGS instead. In case of an updated Data Request this will make it easy
   to keep the user defined settings and distinguish between user and data request settings.
 + Use '#' for comments (also inline).
###################################################

##################################

2) PostProcessing Script

##################################

TimeSlices and SettingsContainer can be defined in the PostProcessing script.
They will be defined as ASSOCIATIVE ARRAY.

TimeSlices:
TimeSlices can also be directly defined in the configuration file as explained under 1).
The alternative is to define TimeSlices in the PostProcessing script and to reference them
in the configuration file.

Example:
"TimeSlices+=([DAMIP20]=(2081010100-2100123124))
 TimeSlices+=([DAMIP42]=(1979010100-2020123124))"

will create the associative array 'TimeSlices', containing the two
slices 'DAMIP20' and 'DAMIP42'. 
The TimeSlices bounds have to be entered with at least 10 digits: [...Y]YYYYMMDDHH, 
 with both bounds having the same number of digits.
'DAMIP20' or 'DAMIP42' are the TimeSlices labels, which can be referenced
in the configuration file,
eg. 'Amon: tas = slice: DAMIP20, DAMIP42'. 

Example 2: More slices for the same TimeSlices label
"TimeSlices+=([DAMIP40]=((2041010100-2060123124 2081010100-2100123124))"

will create the associative array 'TimeSlices', containing the
slice 'DAMIP40', which actually consists of 2 slices.


SettingsContainer:
Standard settings can be combined in containers, called SettingsContainer.
Each setting container can be activated dynamically so the settings will only
be taken into account when needed.
SettingsContainer make it possible to map complex decision-making processes on 
whether to process variables whose processing depends on the experiment or model 
setup and prevent long if-else-clauses in the post processing script.

Example 1:
SettingsContainer+=([AquaPlanet]=([Amon]=(fco2nat:False)))
SettingsContainer[AquaPlanet]+=([Lmon]=(mrso:False mrro:False mrros:False nbp:False fFire:False))

This two lines define the setting container labeled 'AquaPlanet'. If the Option 'AquaPlanet' 
will be set to 'True' in the configuration file, the following standard settings will be applied:
Amon:fco2nat=False
Lmon:mrso   =False
Lmon:mrro   =False
Lmon:mrros  =False
Lmon:nbp    =False
Lmon:fFire  =False
If the Option 'AquaPlanet' is set to 'False', no standard settings will be applied.
The OptionDefinition 'AquaPlanet' can be dynamically activated by adding
Option:AquaPlanet=switch
to the configuration file. The variable 'switch' has to be set as True/False in the post 
processing script. This can be done for example by reading it in from a namelist that 
contains information whether it was an aqua planet experiment or not! 

Example 2:
SettingsContainer+=([switch_co2]=([Amon]=(co2:True,lco2:True,lco2_scenario:True co2mass:True,lco2:False,lco2_scenario:True co2massClim:True,lco2:False,lco2_scenario:False)))
'True' is the default so it can also be defined as:
SettingsContainer+=([switch_co2]=([Amon]=(co2,lco2,lco2_scenario co2mass,lco2:False,lco2_scenario co2massClim,lco2:False,lco2_scenario:False)))

This line defines the setting container labeled 'switch_co2'.
Beside the standard settings it also contains conditions under which the standard setting 
will be translated to False or True:
Amon:co2=True under the condition that the switches 'lco2' and 'lco2_scenario' are True, 
otherwise it will be set:
Amon:co2=False
Amon:co2mass=True under the condition that the switch 'lco2' is False and the switch 'lco2_scenario' 
is True, otherwise it will be set:
Amon:co2mass=False
Amon:co2massClim=True under the condition that the switches 'lco2' and 'lco2_scenario' are False,
otherwise it will be set:
Amon:co2massClim=False
The switches 'lco2' and 'lco2_scenario' must be defined as True or False in the post processing 
script. If the OptionDefinition is not activated in the configuration script, no standard settings 
will be applied.
It would also be possible to replace any other True/False value by a switch variable, if necessary.




##################################

3) Functions

##################################

Read_request_config [-s/-v] $experiment $dReqconf || exit 1

The function Read_request_config does read the Configuration File $dReqconf.
The configuration depends on the $experiment label.
Optionally verbose output can be displayed with verbose 
having the following levels:
-s  -> silent mode - no verbose output
-v  -> verbose mode- maximal verbose output
no option -> minimal verbose output 

The function Read_request_config needs the associative arrays 'TimeSlices' and 'SettingsContainer'
to be defined in the post processing script if any of the 'TimeSlices' or 'SettingsContainer'
is being referenced in the configuration file!

The function Read_request_config is meant to be only called once to read the DataRequest/User 
Configuration and store it into associative arrays.


if_requested [-v/-s] $member $model $miptable $variable $chunk [$check/$endOfChunk] && echo "requested" || echo "not requested"

The function if_requested is testing the current settings (model, miptable, variable, timestep/chunk) 
against the configuration settings and decides whether the current variable is being processed for 
this timestep/chunk or not.

Additionally there are options designed to handle climatologies and decadal means:
Define $check as "clim" when processing the climatological mean of a variable to check
  if this timestep is part of a climatological interval.
Define $check as "climlast" when processing the climatological mean of a variable to check
  if this timestep is the last timestep to be considered in the climatological interval.
  If no TimeSlice is defined for this variable or overlapping TimeSlices are defined 
  an error is being raised.
Define $endOfChunk to test if "*$endOfChunk" is equal to "$chunk". 
  Eg. set it to "???9010100-???9123124" to test if the chunk corresponds to the year xxx9
  for which the decadal mean xxx0-xxx9 has to be processed. Wildcard characters '*' and '?'
  are allowed.

Optionally verbose output can be displayed with verbose
having the following levels:
-s  -> silent mode - no verbose output
-v  -> verbose mode- maximal verbose output
no option -> minimal verbose output            

$member - Realisation of the experiment, eg. r1i1p1f1
$model  - Current submodel, eg. jsbach
$miptable - MIP table, eg. Amon
$variable - Variable shortname/label, eg. tas
$chunk    - either timestep or both bounds of the chunk separated by '-',
             10+ digits accuracy for the time bounds: [...Y]YYYYMMDDHH, eg. '1850010100-1850123124' 
             or '1850010100'. If both bounds are given, they need to have the same number of digits.
$check    - "clim" or "climlast" to check if chunk is part/last timestep of a climatological interval
$endOfChunk - is being compared with chunk, may contain wildcard characters '?' and '*'
The function if_requested needs the associative array 'TimeSlices' defined if the configuration 
file contains any TimeSlices settings.
The function if_requested also needs a preceding call of the function Read_request_config 
to read in the user and data request configuration.

The function returns exit code 0, when the variable shall be processed for this timestep/chunk
 (catch with '&&'), otherwise it returns exit code 1 (catch with '||').
