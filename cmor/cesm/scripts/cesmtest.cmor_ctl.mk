EXP_ID = cesmtest
INITIAL_YEAR = 2700

FIRST_YEAR = 2700
LAST_YEAR = 2709
LAST_YEAR = 2700
CHUNK_SIZE = 100
LAST_CHUNK_YEAR = $(shell echo $$(($*+$(CHUNK_SIZE)-1 > $(LAST_YEAR) ? $(LAST_YEAR) : $*+$(CHUNK_SIZE)-1)))

ACCOUNT = bm0021
PARTITION = compute

SUFFIX =
ARCHIVE_SUFFIX =

TARGET_STEP=cmor

AGGR_TIME = 00:10:00
DIAGS_TIME = 00:30:00
CMOR_TIME = 02:30:00

CMOR_FLAGS =

SBATCHFLAGS=-W

MESSAGE = $$(date +%Y-%m-%dT%H:%M:%S): CMIP6 diagnostics and CMOR rewriting ($(SUFFIX:_%=%))

.PHONY: all
.PRECIOUS: targets/$(EXP_ID).aggr$(SUFFIX).% targets/$(EXP_ID).diags$(SUFFIX).% targets/$(EXP_ID).cmor$(SUFFIX).%

INITIAL_TARGET = targets/$(EXP_ID).aggr$(SUFFIX).$(INITIAL_YEAR)

all:
	@echo "$(MESSAGE) started"
	mkdir -vp targets
	$(MAKE) $(MFLAGS) -f $(MAKEFILE_LIST) $(INITIAL_TARGET)
	$(MAKE) $(MFLAGS) -f $(MAKEFILE_LIST) $(addprefix targets/$(EXP_ID).$(TARGET_STEP)$(SUFFIX).,$(shell seq $(FIRST_YEAR) $(CHUNK_SIZE) $(LAST_YEAR)))
	@echo "$(MESSAGE) finished"

targets/$(EXP_ID).aggr$(SUFFIX).%:
	sbatch $(SBATCHFLAGS) --time=$(AGGR_TIME) --job-name=$(EXP_ID)_runpp_aggr$(SUFFIX) --output=%x_%j.log --comment=$* $(EXP_ID).runpp -s '$(SUFFIX)' -S '$(ARCHIVE_SUFFIX)' -A $* $(LAST_CHUNK_YEAR)
	@touch $@

targets/$(EXP_ID).diags$(SUFFIX).%: targets/$(EXP_ID).aggr$(SUFFIX).%
	sbatch $(SBATCHFLAGS) --time=$(DIAGS_TIME) --job-name=$(EXP_ID)_runpp_diag$(SUFFIX) --output=%x_%j.log --comment=$* $(EXP_ID).runpp -s '$(SUFFIX)' -S '$(ARCHIVE_SUFFIX)' -d $* $(LAST_CHUNK_YEAR)
	@touch $@

targets/$(EXP_ID).cmor$(SUFFIX).%: targets/$(EXP_ID).diags$(SUFFIX).%
	sbatch $(SBATCHFLAGS) --time=$(CMOR_TIME) --job-name=$(EXP_ID)_runpp_cmor$(SUFFIX) --output=%x_%j.log --comment=$* $(EXP_ID).runpp -s '$(SUFFIX)' -S '$(ARCHIVE_SUFFIX)' -c $(CMOR_FLAGS) $* $(LAST_CHUNK_YEAR)
	@touch $@

