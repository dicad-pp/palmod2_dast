#-- CMOR-rewrite for pop2 (ESM: CESM) Omon
cn='msftbarot msftmz'
for var in $cn; do
  { (if_requested $member $ocemod Omon $var $chunk || continue
  mkdir -p $dr/$submodel/Omon_${var}
  ifile=${sdir}/out_diag/Omon_${var}_$period.nc
  echo $cdo cmor,Omon,i=$it,mt=$mt,dr=$dr/$submodel/Omon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Omon,i=$it,mt=$mt,dr=$dr/$submodel/Omon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Omon 2>&1
done

#-- CMOR-rewrite for pop2 (ESM: CESM) Omon
cn='so thetao vo'
find_file "$sdir" "*.pop.*.${period}.nc" ifile >> $err.find_file.Omon 2>&1
for var in $cn; do
  { (if_requested $member $ocemod Omon $var $chunk || continue
  mkdir -p $dr/$submodel/Omon_${var}
  echo $cdo cmor,Omon,i=$it,mt=$mt,dr=$dr/$submodel/Omon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Omon,i=$it,mt=$mt,dr=$dr/$submodel/Omon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Omon 2>&1
done

