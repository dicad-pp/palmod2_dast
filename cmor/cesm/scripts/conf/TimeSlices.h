######################################################
# PalMod2 Data Request TimeSlices (v00.00.02)        #
######################################################
TimeSlices+=([_slice_clim20]=(001995010100-002014123124))
TimeSlices+=([_slice_climLast20]=($((finyear-19))010100-${finyear}123124))
TimeSlices+=([_slice_scenario20]=(002081010100-002100123124))
TimeSlices+=([_slice_hist20]=(001995010100-002014123124))
TimeSlices+=([_slice_hist36]=(001979010100-002014123124))
TimeSlices+=([_slice_hist55]=(001960010100-002014123124))
TimeSlices+=([_slice_hist65]=(001950010100-002014123124))
TimeSlices+=([_slice_histext]=(002015010100-002019123124))
TimeSlices+=([_slice_hist55plus]=(001850010100-001850123124 001960010100-002014123124))
#piControl slices: Years matching the last x years of historical 
# (020-165), or years matching historical and beyond (200).
TimeSlices+=([_slice_piControl020]=(001995010100-002014123124))
TimeSlices+=([_slice_piControl030]=(001985010100-002014123124))
TimeSlices+=([_slice_piControl040]=(001975010100-002014123124))
TimeSlices+=([_slice_piControl050]=(001965010100-002014123124))
TimeSlices+=([_slice_piControl100]=(001915010100-002014123124))
TimeSlices+=([_slice_piControl140]=(001875010100-002014123124))
TimeSlices+=([_slice_piControl165]=(001850010100-002014123124))
TimeSlices+=([_slice_piControl200]=(001850010100-002049123124))
TimeSlices+=([_slice_piControl500]=(001850010100-002349123124))
TimeSlices+=([_slice_piControl1000]=(001850010100-002849123124))

#TimeSlices for 1st year only
TimeSlices+=([first_year]=(${iniyear}010100-${iniyear}123124))
TimeSlices+=([first_step]=(${iniyear}010100-${iniyear}013124))
#TimeSlice for default climatology (entire experiment time span)
TimeSlices+=([DefaultClim]=(${iniyear}010100-${finyear}013124))
