#-- Diagnostic for pop2 (ESM: CESM) variable msftbarot / table Omon
# Editor Note: UM: a) conversion from Sv to kg/s by multiplication with 10^9 b) on original horizontal POP grid
{ (if_requested $member $ocemod Omon msftbarot $chunk && {
  find_file -e            "$sdir" "*.pop.*.${period}.nc" ifile
  $cdo -f nc -O \
    expr,'msftbarot=BSF*10^9;' \
    $ifile ${sdir}/out_diag/Omon_msftbarot_$period.nc || echo ERROR
}; )&; }>$err.msftbarot.Omon 2>&1

#-- Diagnostic for pop2 (ESM: CESM) variable msftmz / table Omon
# Editor Note: UM: a) conversion from Sv to kg/s by multiplying with 10^9 b) How to handle the 5-dim. overturning of POP here? MOC is a function of time, transport region (global, Atl.), components (Eulerian, eddy, submeso), depth and latitude.
{ (if_requested $member $ocemod Omon msftmz $chunk && {
  find_file -e            "$sdir" "*.pop.*.${period}.nc" ifile
  $cdo -f nc -O \
    expr,'msftmz=MOC*10^9;' \
    $ifile ${sdir}/out_diag/Omon_msftmz_$period.nc || echo ERROR
}; )&; }>$err.msftmz.Omon 2>&1

