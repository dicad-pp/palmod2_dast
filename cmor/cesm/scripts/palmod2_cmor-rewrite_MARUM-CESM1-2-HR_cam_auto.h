#-- CMOR-rewrite for cam (ESM: CESM) Amon
cn='pr prsn'
for var in $cn; do
  { (if_requested $member $atmmod Amon $var $chunk || continue
  mkdir -p $dr/$submodel/Amon_${var}
  ifile=${sdir}/out_diag/Amon_${var}_$period.nc
  echo $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Amon 2>&1
done

#-- CMOR-rewrite for cam (ESM: CESM) Amon
cn='prc ps psl tas ts'
find_file "$sdir" "*.cam.*.${period}.nc" ifile >> $err.find_file.Amon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod Amon $var $chunk || continue
  mkdir -p $dr/$submodel/Amon_${var}
  echo $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Amon 2>&1
done

#-- CMOR-rewrite for cam (ESM: CESM) Amon
cn='zg'
find_file "$sdir" "*.cam.*.${period}.plev19.nc" ifile >> $err.find_file.Amon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod Amon $var $chunk || continue
  mkdir -p $dr/$submodel/Amon_${var}
  echo $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Amon 2>&1
done

