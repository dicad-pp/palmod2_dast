#-- Diagnostic for cam (ESM: CESM) variable pr / table Amon
# Editor Note: Unit conversion m s-1 to kg m-2 s-1 by multiplication with factor 1000? Ute: suggestion is fine
{ (if_requested $member $atmmod Amon pr $chunk && {
  find_file -e            "$sdir" "*.cam.*.${period}.nc" ifile
  $cdo -f nc -O \
    expr,'pr=(PRECL+PRECC)*1000.;' \
    $ifile ${sdir}/out_diag/Amon_pr_$period.nc || echo ERROR
}; )&; }>$err.pr.Amon 2>&1

#-- Diagnostic for cam (ESM: CESM) variable prsn / table Amon
# Editor Note: Unit conversion m s-1 to kg m-2 s-1 by multiplication with factor 1000?
{ (if_requested $member $atmmod Amon prsn $chunk && {
  find_file -e            "$sdir" "*.cam.*.${period}.nc" ifile
  $cdo -f nc -O \
    expr,'prsn=(PRECSC+PRECSL)*1000.;' \
    $ifile ${sdir}/out_diag/Amon_prsn_$period.nc || echo ERROR
}; )&; }>$err.prsn.Amon 2>&1

