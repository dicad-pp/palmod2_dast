#-- CMOR-rewrite for echam6 (ESM: AWI-ESM) AERmon
cn='ua va'
find_file "$sdir" "*_echammon_after_${period}*" ifile >> $err.find_file.AERmon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod AERmon $var $chunk || continue
  mkdir -p $dr/$submodel/AERmon_${var}
  $cdo cmor,AERmon,i=$it,mt=$mt,dr=$dr/$submodel/AERmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.AERmon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: AWI-ESM) Amon
cn='evspsbl pr rlds rsds'
for var in $cn; do
  { (if_requested $member $atmmod Amon $var $chunk || continue
  mkdir -p $dr/$submodel/Amon_${var}
  ifile=${sdir}/out_diag/Amon_${var}_$period.nc
  $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Amon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: AWI-ESM) Amon
cn='cl clt hfls hfss hurs huss prc prsn ps rlus rlut rsdt rsus rsut sfcWind tas tasmax tasmin ts uas vas'
find_file "$sdir" "*_${period}*_echam" ifile >> $err.find_file.Amon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod Amon $var $chunk || continue
  mkdir -p $dr/$submodel/Amon_${var}
  $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Amon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: AWI-ESM) Amon
cn='psl'
find_file "$sdir" "*_echammon_after_${period}*" ifile >> $err.find_file.Amon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod Amon $var $chunk || continue
  mkdir -p $dr/$submodel/Amon_${var}
  $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Amon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: AWI-ESM) Amon
cn='hur hus ta ua va wap zg'
find_file "$sdir" "*_echammon_plev19_${period}*" ifile >> $err.find_file.Amon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod Amon $var $chunk || continue
  mkdir -p $dr/$submodel/Amon_${var}
  $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Amon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: AWI-ESM) Emon
cn='tdps'
find_file "$sdir" "*_${period}*_echam" ifile >> $err.find_file.Emon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod Emon $var $chunk || continue
  mkdir -p $dr/$submodel/Emon_${var}
  $cdo cmor,Emon,i=$it,mt=$mt,dr=$dr/$submodel/Emon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Emon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: AWI-ESM) LImon
cn='snw'
for var in $cn; do
  { (if_requested $member $atmmod LImon $var $chunk || continue
  mkdir -p $dr/$submodel/LImon_${var}
  ifile=${sdir}/out_diag/LImon_${var}_$period.nc
  $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.LImon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: AWI-ESM) LImon
cn='snm'
find_file "$sdir" "*_${period}*_accw" ifile >> $err.find_file.LImon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod LImon $var $chunk || continue
  mkdir -p $dr/$submodel/LImon_${var}
  $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.LImon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: AWI-ESM) LImon
cn='snd'
find_file -p $period "$sdir" "*_${period}*_echam" ifile >> $err.find_file.LImon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod LImon $var $chunk || continue
  mkdir -p $dr/$submodel/LImon_${var}
  $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.LImon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: AWI-ESM) Lmon
cn='mrros'
for var in $cn; do
  { (if_requested $member $atmmod Lmon $var $chunk || continue
  mkdir -p $dr/$submodel/Lmon_${var}
  ifile=${sdir}/out_diag/Lmon_${var}_$period.nc
  $cdo cmor,Lmon,i=$it,mt=$mt,dr=$dr/$submodel/Lmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Lmon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: AWI-ESM) Lmon
cn='mrro'
find_file "$sdir" "*_${period}*_accw" ifile >> $err.find_file.Lmon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod Lmon $var $chunk || continue
  mkdir -p $dr/$submodel/Lmon_${var}
  $cdo cmor,Lmon,i=$it,mt=$mt,dr=$dr/$submodel/Lmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Lmon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: AWI-ESM) SImon
cn='sitemptop'
find_file "$sdir" "*_${period}*_echam" ifile >> $err.find_file.SImon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod SImon $var $chunk || continue
  mkdir -p $dr/$submodel/SImon_${var}
  $cdo cmor,SImon,i=$it,mt=$mt,dr=$dr/$submodel/SImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.SImon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: AWI-ESM) dec
cn='orog'
for var in $cn; do
  { (if_requested $member $atmmod dec $var $chunk '???0123124' || continue
  mkdir -p $dr/$submodel/dec_${var}
  ifile=${sdir}/out_diag/dec_${var}_$period.nc
  $cdo cmor,dec,i=$it,mt=$mt,dr=$dr/$submodel/dec_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.dec 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: AWI-ESM) fx
cn='areacella'
for var in $cn; do
  { (if_requested $member $atmmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  ifile=${sdir}/out_diag/fx_$var.nc
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

