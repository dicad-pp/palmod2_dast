#-- Diagnostic for jsbach (ESM: AWI-ESM) variable vegFrac / table Emon
# Editor Note: Sum up all tiles. Provide for all years only if dynveg=true; otherwise for the 1st year only.
{ (if_requested $member $srfmod Emon vegFrac $chunk && {
  find_file -e            "$sdir" "slm_nan_${period}" ifile1
  find_file -e            "$sdir" "veg_ratio_max_mon_${period}*" ifile2
  $cdo -f nc -O \
    expr,'vegFrac=vertsum(var20)/slm;' \
    -merge -selname,slm $ifile1 -selname,var20 $ifile2 \
    ${sdir}/out_diag/Emon_vegFrac_$period.nc || echo ERROR
}; )&; }>$err.vegFrac.Emon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable sbl / table LImon
{ (if_requested $member $srfmod LImon sbl $chunk && {
  find_file -e            "$sdir" "*_${period}*_land" ifile
  $cdo -f nc -O \
    expr,'sbl=-var80;' \
    $ifile ${sdir}/out_diag/LImon_sbl_$period.nc || echo ERROR
}; )&; }>$err.sbl.LImon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable c3PftFrac / table Lmon
# Editor Note: sum all C3 tiles and apply mask for crops. With land use changes cropFracC3 is time dependent; provide then for all year; otherwise provide for 1st year only.
{ (if_requested $member $srfmod Lmon c3PftFrac $chunk && {
  find_file -e            "$sdir" "C3C4_crop_mask_${period}" ifile1
  find_file -e            "$sdir" "box_cover_fract_mon_${period}*" ifile2
  find_file -e            "$sdir" "slm_nan_${period}" ifile3
  $cdo -f nc -O \
    expr,'c3PftFrac=vertsum(sellevidxrange(var12,1,7))+sellevel(var12,9)+sellevidx(var12,11)*(1-C3C4_crop_mask)/slm;' \
    -merge -selname,C3C4_crop_mask $ifile1 -selname,var12 $ifile2 -selname,slm $ifile3 \
    ${sdir}/out_diag/Lmon_c3PftFrac_$period.nc || echo ERROR
}; )&; }>$err.c3PftFrac.Lmon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable c4PftFrac / table Lmon
# Editor Note: sum all C4 tiles and apply mask for crops. With land use changes c4PftFrac is time dependent; provide then for all year; otherwise provide for 1st year only.
{ (if_requested $member $srfmod Lmon c4PftFrac $chunk && {
  find_file -e            "$sdir" "C3C4_crop_mask_${period}" ifile1
  find_file -e            "$sdir" "box_cover_fract_mon_${period}*" ifile2
  find_file -e            "$sdir" "slm_nan_${period}" ifile3
  $cdo -f nc -O \
    expr,'c4PftFrac=sellevidx(var12,8)+sellevidx(var12,10)+sellevidx(var12,11)*C3C4_crop_mask/slm;' \
    -merge -selname,C3C4_crop_mask $ifile1 -selname,var12 $ifile2 -selname,slm $ifile3 \
    ${sdir}/out_diag/Lmon_c4PftFrac_$period.nc || echo ERROR
}; )&; }>$err.c4PftFrac.Lmon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable cLitter / table Lmon
{ (if_requested $member $srfmod Lmon cLitter $chunk && {
  find_file -e            "$sdir" "*_${period}*_yasso" ifile
  $cdo -f nc -O \
    expr,'cLitter=0.01201*vertsum(var31+var33+var35+var37+var41+var43+var45+var47);' \
    $ifile ${sdir}/out_diag/Lmon_cLitter_$period.nc || echo ERROR
}; )&; }>$err.cLitter.Lmon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable cVeg / table Lmon
# Editor Note: Sum up all tiles
{ (if_requested $member $srfmod Lmon cVeg $chunk && {
  find_file -e            "$sdir" "*_${period}*_veg" ifile
  $cdo -f nc -O \
    expr,'cVeg=0.01201*vertsum(var160+var161+var162);' \
    $ifile ${sdir}/out_diag/Lmon_cVeg_$period.nc || echo ERROR
}; )&; }>$err.cVeg.Lmon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable fFire / table Lmon
{ (if_requested $member $srfmod Lmon fFire $chunk && {
  find_file -e            "$sdir" "*_${period}*_veg" ifile
  $cdo -f nc -O \
    expr,'fFire=0.272912*var56;' \
    $ifile ${sdir}/out_diag/Lmon_fFire_$period.nc || echo ERROR
}; )&; }>$err.fFire.Lmon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable frivera / table Lmon
# Editor Note: disch, multiplication with density of water 1000 kg m-3 to attain kg m-2 s-1 out of m s-1
{ (if_requested $member $srfmod Lmon frivera $chunk && {
  find_file -e            "$sdir" "*_${period}*_jsbach" ifile
  $cdo -f nc -O \
    expr,'frivera=1000*var218;' \
    $ifile ${sdir}/out_diag/Lmon_frivera_$period.nc || echo ERROR
}; )&; }>$err.frivera.Lmon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable gpp / table Lmon
# Editor Note: Sum up all tiles
{ (if_requested $member $srfmod Lmon gpp $chunk && {
  find_file -e            "$sdir" "*_${period}*_veg" ifile
  $cdo -f nc -O \
    expr,'gpp=0.01201*vertsum(var173);' \
    $ifile ${sdir}/out_diag/Lmon_gpp_$period.nc || echo ERROR
}; )&; }>$err.gpp.Lmon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable grassFrac / table Lmon
# Editor Note: sum grass tiles (7,8)
{ (if_requested $member $srfmod Lmon grassFrac $chunk && {
  find_file -e            "$sdir" "box_cover_fract_mon_${period}*" ifile1
  find_file -e            "$sdir" "slm_nan_${period}" ifile2
  $cdo -f nc -O \
    expr,'grassFrac=vertsum(sellevidxrange(var12,7,8))/slm;' \
    -merge -selname,var12 $ifile1 -selname,slm $ifile2 \
    ${sdir}/out_diag/Lmon_grassFrac_$period.nc || echo ERROR
}; )&; }>$err.grassFrac.Lmon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable landCoverFrac / table Lmon
# Editor Note: apply c3/c4-crop-mask, veg_ratio_max and glacier mask, to separate all 14 PFTs. Provide for all years only if dynveg=true; otherwise for the 1st year only.
{ (if_requested $member $srfmod Lmon landCoverFrac $chunk && {
  # Insert here your custom diagnostics. Per default an external placeholder script is being included.
  . ${SCRIPT_ROOT}/incl_mod_jsbach/c6_diag_Lmon_landCoverFrac_jsbach.h
}; )&; }>$err.landCoverFrac.Lmon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable mrso / table Lmon
# Editor Note: sum up all layers. Is it not ECHAM6 var140*1000?
{ (if_requested $member $srfmod Lmon mrso $chunk && {
  find_file -e            "$sdir" "*_${period}*_jsbach" ifile
  $cdo -f nc -O \
    expr,'mrso=1000*vertsum(var84);' \
    $ifile ${sdir}/out_diag/Lmon_mrso_$period.nc || echo ERROR
}; )&; }>$err.mrso.Lmon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable npp / table Lmon
# Editor Note: Sum up all tiles
{ (if_requested $member $srfmod Lmon npp $chunk && {
  find_file -e            "$sdir" "*_${period}*_veg" ifile
  $cdo -f nc -O \
    expr,'npp=0.01201*vertsum(var178);' \
    $ifile ${sdir}/out_diag/Lmon_npp_$period.nc || echo ERROR
}; )&; }>$err.npp.Lmon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable rh / table Lmon
# Editor Note: rh comprises respiration from ALL consumers, i.e. from soil organisms as well as above ground herbivory from insects and megafauna (including grazers on pastures)|recipe:-0.01201 * (vertsum(var143) + vertsum(var170) + var211)
{ (if_requested $member $srfmod Lmon rh $chunk && {
  find_file -e            "$sdir" "*_${period}*_veg" ifile
  $cdo -f nc -O \
    expr,'rh=-0.01201*(vertsum(var170+var143)+var211);' \
    $ifile ${sdir}/out_diag/Lmon_rh_$period.nc || echo ERROR
}; )&; }>$err.rh.Lmon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable shrubFrac / table Lmon
# Editor Note: sum up shrub tiles (5,6). Provide for all years only if dynveg=true; otherwise for the 1st year only.
{ (if_requested $member $srfmod Lmon shrubFrac $chunk && {
  find_file -e            "$sdir" "box_cover_fract_mon_${period}*" ifile1
  find_file -e            "$sdir" "slm_nan_${period}" ifile2
  $cdo -f nc -O \
    expr,'shrubFrac=vertsum(sellevidxrange(var12,5,6))/slm;' \
    -merge -selname,var12 $ifile1 -selname,slm $ifile2 \
    ${sdir}/out_diag/Lmon_shrubFrac_$period.nc || echo ERROR
}; )&; }>$err.shrubFrac.Lmon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable treeFrac / table Lmon
# Editor Note: sum up tree tiles (1,2,3,4). Provide for all years only if dynveg=true; otherwise for the 1st year only.
{ (if_requested $member $srfmod Lmon treeFrac $chunk && {
  find_file -e            "$sdir" "box_cover_fract_mon_${period}*" ifile1
  find_file -e            "$sdir" "slm_nan_${period}" ifile2
  $cdo -f nc -O \
    expr,'treeFrac=vertsum(sellevidxrange(var12,1,4))/slm;' \
    -merge -selname,var12 $ifile1 -selname,slm $ifile2 \
    ${sdir}/out_diag/Lmon_treeFrac_$period.nc || echo ERROR
}; )&; }>$err.treeFrac.Lmon 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable sftgif / table dec
{ (if_requested $member $srfmod dec sftgif $chunk && {
  find_file -e            "$sdir" "glac_dec_${period}" ifile
  $cdo -f nc -O \
    yearmean \
    -selname,glac $ifile \
    ${sdir}/tmp_diag/dec_sftgif_$period.nc || echo ERROR
  if_requested $member $srfmod dec sftgif $chunk '???0123124' && {
    eval $cdo -O ensmean \
      ${sdir}/tmp_diag/dec_sftgif_{$((period-9))..${period}}.nc ${sdir}/out_diag/dec_sftgif_${period}.nc && \
      eval rm ${sdir}/tmp_diag/dec_sftgif_{$((period-9))..${period}}.nc || echo ERROR
}; }; )&; }>$err.sftgif.dec 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable sftlf / table dec
{ (if_requested $member $srfmod dec sftlf $chunk && {
  find_file -e            "$sdir" "*_${period}*_jsbach" ifile
  $cdo -f nc -O \
    yearmean \
    -selcode,25 $ifile \
    ${sdir}/tmp_diag/dec_sftlf_$period.nc || echo ERROR
  if_requested $member $srfmod dec sftlf $chunk '???0123124' && {
    eval $cdo -O ensmean \
      ${sdir}/tmp_diag/dec_sftlf_{$((period-9))..${period}}.nc ${sdir}/out_diag/dec_sftlf_${period}.nc && \
      eval rm ${sdir}/tmp_diag/dec_sftlf_{$((period-9))..${period}}.nc || echo ERROR
}; }; )&; }>$err.sftlf.dec 2>&1

#-- Diagnostic for jsbach (ESM: AWI-ESM) variable mrsofc / table fx
{ (if_requested $member $srfmod fx mrsofc $chunk && {
  find_file -e            "$sdir" "aggr_fx_mrsofc_${period}.nc" ifile
  $cdo -f nc -O \
    expr,'mrsofc=soil_field_cap*soil_depth*1.e+3;' \
    $ifile ${sdir}/out_diag/fx_mrsofc.nc || echo ERROR
}; )&; }>$err.mrsofc.fx 2>&1

