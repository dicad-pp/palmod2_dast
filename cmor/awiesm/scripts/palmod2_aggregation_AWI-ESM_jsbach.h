################################
# JSBACH #######################
################################
suf=grb

continue;

{ (if_requested $member $srfmod jsbachdecfx input $chunk '???1123124' && {
  # Read input file
  if [[ "${iniyeararr[$y]}" == "$period" ]]
  then
    echo "Reading from parent ${RAW_EXP_ID_PARENT[$y]} jsbach_T31GR30_11tiles_5layers_natural-veg_${inv_chunk_year}k.nc"
    # $rawsdir_input
    ifile=/work/mh0110/m211003/mpiesm-1.2.00p1/experiments/${RAW_EXP_ID_PARENT[$y]}/restart/topo/jsbach_T31GR30_11tiles_5layers_natural-veg_${inv_chunk_year}k.nc
  else
    echo "Reading from ${RAW_EXP_IDS[$y]} jsbach_T31GR30_11tiles_5layers_natural-veg_${inv_chunk_year}k.nc"
    ifile=/work/mh0110/m211003/mpiesm-1.2.00p1/experiments/${RAW_EXP_IDS[$y]}/restart/topo/jsbach_T31GR30_11tiles_5layers_natural-veg_${inv_chunk_year}k.nc
  fi

  # Create decadal "fx"-variables

  #-- slm
  $cdo selname,slm $ifile $sdir/tmp_aggr/tmp_slm_$period || echo ERROR

  #-- slm_nan
  $cdo setctomiss,0 -selname,slm $ifile $sdir/tmp_aggr/tmp_slm_nan_$period || echo ERROR

  #-- glac
  $cdo setname,glac -eqc,1 -sellevel,1 -selname,cover_type $ifile $sdir/tmp_aggr/tmp_glac_$period || echo ERROR

  #-- veg_ratio_max
  $cdo -f grb setcode,20 -selname,veg_ratio_max $ifile $sdir/tmp_aggr/tmp_veg_ratio_max_mon_ini_$period || echo ERROR

  #-- C3C4_crop_mask
  $cdo setname,C3C4_crop_mask -expr,'C3C4_crop_mask=sellevel(cover_type,11)-20' $ifile $sdir/tmp_aggr/tmp_C3C4_crop_mask_$period || echo ERROR

  #-- mrsofc
  $cdo expr,'mrsofc=soil_field_cap*soil_depth*1.e+3;' $ifile ${sdir}/tmp_aggr/tmp_mrsofc_${period}.nc || echo ERROR

  for l_yyyy in $(seq $period $(( period + 9 )) ); do
    $cdo settaxis,${l_yyyy}-01-01,12:00:00,1mon -duplicate,12 $sdir/tmp_aggr/tmp_slm_$period $sdir/out_aggr/slm_${l_yyyy} || echo ERROR
    $cdo settaxis,${l_yyyy}-01-01,12:00:00,1mon -duplicate,12 $sdir/tmp_aggr/tmp_slm_nan_$period $sdir/out_aggr/slm_nan_${l_yyyy} || echo ERROR
    $cdo settaxis,${l_yyyy}-01-01,12:00:00,1mon -duplicate,12 $sdir/tmp_aggr/tmp_glac_$period $sdir/out_aggr/glac_${l_yyyy} || echo ERROR
    $cdo settaxis,${l_yyyy}-01-01,12:00:00,1mon -duplicate,12 $sdir/tmp_aggr/tmp_veg_ratio_max_mon_ini_$period $sdir/tmp_aggr/veg_ratio_max_mon_ini_${l_yyyy}.tmp || echo ERROR
    $cdo settaxis,${l_yyyy}-01-01,12:00:00,1mon -duplicate,12 $sdir/tmp_aggr/tmp_C3C4_crop_mask_$period $sdir/out_aggr/C3C4_crop_mask_${l_yyyy} || echo ERROR
    $cdo settaxis,${l_yyyy}-01-01,12:00:00,1mon -duplicate,12 -selname,slm $ifile $sdir/out_aggr/sftlf_${l_yyyy}.nc || echo ERROR
    $cdo settaxis,${l_yyyy}-01-01,12:00:00,1mon -duplicate,12 -selname,root_depth $ifile ${sdir}/out_aggr/rootd_${l_yyyy}.nc || echo ERROR
    $cdo settaxis,${l_yyyy}-01-01,12:00:00,1mon -duplicate,12 ${sdir}/tmp_aggr/tmp_mrsofc_${period}.nc ${sdir}/out_aggr/mrsofc_${l_yyyy}.nc || echo ERROR
  done

}; )&; }>$err.jsbachdecfx.input 2>&1

wait

#
#-- cover_fract, veg_ratio_max, and box_cover_fract (monthly mean)
#
{ (if_requested $member $srfmod jsbachdecfx input $chunk && {
  $cdo selcode,12 $sdir/${RAW_EXP_IDS[$y]}_jsbach_jsbach_mm_$period.$suf $sdir/tmp_aggr/cover_fract_mon_$period.tmp || echo ERROR
  if [[ "$(echo ${dynveg:-false} | tr [:upper:] [:lower:])" == "true" ]]; then
    $cdo selcode,20 $sdir/${RAW_EXP_IDS[$y]}_jsbach_jsbach_mm_$period.$suf $sdir/tmp_aggr/veg_ratio_max_mon_$period.tmp || echo ERROR
  else
    cp $sdir/tmp_aggr/veg_ratio_max_mon_ini_$period.tmp $sdir/tmp_aggr/veg_ratio_max_mon_$period.tmp || echo ERROR
  fi

  #  In grib format 3.05e-5 is the minimum value different from 0 for records with values between 0 and 1
  # we set all fractions below 5.e-5 to zero, to avoid errors with variables that need to be scaled.
  # It is important to use netCDF for these calculations!
  $cdo mul              $sdir/tmp_aggr/veg_ratio_max_mon_$period.tmp   $sdir/tmp_aggr/cover_fract_mon_$period.tmp      $sdir/tmp_aggr/box_cover_fract_mon_$period.tmp         || echo ERROR
  $cdo mul -gtc,5.e-5   $sdir/tmp_aggr/veg_ratio_max_mon_$period.tmp   $sdir/tmp_aggr/veg_ratio_max_mon_$period.tmp    $sdir/out_aggr/veg_ratio_max_mon_$period   || echo ERROR
  $cdo mul -gtc,5.e-5   $sdir/tmp_aggr/cover_fract_mon_$period.tmp     $sdir/tmp_aggr/cover_fract_mon_$period.tmp      $sdir/out_aggr/cover_fract_mon_$period     || echo ERROR
  $cdo mul -gtc,5.e-5   $sdir/tmp_aggr/box_cover_fract_mon_$period.tmp $sdir/tmp_aggr/box_cover_fract_mon_$period.tmp  $sdir/out_aggr/box_cover_fract_mon_$period || echo ERROR
  $cdo setname,bare -sub -addc,1 -mulc,-1 $sdir/tmp_aggr/veg_ratio_max_mon_$period.tmp $sdir/out_aggr/glac_$period $sdir/out_aggr/bare_mon_$period            || echo ERROR
  #
  #-- veg_ratio_max and box_cover_fract (annual mean)
  #
  $cdo yearmonmean $sdir/out_aggr/veg_ratio_max_mon_$period   $sdir/out_aggr/veg_ratio_max_yr_$period || echo ERROR
  $cdo yearmonmean $sdir/out_aggr/box_cover_fract_mon_$period $sdir/out_aggr/box_cover_fract_yr_$period || echo ERROR
  rm -f $sdir/tmp_aggr/*$period.tmp
}; )&; }>$err.veg.type 2>&1

###########################################################################################################

wait

