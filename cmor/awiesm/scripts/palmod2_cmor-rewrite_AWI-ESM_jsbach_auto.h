#-- CMOR-rewrite for jsbach (ESM: AWI-ESM) Emon
cn='vegFrac'
for var in $cn; do
  { (if_requested $member $srfmod Emon $var $chunk || continue
  mkdir -p $dr/$submodel/Emon_${var}
  ifile=${sdir}/out_diag/Emon_${var}_$period.nc
  $cdo cmor,Emon,i=$it,mt=$mt,dr=$dr/$submodel/Emon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Emon 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: AWI-ESM) LImon
cn='sbl'
for var in $cn; do
  { (if_requested $member $srfmod LImon $var $chunk || continue
  mkdir -p $dr/$submodel/LImon_${var}
  ifile=${sdir}/out_diag/LImon_${var}_$period.nc
  $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.LImon 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: AWI-ESM) LImon
cn='snc'
find_file "$sdir" "*_${period}*_land" ifile >> $err.find_file.LImon 2>&1
for var in $cn; do
  { (if_requested $member $srfmod LImon $var $chunk || continue
  mkdir -p $dr/$submodel/LImon_${var}
  $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.LImon 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: AWI-ESM) Lmon
cn='c3PftFrac c4PftFrac cLitter cVeg fFire frivera gpp grassFrac landCoverFrac mrso npp rh shrubFrac treeFrac'
for var in $cn; do
  { (if_requested $member $srfmod Lmon $var $chunk || continue
  mkdir -p $dr/$submodel/Lmon_${var}
  ifile=${sdir}/out_diag/Lmon_${var}_$period.nc
  $cdo cmor,Lmon,i=$it,mt=$mt,dr=$dr/$submodel/Lmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Lmon 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: AWI-ESM) Lmon
cn='lai tran tsl'
find_file "$sdir" "*_${period}*_land" ifile >> $err.find_file.Lmon 2>&1
for var in $cn; do
  { (if_requested $member $srfmod Lmon $var $chunk || continue
  mkdir -p $dr/$submodel/Lmon_${var}
  $cdo cmor,Lmon,i=$it,mt=$mt,dr=$dr/$submodel/Lmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Lmon 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: AWI-ESM) Lmon
cn='baresoilFrac'
find_file "$sdir" "*_${period}*_veg" ifile >> $err.find_file.Lmon 2>&1
for var in $cn; do
  { (if_requested $member $srfmod Lmon $var $chunk || continue
  mkdir -p $dr/$submodel/Lmon_${var}
  $cdo cmor,Lmon,i=$it,mt=$mt,dr=$dr/$submodel/Lmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Lmon 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: AWI-ESM) dec
cn='sftgif sftlf'
for var in $cn; do
  { (if_requested $member $srfmod dec $var $chunk '???0123124' || continue
  mkdir -p $dr/$submodel/dec_${var}
  ifile=${sdir}/out_diag/dec_${var}_$period.nc
  $cdo cmor,dec,i=$it,mt=$mt,dr=$dr/$submodel/dec_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.dec 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: AWI-ESM) fx
cn='mrsofc'
for var in $cn; do
  { (if_requested $member $srfmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  ifile=${sdir}/out_diag/fx_$var.nc
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: AWI-ESM) fx
cn='rootd'
find_file "$sdir" "fx_rootd_${period}.nc" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $srfmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: AWI-ESM) fx
cn='sftlf'
find_file "$sdir" "fx_sftlf_${period}.nc" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $srfmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: AWI-ESM) fx
cn='sftgif'
find_file "$sdir" "glac_${period}" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $srfmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

