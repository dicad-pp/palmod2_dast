#-- Diagnostic for echam6 (ESM: AWI-ESM) variable evspsbl / table Amon
# Editor Note: *_DATE*_echam.nc|evap
{ (if_requested $member $atmmod Amon evspsbl $chunk && {
  find_file -e            "$sdir" "*_${period}*_echam" ifile
  $cdo -f nc -O \
    expr,'evspsbl=-1*var182;' \
    $ifile ${sdir}/out_diag/Amon_evspsbl_$period.nc || echo ERROR
}; )&; }>$err.evspsbl.Amon 2>&1

#-- Diagnostic for echam6 (ESM: AWI-ESM) variable pr / table Amon
# Editor Note: *_DATE*_echam.nc|aprl,aprc
{ (if_requested $member $atmmod Amon pr $chunk && {
  find_file -e            "$sdir" "*_${period}*_echam" ifile
  $cdo -f nc -O \
    expr,'pr=var142+var143;' \
    $ifile ${sdir}/out_diag/Amon_pr_$period.nc || echo ERROR
}; )&; }>$err.pr.Amon 2>&1

#-- Diagnostic for echam6 (ESM: AWI-ESM) variable rlds / table Amon
# Editor Note: *_DATE*_echam.nc|trads,tradsu
{ (if_requested $member $atmmod Amon rlds $chunk && {
  find_file -e            "$sdir" "*_${period}*_echam" ifile
  $cdo -f nc -O \
    expr,'rlds=var177-var205;' \
    $ifile ${sdir}/out_diag/Amon_rlds_$period.nc || echo ERROR
}; )&; }>$err.rlds.Amon 2>&1

#-- Diagnostic for echam6 (ESM: AWI-ESM) variable rsds / table Amon
# Editor Note: *_DATE*_echam.nc|srads,sradsu
{ (if_requested $member $atmmod Amon rsds $chunk && {
  find_file -e            "$sdir" "*_${period}*_echam" ifile
  $cdo -f nc -O \
    expr,'rsds=var176-var204;' \
    $ifile ${sdir}/out_diag/Amon_rsds_$period.nc || echo ERROR
}; )&; }>$err.rsds.Amon 2>&1

#-- Diagnostic for echam6 (ESM: AWI-ESM) variable snw / table LImon
{ (if_requested $member $atmmod LImon snw $chunk && {
  find_file -e            "$sdir" "*_${period}*_echam" ifile
  $cdo -f nc -O \
    expr,'snw=1000*var141;' \
    $ifile ${sdir}/out_diag/LImon_snw_$period.nc || echo ERROR
}; )&; }>$err.snw.LImon 2>&1

#-- Diagnostic for echam6 (ESM: AWI-ESM) variable mrros / table Lmon
{ (if_requested $member $atmmod Lmon mrros $chunk && {
  find_file -e            "$sdir" "*_${period}*_accw" ifile
  $cdo -f nc -O \
    expr,'mrros=var160-var161;' \
    $ifile ${sdir}/out_diag/Lmon_mrros_$period.nc || echo ERROR
}; )&; }>$err.mrros.Lmon 2>&1

#-- Diagnostic for echam6 (ESM: AWI-ESM) variable orog / table dec
# Editor Note: Alternatively, submit surface topography from the restart file. Updated every 10 years.
{ (if_requested $member $atmmod dec orog $chunk && {
  find_file -e            "$sdir" "*_${period}*_echam" ifile
  $cdo -f nc -O \
    yearmean \
    -expr,'orog=var129/9.81;' \
    $ifile ${sdir}/tmp_diag/dec_orog_$period.nc || echo ERROR
  if_requested $member $atmmod dec orog $chunk '???0123124' && {
    eval $cdo -O ensmean \
      ${sdir}/tmp_diag/dec_orog_{$((period-9))..${period}}.nc ${sdir}/out_diag/dec_orog_${period}.nc && \
      eval rm ${sdir}/tmp_diag/dec_orog_{$((period-9))..${period}}.nc || echo ERROR
}; }; )&; }>$err.orog.dec 2>&1

#-- Diagnostic for echam6 (ESM: AWI-ESM) variable areacella / table fx
{ (if_requested $member $atmmod fx areacella $chunk && {
  find_file -e            "$sdir" "*_${period}*_echam" ifile
  $cdo -f nc -O \
    expr,'areacella=gridarea(var129);' \
    $ifile ${sdir}/out_diag/fx_areacella.nc || echo ERROR
}; )&; }>$err.areacella.fx 2>&1

