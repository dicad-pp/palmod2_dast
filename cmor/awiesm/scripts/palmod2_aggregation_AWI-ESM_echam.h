#
#-- CMIP6 pressure axes
#
plev39="100000,92500,85000,70000,60000,50000,40000,30000,25000,20000,17000,
15000,13000,11500,10000,9000,8000,7000,5000,3000,2000,1500,1000,700,
500,300,200,150,100,70,50,40,30,20,15,10,7,5,3"
plev27="100000,97500,95000,92500,90000,87500,85000,82500,80000,77500,75000,
70000,65000,60000,55000,50000,45000,40000,35000,30000,25000,22500,
20000,17500,15000,12500,10000"
plev27and7h="100000,97500,95000,92500,90000,87500,85000,82500,80000,77500,75000,
70000,65000,60000,55000,50000,45000,40000,35000,30000,25000,22500,
20000,17500,15000,12500,10000,5000"
plev23="100000,92500,85000,70000,60000,50000,40000,30000,25000,20000,15000,10000,7000,5000,3000,2000,1000,700,500,300,200,100,40"
plev19="100000,92500,85000,70000,60000,50000,40000,30000,25000,20000,15000,
10000,7000,5000,3000,2000,1000,500,100"
plev19plus="100000,92500,85000,77500,70000,60000,50000,40000,30000,25000,20000,15000,10000,7000,5000,3000,2000,1000,700,500,300,200,100,50,20,10"
plev8="100000,85000,70000,50000,25000,10000,5000,1000"
plev7h="92500,85000,70000,60000,50000,25000,5000"
plev4="92500,85000,50000,25000"
plev3="85000,50000,25000"



################################
# ECHAM6 #######################
################################
suf=grb

#
# Afterburner vertical interpolation
#

  #-- interpolation onto plev19(plus): echam: mon: ta,ua,va,hus,wap,zg,hur
  cl_echammon_plev19="130,131,132,133,135,156,157"
  { (if_requested -s $member $atmmod echammonplev19 interpolation $chunk && {
   echo "aa | block-echam.mon.plev19        | begin $(date)">>$loadBal
   ifile=$sdir/${RAW_EXP_IDS[$y]}_${period}.01_echam
   ofile=$sdir/out_aggr/${RAW_EXP_IDS[$y]}_echam6_echammon_plev19_$period.$suf; rm -f $ofile
   $cdo afterburner $ifile $ofile <<EOF || echo ERROR
   &SELECT CODE=${cl_echammon_plev19},LEVEL=$plev19plus,FORMAT=1,TYPE=30 &end
EOF
   echo "aa | block-echam.mon.plev19        | end   $(date)">>$loadBal
   }; )& ; }>$err.echammon.plev19 2>&1

  #-- afterburner diagnostic: echam: mon: ua, va, psl
  { (if_requested -s $member $atmmod echammonafter afterburner $chunk && {
  echo "aa | block-echam.mon.after           | begin $(date)">>$loadBal
  ifile=$sdir/${RAW_EXP_IDS[$y]}_${period}.01_echam
  ofile=$sdir/out_aggr/${RAW_EXP_IDS[$y]}_echam6_echammon_after_$period.$suf; rm -f $ofile
  $cdo afterburner $ifile $ofile <<EOF || echo ERROR
  &SELECT CODE=131,132,134,151,FORMAT=1,TYPE=20 &end
EOF
  echo "aa | block-echam.mon.after           | end   $(date)">>$loadBal
  }; )& ; }>$err.echammon.after 2>&1



wait #-- for all operations to finish

