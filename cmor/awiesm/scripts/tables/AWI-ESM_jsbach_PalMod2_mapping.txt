!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Automatically created Variable-Mapping-Table for JSBACH
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Creation Date: 2022-09-07 17:45:11.769782 UTC
! Data Request Version: 00.00.03
! $URL$
! $Rev$  $Date$
!----------------------------------------------------------------------------------------
! This table contains information that has to be provided in order to use the CDO cmor operator.
! E.g. if your data files are GRIB formatted, the code number of the variable which
!   corresponds to the target variable indicated by the short (CMOR) name (cmor_name=)
!   needs to be given (with keyword 'code').
! The table format:
!  Each line has to start with '&parameter '
!  Comments start with an exclamation sign
!  The order of keyword assignments does not matter
!  There must be at least 1 space between assignments
!  Any keyword value may be written with double-quotes
!  If a keyword value contains spaces, it must be delimited by double-quotes
! Keywords interpreted by cdo cmor,... are :
!
! Entry            	Type 	Description
!----------------------------------------------------------------------------------------
! name                 STRING  Name of the variable in a NetCDF model output file
! code                 STRING  1 to 3-digit variable code (for GRIB files)
! cmor_name            WORD    CMIP/CMOR name of the target variable
!                              (this is not necessarily the output variable name!)
! character_axis       STRING  Standard (CMOR) name of a character axis
! param                STRING  GRIB parameter (GRIB1: code[.tabnum]; GRIB2: num[.cat[.dis]])
! units                STRING  Units (UDUNITS!) of the variable
! positive             WORD    'u' or 'd' for 'up' and 'down' flux direction
! variable_comment     STRING  Information concerning the target variable that will be
!                              transformed into a variable NetCDF attribute
! cell_methods         WORD    Information about the time:cell_methods of the target variable
! project_mip_table    WORD    MIP-table name of the target variable
! z-axis               WORD    vertical axis name of the target variable
!
! OtherAdditional keywords are allowed; they will be ignored by the cmor operator.
!
!----------------------------------------------------------------------------------------
&parameter cmor_name="snc"                          code="60"  project_mip_table="LImon"        units="1"             cell_methods="m"  /
&parameter cmor_name="baresoilFrac"                 code="35"  project_mip_table="Lmon"         units="1"             cell_methods="m" character_axis="typebare"  /
&parameter cmor_name="lai"                          code="107" project_mip_table="Lmon"         units="1"             cell_methods="m"  /
&parameter cmor_name="tran"                         code="76"  project_mip_table="Lmon"         units="kg m-2 s-1"    cell_methods="m" positive="d"  /
&parameter cmor_name="tsl"                          code="68"  project_mip_table="Lmon"         units="K"             cell_methods="m" z_axis="sdepth"      /
&parameter cmor_name="sftgif"                       name="glac"                                    project_mip_table="dec"          units="1"             cell_methods="m"  /
&parameter cmor_name="sftlf"                        name="var25"                                   project_mip_table="dec"          units="1"             cell_methods="m"  /
&parameter cmor_name="rootd"                        name="root_depth"                              project_mip_table="fx"           units="m"             cell_methods="n"  /
&parameter cmor_name="sftgif"                       name="glac"                                    project_mip_table="fx"           units="1"             cell_methods="n"  /
&parameter cmor_name="sftlf"                        name="slm"                                     project_mip_table="fx"           units="1"             cell_methods="n"  /
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Diagnosed Variables incl character dimension !
! listed in the following:                     !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
&parameter cmor_name="vegFrac"                                                                     project_mip_table="Emon"         units="1"             cell_methods="m" character_axis="typeveg"  variable_comment="Grass fracture includes natural grass and pasture."  /
&parameter cmor_name="c3PftFrac"                                                                   project_mip_table="Lmon"         units="1"             cell_methods="m" character_axis="typec3pft"  /
&parameter cmor_name="c4PftFrac"                                                                   project_mip_table="Lmon"         units="1"             cell_methods="m" character_axis="typec4pft"  /
&parameter cmor_name="grassFrac"                                                                   project_mip_table="Lmon"         units="1"             cell_methods="m" character_axis="typenatgr" variable_comment="Grass fracture for natural grass only."  /
&parameter cmor_name="landCoverFrac"                                                               project_mip_table="Lmon"         units="1"             cell_methods="m" character_axis="vegtype"   /
&parameter cmor_name="shrubFrac"                                                                   project_mip_table="Lmon"         units="1"             cell_methods="m" character_axis="typeshrub"  /
&parameter cmor_name="treeFrac"                                                                    project_mip_table="Lmon"         units="1"             cell_methods="m" character_axis="typetree"  /
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Diagnosed Variables listed in the following: !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
&parameter cmor_name="sbl"                                     project_mip_table="LImon"        units="kg m-2 s-1"    cell_methods="m"  /
&parameter cmor_name="cLitter"                                 project_mip_table="Lmon"         units="kg m-2"        cell_methods="m" variable_comment="Only the yasso above ground litter pools are considered here, as carbon from below ground litter pools is counted as soil carbon. Thus a further subdivision into cLitterAbove and cLitterBelow is not possible."  /
&parameter cmor_name="cVeg"                                    project_mip_table="Lmon"         units="kg m-2"        cell_methods="m"  /
&parameter cmor_name="fFire"                                   project_mip_table="Lmon"         units="kg m-2 s-1"    cell_methods="m" positive="u"  /
&parameter cmor_name="frivera"                                 project_mip_table="Lmon"         units="kg m-2 s-1"    cell_methods="m"  /
&parameter cmor_name="gpp"                                     project_mip_table="Lmon"         units="kg m-2 s-1"    cell_methods="m" variable_comment="This is potential GPP. It is calculated before the actual plant demand is determined. Autotrophic respiration (ra) is calculated from the difference of (potential) GPP and actual NPP."  /
&parameter cmor_name="mrso"                                    project_mip_table="Lmon"         units="kg m-2"        cell_methods="m"  /
&parameter cmor_name="npp"                                     project_mip_table="Lmon"         units="kg m-2 s-1"    cell_methods="m" positive="d"  /
&parameter cmor_name="rh"                                      project_mip_table="Lmon"         units="kg m-2 s-1"    cell_methods="m" positive="u"  /
&parameter cmor_name="mrsofc"                                  project_mip_table="fx"           units="kg m-2"        cell_methods="n"  /
