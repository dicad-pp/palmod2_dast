#
# experiment info:
#
_CONTROLLED_VOCABULARY_FILE=PalMod2_CV.json
_FORMULA_VAR_FILE=PalMod2_formula_terms.json
_AXIS_ENTRY_FILE=PalMod2_coordinate.json
CONVENTIONS="CF-1.7 PalMod-2.0"
TITLE="AWI-ESM-2-1 output prepared for PalMod2"
_history_template="%s ; CMOR rewrote data to be consistent with <activity_id>, <Conventions> and CF standards."

EXPERIMENT_ID=transient-deglaciation-prescribed-glac1d
EXPERIMENT="transient deglaciation with GLAC-1D ice sheets"
#Specify VARIANT_LABEL explicitly or realization_index, physics_index, initialization_index and forcing_index:
VARIANT_LABEL=r1i1p1f1
REALIZATION_INDEX=1
INITIALIZATION_INDEX=1
PHYSICS_INDEX=1
FORCING_INDEX=1
VARIANT_INFO="forcing: greenhouse gases: Köhler, Peter; Nehrbass-Ahles, Christoph; Schmitt, Jochen; Stocker, Thomas F; Fischer, Hubertus (2017): Continuous record of the atmospheric greenhouse gas carbondioxide (CO2), final spline-smoothed data of calculated radiative forcing (Version 2). PANGAEA, https://doi.or/10.1594/PANGAEA.876013, In supplement to: Köhler, P et al. (2017): A 156 kyr smoothed history of the atmospheric greenhouse gases CO2, CH4, and N2O and their radiative forcing. Earth System Science Data, 9(1), 363-387, https://doi.org/10.5194/essd-9-363-2017 orbital parameters: Berger Program"
ACTIVITY_ID=PalMod2
#MIP_ERA=PalMod2                   #ONLY CMIP6  
PROJECT_ID=PalMod2
REQUIRED_TIME_UNITS="days since 1-1-1 00:00:00"
#FURTHER_INFO_URL="https://furtherinfo.es-doc.org/<mip_era>.<institution_id>.<source_id>.<experiment_id>.<sub_experiment_id>.<variant_label>
#Branch Info: If needed use the following 2 attributes to define the branch time
#  in Parent_Time_Units and Required_Time_Units (=child)
#  else use BRANCH_DATES to give the dates in YYYYMMDD,YYYYMMDD format
#BRANCH_TIME_IN_PARENT=
#BRANCH_TIME_IN_CHILD=
#BRANCH_DATES=19000101,18500101 
#BRANCH_METHOD="standard"
#PARENT_MIP_ERA=none              #ONLY CMIP6
#PARENT_ACTIVITY_ID=none
#PARENT_EXPERIMENT=none
#cmip6-spinup-HR
#PARENT_EXPERIMENT_ID=none
#PARENT_SOURCE_ID=none
#PARENT_TIME_UNITS="days since 1850-1-1 00:00:00"
#PARENT_VARIANT_LABEL=none
SUB_EXPERIMENT_ID=none
SUB_EXPERIMENT=none
#
#model info:
#
SOURCE_ID=AWI-ESM-2-1-LR
#REFERENCES="None"
SOURCE="AWI-ESM 2.1 LR (2019): \naerosol: none\natmos: ECHAM6.3.04p1 (T63L47 native atmosphere T63 gaussian grid; 192 x 96 longitude/latitude; 47 levels; top level 80 km)\natmosChem: none\nland: JSBACH 3.20 with dynamic vegetation\nlandIce: none\nocean: FESOM 2 (unstructured grid in the horizontal with 126858 wet nodes; 48 levels; top grid cell 0-5 m)\nocnBgchem: none\nseaIce: FESOM 2\nsolidLand: none/prescribed"
SOURCE_TYPE="AOGCM"
CALENDAR=proleptic_gregorian
#MAPPING_TABLE_DIR=cmip6_mapping_tables
#GRID_FILE_DIR=__GRID_FILE_DIR
#GRID_FILE=__GRID_FILE
GRID=gn
GRID_LABEL=gn
#NOMINAL_RESOLUTION read from external file
#For FESOM2
#NOMINAL_RESOLUTION="500 km"
#For ECHAM6/JSBACH
#NOMINAL_RESOLUTION="500 km"
#
CHAR_AXIS_VEGTYPE=bare_land,glacier,tropical_evergreen_trees,tropical_deciduous_trees,extra-tropical_evergreen_trees,extra-tropical_deciduous_trees,raingreen_shrubs,deciduous_shrubs,C3_grass,C4_grass,C3_pasture,C4_pasture,C3_crops,C4_crops
CHAR_AXIS_SOILPOOLS=acid-soluble_below_ground_leaf_litter,acid-soluble_below_ground_woody_litter,water-soluble_below_ground_leaf_litter,water-soluble_below_ground_woody_litter,ethanol-soluble_below_ground_leaf_litter,ethanol-soluble_below_ground_woody_litter,non-soluble_below_ground_leaf_litter,non-soluble_below_ground_woody_litter,humus_from_leaf_litter,humus_from_woody_litter
CHAR_AXIS_LANDUSE=primary_and_secondary_land,crops,pastures,urban
CHAR_AXIS_BASIN=global_ocean,atlantic_arctic_ocean,indian_pacific_ocean
CHAR_AXIS_OLINE=barents_opening,bering_strait,canadian_archipelago,denmark_strait,drake_passage,english_channel,pacific_equatorial_undercurrent,faroe_scotland_channel,florida_bahamas_strait,fram_strait,iceland_faroe_channel,indonesian_throughflow,mozambique_channel,taiwan_luzon_straits,windward_passage
CHAR_AXIS_SILINE=barents_opening,bering_strait,canadian_archipelago,fram_strait
#spectband for the 3 aeropt variables
CHAR_AXIS_SPECTBAND_aeroptbnd=1818182
CHAR_AXIS_SPECTBAND_aeroptbnd_BOUNDS=2262443,1600000
CHAR_AXIS_SPECTBAND_aeroptbnd_UNITS="m-1"
CHAR_AXIS_SPECTBAND_aerssabnd=1818182
CHAR_AXIS_SPECTBAND_aerssabnd_BOUNDS=2262443,1600000
CHAR_AXIS_SPECTBAND_aerssabnd_UNITS="m-1"
CHAR_AXIS_SPECTBAND_aerasymbnd=1818182
CHAR_AXIS_SPECTBAND_aerasymbnd_BOUNDS=2262443,1600000
CHAR_AXIS_SPECTBAND_aerasymbnd_UNITS="m-1"
# 2d-spectband for the albedos
CHAR_AXIS_SPECTBAND_albdirbnd=3225689,766690
CHAR_AXIS_SPECTBAND_albdirbnd_BOUNDS=5000000,1451379,1451379,82001
CHAR_AXIS_SPECTBAND_albdirbnd_UNITS="m-1"
CHAR_AXIS_SPECTBAND_albdiffbnd=3225689,766690
CHAR_AXIS_SPECTBAND_albdiffbnd_BOUNDS=5000000,1451379,1451379,82001
CHAR_AXIS_SPECTBAND_albdiffbnd_UNITS="m-1"
CHAR_AXIS_SPECTBAND_solbnd=1818182
CHAR_AXIS_SPECTBAND_solbnd_BOUNDS=2262443,1600000
CHAR_AXIS_SPECTBAND_solbnd_UNITS="m-1"
#T_AXIS="cmip" causes the time axis to be rewritten according to the requested time stamps
T_AXIS="cmip"
#OUTPUT_MODE="r"
OUTPUT_MODE="a"
MAX_SIZE=0
SAVE_CHUNK="n"
#For files without time axis, if one is requested:
#firsttimeval=1 (1850-01-02 00:00:00 wenn days since 1850-01-01 00:00:00)
FIRSTTIMEVAL=1
#
# institution and contact info:
#
# none of these can be set in the command line
#
DEFLATE_LEVEL=1
CONTACT=palmod2-awi-esm@dkrz.de
#
# AWI
#
INSTITUTION_ID=AWI
INSTITUTION="Alfred Wegener Institute, Helmholtz Centre for Polar and Marine Research, Am Handelshafen 12, 27570 Bremerhaven, Germany"
LICENSE="PalMod2 model data produced by AWI is licensed under a Creative Commons Attribution ShareAlike 4.0 International License (https://creativecommons.org/licenses). The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law."
#MIP_TABLE_DIR="/pool/data/CMIP6/cmip6-cmor-tables/Tables/"
MIP_TABLE_DIR="/work/bm0021/PalMod2/cmor_tables/"
