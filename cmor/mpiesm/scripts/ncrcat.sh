cdirs=$(find . -maxdepth 1 -name "cmor*" -type d)

for cdir in $cdirs; do
  psfiles=( $( find $cdir/echam6/Amon_ps -type f ) )
  l=$( echo $cdir | cut -d "/" -f 2 | cut -d "_" -f2 | cut -d "-" -f1)
  u=$( echo $cdir | cut -d "/" -f 2 | cut -d "_" -f2 | cut -d "-" -f2)
  base=$(basename ${psfiles[0]} | rev | cut -d "_" -f 2- | rev)
  #echo $cdir
  #echo $l $u
  echo ${base}_$(printf "%05d" $l )01-$(printf "%05d" $u )12.nc
  ncrcat -h ${psfiles[@]} ${base}_$(printf "%05d" $l )01-$(printf "%05d" $u)12.nc || echo "ERROR ${base}_$(printf "%05d" $l )01-$(printf "%05d" $u )12.nc"
  #break
done
