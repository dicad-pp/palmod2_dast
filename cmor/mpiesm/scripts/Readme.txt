Launch complete cmorisation (all 26000-spinup simulation years!) by running
>sbatch transient_deglaciation_prescribed_glac1d_r1i1p1f1-CR.cmor_ctl

!!   Make sure the spinup parameter is set to the same value in the
!!    *.cmor_ctl and *.runpp scripts

Launch cmorization in parts by directly submitting transient_deglaciation_prescribed_glac1d_r1i1p1f1-CR.runpp:
eg. for the first 400 years of the first subsimulation (spinup=1000, iniyeararr[0]=4000:
>sbatch transient_deglaciation_prescribed_glac1d_r1i1p1f1-CR.runpp 0 0 5000 5399
eg. for the last 400 years of the first subsimulation and the first 400 years of the 2nd subsimulation
 (finyeararr[0]=8999, iniyeararr[1]=2000)
>sbatch transient_deglaciation_prescribed_glac1d_r1i1p1f1-CR.runpp 0 1 8600 2399
Note: If your YYYY string does not end with ???9 or not start with ???0 decadal variables might be missing 
      for parts of the first and last decade of your selected range of simulation years.

