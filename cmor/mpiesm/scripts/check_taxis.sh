#!/bin/bash

##################################
# Martin Schupfner, DKRZ, 2021
#
# Clear a cdo-cmor created archive 
# from incompletely written files
# and output files of incompletely 
# standardized time intervals. 
##################################

function print_usage {
  echo "Run as 'bash check_taxis.sh  <archive_dir>'"
  echo "  archive_dir: archive parent directory of the CMOR directory structure (DRS)."
  exit 1
}

if [[ -z "$1" ]]; then
  print_usage
else
  if [[ ! -d $1 ]]; then
    echo "The directory "$1" does not exist!"
    print_usage
  fi
fi
archive=$1 

startyear=1
#startyear=2001
#startyear=1850
#startyear=7001

chunking=100
gap=0

endyear=25000
#endyear=3049
#endyear=14500

timestepformat="%05d"
#timestepformat="%04d"

declare -a missing_files
declare -a additional_files



# Find empty folders
dst=( $(find $archive -type d -empty | sort -u ) )
echo "------------------------------------------------"
echo "${#dst[@]} empty directories found"
echo "------------------------------------------------"
printf '%s\n' "${dst[@]}"



# Find all datasets
dst=( $(find $archive -name "gn" -type d | sort -u ) )
echo
echo "------------------------------------------------"
echo "${#dst[@]} datasets found"
echo "------------------------------------------------"
echo



# Find all netCDF files for each dataset
#  - list missing files
#  - list additional files
num=0
for ds in ${dst[@]}; do
num=$((num+1))
[[ "$ds" == *"/fx/"* ]] || [[ "$ds" == *"/Ofx/"* ]] && {

ncf=( $(find $ds -name "*.nc" -type f | sort -u ) )
[[ ${#ncf[@]} -lt 1 ]] && echo "ERROR: No netCDF file found for $ds"
echo "------------------------------------------------"
echo "$num $(basename ${ncf[0]} | cut -d "_" -f 1-2)   #${#ncf[@]}"
echo "------------------------------------------------"
echo
continue

}

ncf=( $(find $ds -name "*.nc" -type f | sort -u ) )

ncfs=$( echo ${ncf[1]} | rev | cut -d "_" -f2- | rev )

declare -a ncf_tgt=()
declare -a missing=()
declare -a additional=()



# mon *_gn_0000101-0010012.nc - *_gn_YYYYY01-{YYYYY+9}12.nc
if [[ "$ds" == *"mon/"* ]]; then

for i in $(seq $startyear $chunking $endyear); do
lb=$(printf "$timestepformat" $i )
ub=$(printf "$timestepformat" $((i+${chunking}-${gap}-1)))
ncf_tgt+=( ${ncfs}_${lb}01-${ub}12.nc )
done # seq mon
#printf '%s\n' "${ncf_tgt[@]}"
#exit 0

# dec *_gn_00001-00100.nc - *_gn_YYYYY-{YYYYY+9}.nc
elif [[ "$ds" == *"dec/"* ]]; then

for i in $(seq $startyear $chunking $endyear); do
lb=$(printf "$timestepformat" $i )
ub=$(printf "$timestepformat" $((i+${chunking}-${gap}-1)))
ncf_tgt+=( ${ncfs}_${lb}-${ub}.nc )
done # seq dec
#printf '%s\n' "${ncf_tgt[@]}"
#exit 0

else
  echo "ERROR: $ds - frequency could not be identified!"
  exit 1
fi # if frequency



[[ ${#ncf[@]} -lt 1 ]] && echo "ERROR: No netCDF file found for $ds"
echo "------------------------------------------------"
echo "$num $(basename ${ncf[0]} | cut -d "_" -f 1-2)   #${#ncf[@]}"
echo "------------------------------------------------"



# Find missing files
for f in ${ncf_tgt[@]}; do
if [[ " ${ncf[@]} " == *" $f "* ]]; then
:
else
missing+=( $f )
fi
done # do missing

echo "  MISSING"
printf '%s\n' "${missing[@]}"



# Find additional files
for f in ${ncf[@]}; do
if [[ " ${ncf_tgt[@]} " == *" $f "* ]]; then
:
else
additional+=( $f )
fi
done # do additional

echo "  ADDITIONAL"
printf '%s\n' "${additional[@]}"

done # ds in dst
exit 0


echo
echo "------------------------------------------------"
echo "------------------------------------------------"
echo "------------------------------------------------"
echo "                    SUMMARY"
echo "------------------------------------------------"
echo "------------------------------------------------"
echo "------------------------------------------------"
echo
echo "------------------------------------------------"
echo "               MISSING  FILES"
echo "------------------------------------------------"
printf '%s\n' "${missing_files[@]}"
echo
echo "------------------------------------------------"
echo "           ADDITIONAL  FILES"
echo "------------------------------------------------"
printf '%s\n' "${additional_files[@]}"

