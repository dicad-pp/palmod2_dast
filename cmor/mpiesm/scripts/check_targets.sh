njobs=250
#njobs=12


#for exp in ssp119-future-methane ssp245-future-methane ssp126-future-methane ssp370-future-methane ssp585-future-methane; do
#for exp in transient-deglaciation-prescribed-glac1d-methane; do
for exp in transient-deglaciation-prescribed-glac1d; do
#for exp in transient-deglaciation-prescribed-ice6g; do

for simu in r1i1p1f1 r1i1p2f2 r1i1p3f2; do
#for simu in r1i1p1f1; do

echo 
echo $exp $simu
echo "---------"

for i in $(seq 1 $njobs); do
[[ ! -e targets/${exp}_${simu}-CR.aggr.$i ]] && echo ${exp}_${simu}-CR.aggr.$i
done

for i in $(seq 1 $njobs); do
[[ ! -e targets/${exp}_${simu}-CR.diags.$i ]] && echo ${exp}_${simu}-CR.diags.$i
done

for i in $(seq 1 $njobs); do
[[ ! -e targets/${exp}_${simu}-CR.cmor.$i ]] && echo ${exp}_${simu}-CR.cmor.$i
done


done
done
