#!/bin/bash

#
# Correct vertical axis for mPISM
#  - in this case used to create
#    gridinfo file serving as input
#    for cdo cmor
#

ifile=/work/bm0021/PalMod2/cmor/mpiesm/scripts/gridinfo_mPISM_L121.nc_source
ofile=/work/bm0021/PalMod2/cmor/mpiesm/scripts/gridinfo_mPISM_L121.nc

function ncattget { ncks --trd -M -m ${3} | grep -E -i "^${2} attribute [0-9]+: ${1}" | cut -f 11- -d ' ' | sort ; }



bnds_0=""
bnds_1=""
lev_val=""

levs=$(ncks --trd -m -M $ifile | grep -E -i ": lev, size =" | cut -f 7 -d ' ' | uniq ;)
[[ ! -z "$levs" ]] && [[ $levs -eq 121 ]] && { [[ -z "$bnds_1" ]] || [[ -z "$bnds_0" ]]; } && {
    lev=( $(ncks -s "%.2f " -Q -H -v lev $ifile |  cut -d$'\n' -f1 ) 6050.00 )
    bnd_i=0
    echo Level interfaces read: ${lev[@]}
    for i in ${lev[@]}; do
        if [[ $bnd_i -eq 121 ]]; then continue; fi
        if [[ $bnd_i -eq 0 ]]; then
            #indp1=$((bnd_i+1))
            lev_val=$( awk "BEGIN { a = ${lev[$bnd_i]}; b = ${lev[$((bnd_i+1))]}; print (a+b)/2. }" )
            bnds_0=${lev[$bnd_i]}
            bnds_1=${lev[$((bnd_i+1))]}
	else
            lev_val=${lev_val},$( awk "BEGIN { a = ${lev[$bnd_i]}; b = ${lev[$((bnd_i+1))]}; print (a+b)/2. }" )
            bnds_0=${bnds_0},${lev[$bnd_i]}
            bnds_1=${bnds_1},${lev[$((bnd_i+1))]}
        fi
        bnd_i=$((bnd_i+1))
    done
    echo "Layer midpoints[:]:" $lev_val
    echo "Bounds[:,1]:" $bnds_0
    echo "Bounds[:,2]:" $bnds_1
}
#exit 0

add_bounds() {
  if [[ -z "$bnds_1" ]] || [[ -z "$bnds_0" ]]; then echo "ERROR: No bounds specified"; exit 0; fi
  if [[ -z "$lev_val" ]]; then echo "ERROR: No layer midpoints specified"; exit 0; fi
  echo
  echo "---------------------------------------------------"
  echo "$ifile"
  echo "Setting lev and lev_bnds ..."
  ncap2 -A -h -s "lev(:)={$lev_val};lev_bnds(:,0)={$bnds_0};lev_bnds(:,1)={$bnds_1};" $ifile $ofile && echo "Successful." || { echo "ERROR: Setting lev and lev_bnds failed!" && exit 0; }
  echo
  echo "Set the following lev_bnds:"
  ncdump -v lev_bnds $ofile | grep -v "}" | grep -A 1000 "lev\_bnds ="
  echo "Set the following lev:"
  ncdump -v lev $ofile | grep -v "lev = 121" | grep -v "}" | grep -A 1000 "lev ="
}

add_bounds


#ncap2 -A -h -s "depth_bnds(:,0)={$bnds_0};depth_bnds(:,1)={$bnds_1};" mrsol.nc mrsol.nc


#ncap2 -O -s "defdim(\"bnds\",2);depth_bnds[depth,bnds]=0.;depth_bnds(:,0)={$bnds_0};depth_bnds(:,1)={$bnds_1};" Emon_mrsol_2015.nc b.nc
#ncatted -O -a bounds,depth,c,c,"depth_bnds" b.nc
#/work/bm0021/cdo_incl_cmor/cdo_2019-07-01_cmor3_gcc/src/cdo cmor,Emon,i=cdocmorinfo,mt=ssp126_r1i1p1f1-HR/scripts/tables/MPI-ESM1-2-MiKlip_jsbach_CMIP6_mapping.txt,dr=./,vd=v20190710,cn=mrsol b.nc


#ncap2 -O -s "defdim(\"bnds\",2);depth_bnds[depth,bnds]=0.;depth_bnds(:,0)={$bnds_0};depth_bnds(:,1)={$bnds_1};" Emon_mrsol_2015.nc b.nc
#ncatted -O -a bounds,depth,c,c,"depth_bnds" b.nc
#/work/bm0021/cdo_incl_cmor/cdo_2019-07-01_cmor3_gcc/src/cdo cmor,Emon,i=cdocmorinfo,mt=ssp126_r1i1p1f1-HR/scripts/tables/MPI-ESM1-2-MiKlip_jsbach_CMIP6_mapping.txt,dr=./,vd=v20190710,cn=mrsol b.nc
