################################
# MPIOM ########################
################################

#--mpiom: fx and masks
#{ ( { if_requested $member $ocemod mpiom fx $chunk || if_requested $member $ocemod mpiom gridfile $chunk ; } || continue
#    find_file "$rawsdir" "*_mpiom_fx_${ocemod_fxyear}*" ifile
#    ln -sf $ifile $sdir/mpiom_fx.nc || echo ERROR fx file
#    # In case these land-sea-masks change with time, this needs an update...
#    $cdo selname,amsuo,amsue $ifile $sdir/grid_mpiom_fx.nc || echo ERROR grid file
#)&; }>>$err.mpiom.fx 2>&1

#--mpiom: basin
#{ ( if [[ ! -s $sdir/${RAW_EXP_IDS[$y]}_mpiom_basin.nc ]]; then
#  cp /pool/data/MPIOM/input/r0010/GR30/GR30_basin.nc $sdir/${RAW_EXP_IDS[$y]}_mpiom_basin.nc || echo ERROR
#else
#  echo "MPIOM BASIN FX file exists!"
#fi
#)&; }>>$err.mpiom.basin 2>&1

#-- mpiom: msftmz / MOC
# Workaround 1 - fails as 5 dimensional variables can only be created by cdo cmor, not the standard cdo
#{ ( if_requested $member $ocemod Omon msftmz $chunk || continue
#  find_file "$sdir" "*_mpiom_data_moc_mm_${period}*" ifile >> $err.find_file.Omon 2>&1
#  $cdo -f nc setlevel,1 -setltype,70 -setname,moc -selname,global_moc       $ifile    ${sdir}/tmp_aggr/global_moc_$period.nc     || echo ERROR
#  $cdo -f nc setlevel,2 -setltype,70 -setname,moc -selname,atlantic_moc     $ifile    ${sdir}/tmp_aggr/atlantic_moc_$period.nc || echo ERROR
#  $cdo -f nc setlevel,3 -setltype,70 -setname,moc -selname,indopacific_moc  $ifile    ${sdir}/tmp_aggr/indopacific_moc_$period.nc || echo ERROR
#  $cdo -f nc -O merge \
#    ${sdir}/tmp_aggr/global_moc_$period.nc \
#    ${sdir}/tmp_aggr/atlantic_moc_$period.nc \
#    ${sdir}/tmp_aggr/indopacific_moc_$period.nc \
#    ${sdir}/out_aggr/msftmz_Omon_$period.nc && \
#  rm -f ${sdir}/tmp_aggr/global_moc_$period.nc \
#        ${sdir}/tmp_aggr/atlantic_moc_$period.nc \
#        ${sdir}/tmp_aggr/indopacific_moc_$period.nc || echo ERROR
#)&; }>>$err.msftmz.Omon 2>&1

#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) Omon
{ (if_requested $member $ocemod Omon msftmz $chunk || continue
  find_file "$sdir" "*_mpiom_data_moc_mm_${period}*" ifile 
  $cdo selname,global_moc,atlantic_moc,indopacific_moc ${cdochain-} $ifile ${sdir}/out_aggr/msftmz_Omon_$period.nc || echo ERROR
)&; }>>$err.msftmz.Omon 2>&1

# sftof
{ ( { if_requested $member $ocemod Odec sftof $chunk || if_requested $member $ocemod Ofx sftof $chunk ; } || continue
  find_file "$sdir" "*_mpiom_data_2d_mm_${period}*.nc" ifile >> $err.find_file.Odec 2>&1
  ofile=$sdir/out_aggr/weto_${period}.nc
  $cdo setcode,171 -expr,"weto=sos/sos" -seltimestep,1 -selname,sos $ifile $ofile || echo ERROR
)&; }>>$err.Odec.sftof 2>&1


# areacello
{ (if_requested $member $ocemod Ofx areacello $chunk || continue
  find_file "$sdir" "*_mpiom_data_2d_mm_${period}*.nc" ifile >> $err.find_file.Ofx 2>&1 
  gfile=$sdir/tmp_aggr/Ofx_areacello.grid
  ofile=$sdir/out_aggr/Ofx_areacello.nc
  $cdo selname,sos $ifile ${ofile}_sos || echo ERROR
  $cdo griddes ${ofile}_sos > $gfile || echo ERROR
  # !!! in newer cdo versions, the bounds will be deleted using sethalo,-1,-1 !!!
  $cdo sethalo,-1,-1 -selcode,1 $ifile ${ofile}_tmp || echo ERROR
  $cdo setgrid,${gfile} -setname,areacello -sethalo,1,1 -gridarea ${ofile}_tmp $ofile && rm -f ${ofile}_tmp ${ofile}_sos $gfile || echo ERROR
)&; }>>$err.Ofx.areacello 2>&1

{ ( { if_requested $member $ocemod Omon areacello $chunk || if_requested $member $ocemod Ofx areacello $chunk ; } || continue
  find_file "$sdir" "*_mpiom_data_2d_mm_${period}*.nc" ifile >> $err.find_file.Omon 2>&1
  gfile=$sdir/tmp_aggr/Omon_areacello_$period.grid
  ofile=$sdir/out_aggr/Omon_areacello_$period.nc
  $cdo selname,sos $ifile ${ofile}_sos || echo ERROR
  $cdo griddes ${ofile}_sos > $gfile || echo ERROR
  $cdo sethalo,-1,-1 -selcode,1 $ifile ${ofile}_tmp || echo ERROR
  $cdo setgrid,${gfile} -setname,areacello -sethalo,1,1 -gridarea ${ofile}_tmp $ofile && rm -f ${ofile}_tmp ${ofile}_sos $gfile || echo ERROR
)&; }>>$err.Omon.areacello 2>&1


wait

