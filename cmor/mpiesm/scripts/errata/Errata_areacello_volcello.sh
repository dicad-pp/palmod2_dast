#!/bin/bash
### Batch Queuing System is SLURM
#SBATCH --partition=compute
#SBATCH --nodes=1
#SBATCH --mem=200G
#SBATCH --time=05:00:00
#SBATCH --mail-type=FAIL
#SBATCH --account=bk1192
#SBATCH --output=SIMU%j.log
#SBATCH --error=SIMU%j.log

# Disable core file creation
ulimit -c 0

# Set cdo cmor outmode to "r" (replace) either in command line or in cdocmorinfo-file!
#cdo="/work/bm0021/cdo_incl_cmor/cdo-2022-05-11_cmor3.6.0_gcc/bin/cdo"
cdo="/work/bm0021/cdo_incl_cmor/cdo-2022-09-20_cmor3.6.0_gcc/bin/cdo"


#exp=transient-deglaciation-prescribed-glac1d
exp=transient-deglaciation-prescribed-ice6g
simu=r1i1p1f1
simu=r1i1p2f2
simu=r1i1p3f2
res=CR

# specify input for fx_areacello
areacello=/work/bk1192/k204212/palmod_thirdBatch/${exp}_${simu}-${res}/archive/PalMod2/MPI-M/MPI-ESM1-2-${res}/${exp}/${simu}/Ofx/areacello/gn/areacello_Ofx_MPI-ESM1-2-${res}_${exp}_${simu}_gn.nc
# specify input folder for Omon_thkcello
ifolder=/work/bk1192/k204212/palmod_secondBatch/${exp}_${simu}-${res}/archive/PalMod2/MPI-M/MPI-ESM1-2-${res}/${exp}/${simu}/Omon/thkcello/gn/ #v20220222

# output will be written to (incl. DRS)
outpath=/work/bk1192/k204212/errata/volcello
mkdir -p $outpath

# specify here all cdocmorinfo files to be read
it="/work/bm0021/PalMod2/cmor/mpiesm/scripts/${exp}_${simu}-${res}_cdocmorinfo,/work/bm0021/PalMod2/cmor/mpiesm/scripts/cdocmorinfo_mpiom_${res}"

infiles_thkcello=( $( find $ifolder -name "thkcello_Omon_*.nc" -type f ) )
echo "#####################################"
echo "# Errata volcello:"
[[ -e $areacello ]] && { echo "# Inputfile for areacello: found at '$areacello'" ; } || { echo "No inputfile found for areacello at $areacello !" ; exit 1 ; }
[[ ${#infiles_thkcello[@]} -ne 0 ]] && { echo "# Inputfiles for thkcello: ${#infiles_thkcello[@]} found at '$ifolder'" ; } || { echo "ERROR: No inputfiles found for thkcello!" ; exit 1 ; }
echo "#####################################"

maxjobs=10
n=0
for ifile in ${infiles_thkcello[@]}; do
  (
  echo $n: $ifile
  infilebase=$( basename $ifile )
  echo $n: $infilebase
  #Call cdo cmor, vd is the new version of the data
  $cdo cmor,Omon,i=$it,dr="$outpath",vd="v20220222",cn="volcello",cm="m",om="r",u="m3" -expr,"volcello=thkcello*areacello" -merge -selname,thkcello $ifile -selname,areacello $areacello || echo "ERROR $n: $infilebase"
  #break #if testing
  )&
  if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
     wait # wait until all have finished (not optimal, but most times good enough)
     #echo $n wait
  fi
done
wait

echo
echo "Done."

