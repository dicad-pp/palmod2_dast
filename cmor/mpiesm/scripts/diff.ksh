#!/bin/ksh
### Batch Queuing System is SLURM
#SBATCH --partition=compute,compute2,prepost
#SBATCH --nodes=1
#SBATCH --mem=200G
#SBATCH --time=00:30:00
#SBATCH --mail-type=FAIL
#SBATCH --account=bm0021
#SBATCH --qos=esgf
#SBATCH --exclusive
#SBATCH --output=diff_%j.log
#SBATCH --error=diff_%j.log


cmor1=/work/bm0021/PalMod2/cmor/transient_deglaciation_prescribed/archive
cmor2=/work/bm0021/PalMod2/cmor/transient_deglaciation_prescribed/archive2

cdo=/sw/rhel6-x64/cdo/cdo-1.9.8-magicsxx-gcc64/bin/cdo
diff=/scratch/k/k204212/test_eday/logs/diff
err=/scratch/k/k204212/test_eday/logs/errors.txt


mkdir -p $diff

rm -f $diff/*.txt
rm -f $err

files_cmor=($( find $cmor1 -type f))

n=0
maxjobs=20

# CMOR
for f in ${files_cmor[@]}; do
  { (
  echo $f >> $err
  fx=$cmor2/$(echo $f | sed "s,$cmor1,,g" )
  $cdo diff $f $fx
  )&; }>>$diff/cmor_$(echo $f | rev | cut -d"/" -f1 | rev | cut -d"_" -f1-5).txt 2>>$err
  # limit jobs
  if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
   wait # wait until all have finished (not optimal, but most times good enough)
   #echo $n wait
  fi
done
wait

a=($(find $diff -not -empty -type f))
echo >> $err
echo "-----------------------------------" >> $err
echo "  Differences occured" >> $err
echo "-----------------------------------" >> $err
for f in ${a[@]}; do
  echo "$(echo $f | rev | cut -d"/" -f1 | rev | cut -d"." -f1)        $f" >> $err
done
echo "" >> $err

a=( $(cat $err | grep "Open failed" | rev | cut -d " " -f1 | rev ) )
echo "-----------------------------------" >> $err
echo "  File not produced" >> $err
echo "-----------------------------------" >> $err
for f in ${a[@]}; do
  fl=$(echo $f | rev | cut -d"/" -f1 | rev | cut -d"_" -f1-2)
  echo "Open failed on $f" >> $diff/${fl}.txt
  echo "$fl        $diff/${fl}.txt" >> $err
done

