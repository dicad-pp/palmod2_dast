#!/bin/bash
set -ue

module load nco


# Attributes
TITLE="MPI-ESM1-2-1 output prepared for PalMod2"
REFERENCES="The paper is currently in open discussion under the following citation:\nMikolajewicz, U., Kapsch, M.-L., Schannwell, C., Six, K. D., Ziemen, F. A., Bagge, M., Baudouin, J.-P., Erokhina, O., Gayler, V., Klemann, V., Meccia, V. L., Mouchet, A., and Riddick, T.: Deglaciation and abrupt events in a coupled comprehensive atmosphere–ocean–ice sheet–solid earth model, Clim. Past Discuss. [preprint], https://doi.org/10.5194/cp-2024-55, in review, 2024"
#NOMINAL_RESOLUTION="500 km" # echam6, jsbach, mpiom, hamocc
#NOMINAL_RESOLUTION="km"    # vilma
#NOMINAL_RESOLUTION="25 km" # mPISM
SOURCE="MPI-ESM1.2.1-CR (2022): \naerosol: none, prescribed Kinne (2010)\natmos: ECHAM6.3 (spectral T31; 96 x 48 longitude/latitude; 31 levels; top level 10 hPa)\natmosChem: none, prescribed\nland: JSBACH3.20, River Transport Model\nlandIce: none / mPISM 0.7 (10 km x 10 km (NH), 15 km x 15 km (SH), 121 levels)\nocean: MPIOM1.63 (bipolar GR3.0, approximately 300km; 122 x 101 longitude/latitude; 40 levels; top grid cell 0-15 m)\nocnBgchem: none, prescribed\nseaIce: unnamed (thermodynamic (Semtner zero-layer) dynamic (Hibler 79) sea ice model)\nsolidLand: none / VILMA-1D"
CONVENTIONS="CF-1.7 PalMod-2.0"
CREATION_DATE="2025-01-05T18:00:00Z"
HISTORY="2025-01-05T18:00:00Z ; CMOR rewrote data to be consistent with PalMod2, CF-1.7 PalMod-2.0 and CF standards."
PARENTEXPID="no parent"
declare -A TABLE_INFO
# 00.00.02
#dreqversion="00.00.02"
#TABLE_INFO[Amon]="Creation Date:(14 February 2022) MD5:26bac4b397da7babac4e36561826e6b0"
#TABLE_INFO[Omon]="Creation Date:(14 February 2022) MD5:26bac4b397da7babac4e36561826e6b0"
#TABLE_INFO[Lmon]="Creation Date:(14 February 2022) MD5:26bac4b397da7babac4e36561826e6b0"
#TABLE_INFO[dec]="Creation Date:(14 February 2022) MD5:26bac4b397da7babac4e36561826e6b0"
#TABLE_INFO[fx]="Creation Date:(14 February 2022) MD5:26bac4b397da7babac4e36561826e6b0"
#TABLE_INFO[Ofx]="Creation Date:(14 February 2022) MD5:26bac4b397da7babac4e36561826e6b0"
# 00.00.03
#dreqversion="00.00.03"
#TABLE_INFO[AERmon]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[Amon]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[centennial]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[dec]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[Emon]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[EmonZ]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[fx]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[IcenAnt]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[IcenGre]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[IdecAnt]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[IdecGre]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[IyrAnt]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[IyrGre]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[LImon]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[Lmon]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[Odec]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[Ofx]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[Omon]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[Oyr]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#TABLE_INFO[SImon]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
# 00.00.04
dreqversion="00.00.04"
TABLE_INFO[AERmon]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[Amon]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[centennial]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[dec]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[Emon]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[EmonZ]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[fx]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[IcenAnt]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[IcenGre]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[IdecAnt]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[IdecGre]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[IyrAnt]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[IyrGre]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[LImon]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[Lmon]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[Odec]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[Ofx]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[Omon]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[Oyr]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[SImon]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
TABLE_INFO[SLdec]="Creation Date:(18 September 2024) MD5:028d52f4a33ef4da346678b341512dda"
#TABLE_INFO[]=
declare -A VARIANT_INFO
# !!!!!!!
# Beware: the variant_info does also vary with the forcing parameter and not only with the physics parameter!
# !!!!!!!
#p1f1 / pmo0007
VARIANT_INFO["1"]="The data are from a synchronously coupled model simulation of the last deglaciation with the MPI-ESM1.2.1-CR-mPISM-VILMA setup, hence, including interactive ice sheets and solid earth components. The simulation used climatological volcanic forcing representing PI. GHG forcing is after Koehler et al. (2017), Orbit after Berger (1978)."
#p1f2 / pmo0012
#VARIANT_INFO["1"]="The data are from a synchronously coupled model simulation of the last deglaciation with the MPI-ESM1.2.1-CR-mPISM-VILMA setup, hence, including interactive ice sheets and solid earth components. GHG forcing is after Koehler et al. (2017), Orbit after Berger (1978). Equivalent to p1 but forced with time-varying volcanic forcing (Schindlbeck-Belo et al., 2024)."
#p2f1 / pmo0009
VARIANT_INFO["2"]="The data are from a synchronously coupled model simulation of the last deglaciation with the MPI-ESM1.2.1-CR-mPISM-VILMA setup, hence, including interactive ice sheets and solid earth components. The simulation used climatological volcanic forcing representing PI. GHG forcing is after Koehler et al. (2017), Orbit after Berger (1978). Equivalent to p1 but with increased vertical mixing in the ocean. For details see Mikoljawicz et al. (2024)."
#p3f1 / pmo0014
VARIANT_INFO["3"]="The data are from a synchronously coupled model simulation of the last deglaciation with the MPI-ESM1.2.1-CR-mPISM-VILMA setup, hence, including interactive ice sheets and solid earth components. The simulation used climatological volcanic forcing representing PI. GHG forcing is after Koehler et al. (2017), Orbit after Berger (1978). Equivalent to p1 but with decreased vertical mixing in the ocean. For details see Mikoljawicz et al. (2024)."
#p4f2 / pmo0016
VARIANT_INFO["4"]="The data are from a synchronously coupled model simulation of the last deglaciation with the MPI-ESM1.2.1-CR-mPISM-VILMA setup, hence, including interactive ice sheets and solid earth components. Equivalent to p1 but with retuned atmosphere as well as modified sea-ice rigidity, ocean mixing, and basal sliding parameters. In addition, the simulation was forced with time-varying volcanic forcing (Schindlbeck-Belo et al., 2024). GHG forcing is after Koehler et al. (2017), Orbit after Berger (1978)."
#p5f2 / pmo0018
VARIANT_INFO["5"]="The data are from a synchronously coupled model simulation of the last deglaciation with the MPI-ESM1.2.1-CR-mPISM-VILMA setup, hence, including interactive ice sheets and solid earth components. GHG forcing is after Koehler et al. (2017), Orbit after Berger (1978). Equivalent to p4 but with the atmosphere tuning of p1. The simulation was forced with time-varying volcanic forcing (Schindlbeck-Belo et al., 2024)."
#p6f2 / pmo0019
VARIANT_INFO["6"]="The data are from a synchronously coupled model simulation of the last deglaciation with the MPI-ESM1.2.1-CR-mPISM-VILMA setup, hence, including interactive ice sheets and solid earth components. GHG forcing is after Koehler et al. (2017), Orbit after Berger (1978). Equivalent to p4 but with brighter albedo parameters. The simulation was forced with time-varying volcanic forcing (Schindlbeck-Belo et al., 2024)."
#p7f2 / pmo0020
VARIANT_INFO["7"]="The data are from a synchronously coupled model simulation of the last deglaciation with the MPI-ESM1.2.1-CR-mPISM-VILMA setup, hence, including interactive ice sheets and solid earth components. GHG forcing is after Koehler et al. (2017), Orbit after Berger (1978). Equivalent to p4 but with decreased basal sliding near the grounding line. The simulation was forced with time-varying volcanic forcing (Schindlbeck-Belo et al., 2024)."
echo "------------------------"
echo $TITLE
echo $REFERENCES
#echo $NOMINAL_RESOLUTION
echo $SOURCE
echo $CONVENTIONS
echo $CREATION_DATE
echo $HISTORY
echo ${TABLE_INFO[@]}
echo "-------------------------"

exp_id=transient-deglaciation-interactive_r1i1p3f1-CR

ifolder=/work/kd1292/ESGF_Buff/k204212/palmod_CS/${exp_id}/PalMod2/
#ifolder=/work/kd1292/ESGF_PalMod/PalMod2/MPI-M/MPI-ESM1-2-1-CR/transient-deglaciation-interactive/r1i1p7f2/

ifiles=($(find $ifolder -type f))
echo ${#ifiles[@]} files found.
i=0
for ifile in ${ifiles[@]}; do
i=$((i+1))
echo $i $ifile
    table=$(echo $ifile | rev | cut -d '/' -f 1 | rev | cut -d '_' -f 2)
    memberid=$(echo $ifile | rev | cut -d '/' -f 1 | rev | cut -d '_' -f 5)
    physics_index=${memberid:5:1}
    forcing_index=${memberid:7:1}
    #forcing_index=2
    realization_index=${memberid:1:1}
    init_index=${memberid:3:1}
    variantlabel=r${realization_index}i${init_index}p${physics_index}f${forcing_index}
    #variantlabel=r1i1p7f2
    uuid="hdl:21.14105/$(uuidgen)"

    # Run ncatted
    #ncatted -O -h -a parent_experiment_id,global,m,c,"${PARENTEXPID}" -a variant_info,global,m,c,"${VARIANT_INFO[$physics_index]}" -a history,global,m,c,"$HISTORY" -a creation_date,global,m,c,"$CREATION_DATE" -a physics_index,global,m,c,"$physics_index" -a variant_label,global,m,c,"$memberid" -a table_info,global,m,c,"${TABLE_INFO[$table]}" -a Conventions,global,m,c,"$CONVENTIONS" -a title,global,m,c,"$TITLE" -a references,global,m,c,"$REFERENCES" -a source,global,m,c,"$SOURCE" -a nominal_resolution,global,m,c,"$NOMINAL_RESOLUTION" $ifile || echo "ERROR $ifile"
    #ncatted -O -h -a variant_info,global,m,c,"${VARIANT_INFO[$physics_index]}" $ifile || echo "ERROR $ifile"
    #ncatted -O -h -a tracking_id,global,m,c,"$uuid" $ifile || echo "ERROR $ifile"
    #ncatted -O -h -a parent_experiment_id,global,m,c,"${PARENTEXPID}" -a source,global,m,c,"$SOURCE" -a references,global,m,c,"$REFERENCES" -a variant_info,global,m,c,"${VARIANT_INFO[$physics_index]}" -a table_info,global,m,c,"${TABLE_INFO[$table]}" -a data_specs_version,global,m,c,${dreqversion} -a history,global,m,c,"$HISTORY" -a creation_date,global,m,c,"$CREATION_DATE" -a Conventions,global,m,c,"$CONVENTIONS" -a title,global,m,c,"$TITLE" -a tracking_id,global,m,c,"$uuid" $ifile || echo "ERROR $ifile"
    #ncatted -O -h -a history,global,m,c,"$HISTORY" -a creation_date,global,m,c,"$CREATION_DATE" $ifile || echo "ERROR $ifile"
    #ncatted -O -h -a forcing_index,global,m,c,"$forcing_index" -a variant_label,global,m,c,"$memberid" $ifile || echo "ERROR $ifile"
    ncatted -O -h -a comment,global,m,c,"The data for several variables for the time period 24621-24630 (i.e. 381-370 BP) was lost due to a hardware failure and is substituted with _FillValue." $ifile || echo "ERROR $ifile"

    # Adjust filename timestamp
    if [[ "$table" != *"fx"* ]]; then
        trunk=$(echo $ifile | rev | cut -d '_' -f 2- | rev)
        timestamp=$(echo $ifile | rev | cut -d '.' -f 2 | cut -d '_' -f 1 | rev)
        time1=$(echo $timestamp | cut -d '-' -f 1 | sed 's/^0*//' )
        time2=$(echo $timestamp | cut -d '-' -f 2 | sed 's/^0*//' )
        echo $time1 $time2
        # Case 1 - Decadal data
        if [[ "$table" == *"dec"* ]]; then
            if [[ "${time1: -1}" == "6" ]]; then
                newtime1=$(printf "%05d" $(expr $time1 - 5))
                newtime2=$(printf "%05d" $(expr $time2 + 4))
            else
                newtime1=$(printf "%05d" ${time1} )
                newtime2=$(printf "%05d" ${time2} )
            fi
        # Case 2 - the rest - annual and monthly data
        elif [[ "$table" == *"mon"* ]]; then
            newtime1=$(printf "%07d" ${time1} )
            newtime2=$(printf "%07d" ${time2} )
        elif [[ "$table" == *"yr"* ]]; then
            newtime1=$(printf "%05d" ${time1} )
            newtime2=$(printf "%05d" ${time2} )
        elif [[ "$table" == *"day"* ]]; then
            newtime1=$(printf "%09d" ${time1} )
            newtime2=$(printf "%09d" ${time2} )
        elif [[ "$table" == *"hr"* ]]; then
            newtime1=$(printf "%011d" ${time1} )
            newtime2=$(printf "%011d" ${time2} )
        else
            echo "ERROR: ${table}: no timestamp renaming rule!"
            continue
        fi
        # mv command
        ifile_new=${trunk}_${newtime1}-${newtime2}.nc
        [[ "$ifile" != "$ifile_new" ]] && {
            mv -v $ifile $ifile_new || echo "ERROR renaming $ifile to $ifile_new"
        }
    fi
done
