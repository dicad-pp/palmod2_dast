#!/bin/bash
set -ue

module load nco


# todo: rename version_ids

# Attributes
TITLE="MPI-ESM1-2 output prepared for PalMod2"
REFERENCES="Kleinen, T., Mikolajewicz, U. and Brovkin, V. (2020). Terrestrial methane emissions from the Last Glacial Maximum to the preindustrial period. Climate of the Past, 16(2):575–595. doi: 10.5194/cp-16-575-2020\nKleinen, T., Gromov, S., Steil, B. and Brovkin, V. (2021). Atmospheric methane underestimated in future climate projections. Environmental Research Letters, 16(9):094006. doi: 10.1088/1748-9326/ac1814\nKapsch, M.-L., Mikolajewicz, U., Ziemen, F., & Schannwell, C. (2022). Ocean response in transient simulations of the last deglaciation dominated by underlying ice-sheet reconstruction and method of meltwater distribution. Geophysical Research Letters, 49, e2021GL096767. https://doi.org/10.1029/2021GL096767\nMPI-ESM: Mauritsen, T. et al. (2019), Developments in the MPI‐M Earth System Model version 1.2 (MPI‐ESM1.2) and Its Response to Increasing CO2, J. Adv. Model. Earth Syst.,11, 998-1038, doi:10.1029/2018MS001400\nMueller, W.A. et al. (2018): A high‐resolution version of the Max Planck Institute Earth System Model MPI‐ESM1.2‐HR. J. Adv. Model. EarthSyst.,10,1383–1413, doi:10.1029/2017MS001217"
NOMINAL_RESOLUTION="500 km" # echam6, jsbach, mpiom, hamocc
#NOMINAL_RESOLUTION="km"    # vilma
#NOMINAL_RESOLUTION="25 km" # mPISM
SOURCE="MPI-ESM1.2-CR (2017): \naerosol: none, prescribed Kinne (2010)\natmos: ECHAM6.3 (spectral T31; 96 x 48 longitude/latitude; 31 levels; top level 10 hPa)\natmosChem: unnamed (accelerated methane chemistry)\nland: JSBACH3.20, River Transport Model\nlandIce: none, prescribed\nocean: MPIOM1.63 (bipolar GR3.0, approximately 300km; 122 x 101 longitude/latitude; 40 levels; top grid cell 0-15 m)\nocnBgchem: none, prescribed\nseaIce: unnamed (thermodynamic (Semtner zero-layer) dynamic (Hibler 79) sea ice model)\nsolidLand: none, prescribed"
CONVENTIONS="CF-1.7 PalMod-2.0"
CREATION_DATE="2023-02-02T10:00:00Z"
HISTORY="2023-02-02T10:00:00Z ; CMOR rewrote data to be consistent with PalMod2, CF-1.7 PalMod-2.0 and CF standards."
#PARENTEXPID="no parent"
PARENTEXPID="transient-deglaciation-prescribed-glac1d-methane"
declare -A TABLE_INFO
# 00.00.02
#dreqversion="00.00.02"
#TABLE_INFO[Amon]="Creation Date:(14 February 2022) MD5:26bac4b397da7babac4e36561826e6b0"
#TABLE_INFO[Omon]="Creation Date:(14 February 2022) MD5:26bac4b397da7babac4e36561826e6b0"
#TABLE_INFO[Lmon]="Creation Date:(14 February 2022) MD5:26bac4b397da7babac4e36561826e6b0"
#TABLE_INFO[dec]="Creation Date:(14 February 2022) MD5:26bac4b397da7babac4e36561826e6b0"
#TABLE_INFO[fx]="Creation Date:(14 February 2022) MD5:26bac4b397da7babac4e36561826e6b0"
#TABLE_INFO[Ofx]="Creation Date:(14 February 2022) MD5:26bac4b397da7babac4e36561826e6b0"
# 00.00.03
dreqversion="00.00.03"
TABLE_INFO[AERmon]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[Amon]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[centennial]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[dec]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[Emon]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[EmonZ]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[fx]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[IcenAnt]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[IcenGre]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[IdecAnt]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[IdecGre]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[IyrAnt]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[IyrGre]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[LImon]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[Lmon]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[Odec]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[Ofx]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[Omon]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[Oyr]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
TABLE_INFO[SImon]="Creation Date:(1 February 2023) MD5:437d9b7f4524e7483e04d57b09f4aaaa"
#
#TABLE_INFO[]=
declare -A VARIANT_INFO
VARIANT_INFO["1"]="PalMod MPI-ESM1-2-CR setup with full methane cycle. Model physics identical to MPI-ESM1-2-CR simulation transient-deglaciation-prescribed_r1i1p1f1."
echo "------------------------"
echo $TITLE
echo $REFERENCES
echo $NOMINAL_RESOLUTION
echo $SOURCE
echo $CONVENTIONS
echo $CREATION_DATE
echo $HISTORY
echo ${TABLE_INFO[@]}
echo "-------------------------"

exp_id=ssp119-future-methane_r1i1p1f1-CR
exp_id=ssp126-future-methane_r1i1p1f1-CR
exp_id=ssp245-future-methane_r1i1p1f1-CR
exp_id=ssp370-future-methane_r1i1p1f1-CR
exp_id=ssp585-future-methane_r1i1p1f1-CR
ifolder=/work/kd1292/k204212/palmod_TK/${exp_id}/archive/PalMod2/

ifiles=($(find $ifolder -type f))
echo ${#ifiles[@]} files found.
i=0
for ifile in ${ifiles[@]}; do
i=$((i+1))
echo $i $ifile
    table=$(echo $ifile | rev | cut -d '/' -f 1 | rev | cut -d '_' -f 2)
    memberid=$(echo $ifile | rev | cut -d '/' -f 1 | rev | cut -d '_' -f 5)
    physics_index=${memberid:5:1}
    forcing_index=${memberid:7:1}
    realization_index=${memberid:1:1}
    init_index=${memberid:3:1}
    variantlabel=r${realization_index}i${init_index}p${physics_index}f${forcing_index}
    uuid="hdl:21.14105/$(uuidgen)"

    # Run ncatted
    #ncatted -O -h -a variant_info,global,m,c,"${VARIANTINFO}" -a parent_experiment_id,global,m,c,"${PARENTEXPID}" -a variant_info,global,m,c,"${VARIANT_INFO[$physics_index]}" -a history,global,m,c,"$HISTORY" -a creation_date,global,m,c,"$CREATION_DATE" -a physics_index,global,m,c,"$physics_index" -a variant_label,global,m,c,"$memberid" -a table_info,global,m,c,"${TABLE_INFO[$table]}" -a Conventions,global,m,c,"$CONVENTIONS" -a title,global,m,c,"$TITLE" -a references,global,m,c,"$REFERENCES" -a source,global,m,c,"$SOURCE" -a nominal_resolution,global,m,c,"$NOMINAL_RESOLUTION" $ifile || echo "ERROR $ifile"
    #ncatted -O -h -a tracking_id,global,m,c,"$uuid" $ifile || echo "ERROR $ifile"
    ncatted -O -h -a history,global,m,c,"$HISTORY" -a creation_date,global,m,c,"$CREATION_DATE" -a table_info,global,m,c,"${TABLE_INFO[$table]}" -a Conventions,global,m,c,"$CONVENTIONS" -a title,global,m,c,"$TITLE" -a references,global,m,c,"$REFERENCES" $ifile -a variant_info,global,m,c,"${VARIANT_INFO[$forcing_index]}" || echo "ERROR $ifile"

    # Adjust filename timestamp
    if [[ "$table" != *"fx"* ]]; then
        trunk=$(echo $ifile | rev | cut -d '_' -f 2- | rev)
        timestamp=$(echo $ifile | rev | cut -d '.' -f 2 | cut -d '_' -f 1 | rev)
        time1=$(echo $timestamp | cut -d '-' -f 1 | sed 's/^0*//' )
        time2=$(echo $timestamp | cut -d '-' -f 2 | sed 's/^0*//' )
        echo $time1 $time2
        # Case 1 - Decadal data
        if [[ "$table" == *"dec"* ]]; then
            if [[ "${time1: -1}" == "5" ]]; then
                newtime1=$(printf "%04d" $(expr $time1 - 5))
                newtime2=$(printf "%04d" $(expr $time2 + 5))
            else
                newtime1=$(printf "%04d" ${time1} )
                newtime2=$(printf "%04d" ${time2} )
            fi
        # Case 2 - the rest - annual and monthly data
        elif [[ "$table" == *"mon"* ]]; then
            newtime1=$(printf "%06d" ${time1} )
            newtime2=$(printf "%06d" ${time2} )
        elif [[ "$table" == *"yr"* ]]; then
            newtime1=$(printf "%04d" ${time1} )
            newtime2=$(printf "%04d" ${time2} )
        elif [[ "$table" == *"day"* ]]; then
            newtime1=$(printf "%08d" ${time1} )
            newtime2=$(printf "%08d" ${time2} )
        elif [[ "$table" == *"hr"* ]]; then
            newtime1=$(printf "%010d" ${time1} )
            newtime2=$(printf "%010d" ${time2} )
        else
            echo "ERROR: ${table}: no timestamp renaming rule!"
            continue
        fi
        # mv command
        ifile_new=${trunk}_${newtime1}-${newtime2}.nc
        [[ "$ifile" != "$ifile_new" ]] && {
            mv -v $ifile $ifile_new || echo "ERROR renaming $ifile to $ifile_new"
        }
    fi
done
