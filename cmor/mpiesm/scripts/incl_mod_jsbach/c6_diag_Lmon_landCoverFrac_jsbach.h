  find_file -e -p $period "$sdir" "box_cover_fract_mon_${period}*" ifile1
  find_file -e -p $period "$sdir" "bare_mon_${period}*"            ifile2
  $cdo splitlevel                                       $ifile1                 ${sdir}/tmp_diag/lct_mon_${period}_       || echo ERROR
  $cdo -f grb setlevel,1 -setltype,70 -setparam,12.180  $ifile2                 ${sdir}/tmp_diag/lct_bare_mon_$period     || echo ERROR
  $cdo -f grb setlevel,2 -setltype,70 -setparam,12.180 -ifthen ${sdir}/out_aggr/slm_${period} -mul ${sdir}/out_aggr/glac_${period} -addc,1 -mulc,0 ${sdir}/tmp_diag/lct_mon_${period}_000001.grb ${sdir}/tmp_diag/lct_glac_mon_$period     || echo ERROR
  $cdo -f grb setlevel,3                                    ${sdir}/tmp_diag/lct_mon_${period}_000001.grb ${sdir}/tmp_diag/lct_tropev_mon_$period   || echo ERROR
  $cdo -f grb setlevel,4                                    ${sdir}/tmp_diag/lct_mon_${period}_000002.grb ${sdir}/tmp_diag/lct_tropdec_mon_$period  || echo ERROR
  $cdo -f grb setlevel,5                                    ${sdir}/tmp_diag/lct_mon_${period}_000003.grb ${sdir}/tmp_diag/lct_extrev_mon_$period   || echo ERROR
  $cdo -f grb setlevel,6                                    ${sdir}/tmp_diag/lct_mon_${period}_000004.grb ${sdir}/tmp_diag/lct_extrdec_mon_$period  || echo ERROR
  $cdo -f grb setlevel,7                                    ${sdir}/tmp_diag/lct_mon_${period}_000005.grb ${sdir}/tmp_diag/lct_rgshrub_mon_$period  || echo ERROR
  $cdo -f grb setlevel,8                                    ${sdir}/tmp_diag/lct_mon_${period}_000006.grb ${sdir}/tmp_diag/lct_decshrub_mon_$period || echo ERROR
  $cdo -f grb setlevel,9                                    ${sdir}/tmp_diag/lct_mon_${period}_000007.grb ${sdir}/tmp_diag/lct_c3grass_mon_$period  || echo ERROR 
  $cdo -f grb setlevel,10                                   ${sdir}/tmp_diag/lct_mon_${period}_000008.grb ${sdir}/tmp_diag/lct_c4grass_mon_$period  || echo ERROR
  $cdo -f grb setlevel,11                                   ${sdir}/tmp_diag/lct_mon_${period}_000009.grb ${sdir}/tmp_diag/lct_c3past_mon_$period   || echo ERROR
  $cdo -f grb setlevel,12                                   ${sdir}/tmp_diag/lct_mon_${period}_000010.grb ${sdir}/tmp_diag/lct_c4past_mon_$period   || echo ERROR
  $cdo -f grb setlevel,14 -setparam,12.180 -mul ${sdir}/out_aggr/C3C4_crop_mask_${period}      ${sdir}/tmp_diag/lct_mon_${period}_000011.grb ${sdir}/tmp_diag/lct_c4crop_mon_$period   || echo ERROR
  $cdo -f grb setlevel,13 -sub ${sdir}/tmp_diag/lct_mon_${period}_000011.grb ${sdir}/tmp_diag/lct_c4crop_mon_$period  ${sdir}/tmp_diag/lct_c3crop_mon_$period      || echo ERROR
  #$cdo -f nc -O ifthen ${sdir}/out_aggr/slm_${period} -chlevelv,landCoverFrac,0,1 -setname,landCoverFrac -merge ${sdir}/tmp_diag/lct_*_mon_$period \
  $cdo -f nc -O ifthen ${sdir}/out_aggr/slm_${period} -setname,landCoverFrac -merge \
                       ${sdir}/tmp_diag/lct_bare_mon_$period \
                       ${sdir}/tmp_diag/lct_glac_mon_$period \
                       ${sdir}/tmp_diag/lct_tropev_mon_$period \
                       ${sdir}/tmp_diag/lct_tropdec_mon_$period \
                       ${sdir}/tmp_diag/lct_extrev_mon_$period \
                       ${sdir}/tmp_diag/lct_extrdec_mon_$period \
                       ${sdir}/tmp_diag/lct_rgshrub_mon_$period \
                       ${sdir}/tmp_diag/lct_decshrub_mon_$period \
                       ${sdir}/tmp_diag/lct_c3grass_mon_$period \
                       ${sdir}/tmp_diag/lct_c4grass_mon_$period \
                       ${sdir}/tmp_diag/lct_c3past_mon_$period \
                       ${sdir}/tmp_diag/lct_c4past_mon_$period \
                       ${sdir}/tmp_diag/lct_c3crop_mon_$period \
                       ${sdir}/tmp_diag/lct_c4crop_mon_$period \
    ${sdir}/out_diag/Lmon_landCoverFrac_$period.nc && rm -f ${sdir}/tmp_diag/lct_mon_${period}_0000??.grb ${sdir}/tmp_diag/lct_*_mon_$period         || echo ERROR
