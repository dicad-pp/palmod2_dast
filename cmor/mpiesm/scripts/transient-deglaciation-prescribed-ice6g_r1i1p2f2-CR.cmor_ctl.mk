EXP_ID = transient-deglaciation-prescribed-ice6g_r1i1p2f2-CR

ACCOUNT = bm0021
PARTITION = prepost,compute,compute2

SUFFIX =
ARCHIVE_SUFFIX =

TARGET_STEP=cmor
NUMCHUNKS=$(shell cat cmor_ctl_list_ice6g_r1i1p2f2 | wc -l )

AGGR_TIME = 00:10:00
DIAGS_TIME = 00:30:00
CMOR_TIME = 02:30:00

CMOR_FLAGS =

SBATCHFLAGS=-W

MESSAGE = $$(date +%Y-%m-%dT%H:%M:%S): CMIP6 diagnostics and CMOR rewriting ($(SUFFIX:_%=%))

.PHONY: all
.PRECIOUS: targets/$(EXP_ID).aggr$(SUFFIX).% targets/$(EXP_ID).diags$(SUFFIX).% targets/$(EXP_ID).cmor$(SUFFIX).% 



all:
	@echo "$(MESSAGE) started"
	mkdir -vp targets
	$(MAKE) $(MFLAGS) -f $(MAKEFILE_LIST) $(addprefix targets/$(EXP_ID).$(TARGET_STEP)$(SUFFIX).,$(shell seq 1 $(NUMCHUNKS)))
	@echo "$(MESSAGE) finished"

targets/$(EXP_ID).aggr$(SUFFIX).%:
	sbatch $(SBATCHFLAGS) --time=$(AGGR_TIME) --job-name=$(EXP_ID)_runpp_aggr$(SUFFIX) --output=%x_%j.log --comment=$* $(EXP_ID).runpp -s '$(SUFFIX)' -S '$(ARCHIVE_SUFFIX)' -A $(shell sed -n '$(*)'p < cmor_ctl_list_ice6g_r1i1p2f2)
	@touch $@

targets/$(EXP_ID).diags$(SUFFIX).%: targets/$(EXP_ID).aggr$(SUFFIX).%
	sbatch $(SBATCHFLAGS) --time=$(DIAGS_TIME) --job-name=$(EXP_ID)_runpp_diag$(SUFFIX) --output=%x_%j.log --comment=$* $(EXP_ID).runpp -s '$(SUFFIX)' -S '$(ARCHIVE_SUFFIX)' -d $(shell sed -n '$(*)'p < cmor_ctl_list_ice6g_r1i1p2f2)
	@touch $@

targets/$(EXP_ID).cmor$(SUFFIX).%: targets/$(EXP_ID).diags$(SUFFIX).%
	sbatch $(SBATCHFLAGS) --time=$(CMOR_TIME) --job-name=$(EXP_ID)_runpp_cmor$(SUFFIX) --output=%x_%j.log --comment=$* $(EXP_ID).runpp -s '$(SUFFIX)' -S '$(ARCHIVE_SUFFIX)' -c $(CMOR_FLAGS) $(shell sed -n '$(*)'p < cmor_ctl_list_ice6g_r1i1p2f2)
	@touch $@

