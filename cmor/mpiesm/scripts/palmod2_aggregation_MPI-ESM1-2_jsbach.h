################################
# JSBACH #######################
################################
suf=grb

{ (if_requested $member $srfmod jsbachdecfx input $chunk "$srfmod_input_dec_chunk" && {

  # Create decadal "fx"-variables
  ifile=$input_file_jsbach

  #-- slm
  $cdo selname,slm $ifile $sdir/tmp_aggr/tmp_slm_$period || echo ERROR

  #-- slm_nan
  $cdo setctomiss,0 -selname,slm $ifile $sdir/tmp_aggr/tmp_slm_nan_$period || echo ERROR

  #-- glac
  $cdo setname,glac -eqc,1 -sellevel,1 -selname,cover_type $ifile $sdir/tmp_aggr/tmp_glac_$period || echo ERROR

  #-- veg_ratio_max
  $cdo -f grb setcode,20 -selname,veg_ratio_max $ifile $sdir/tmp_aggr/tmp_veg_ratio_max_mon_ini_$period || echo ERROR

  #-- C3C4_crop_mask
  $cdo setname,C3C4_crop_mask -expr,'C3C4_crop_mask=sellevel(cover_type,11)-20' $ifile $sdir/tmp_aggr/tmp_C3C4_crop_mask_$period || echo ERROR

  #-- mrsofc
  $cdo expr,'mrsofc=soil_field_cap*soil_depth*1.e+3;' $ifile ${sdir}/tmp_aggr/tmp_mrsofc_${period}.nc || echo ERROR

  for l_yyyy in $(seq $((period - 9)) ${period}); do
    $cdo settunits,days -settaxis,${l_yyyy}-01-15,12:00:00,1mon -duplicate,12 $sdir/tmp_aggr/tmp_slm_$period $sdir/out_aggr/slm_${l_yyyy} || echo ERROR
    $cdo settunits,days -settaxis,${l_yyyy}-01-15,12:00:00,1mon -duplicate,12 $sdir/tmp_aggr/tmp_slm_nan_$period $sdir/out_aggr/slm_nan_${l_yyyy} || echo ERROR
    $cdo settunits,days -settaxis,${l_yyyy}-01-15,12:00:00,1mon -duplicate,12 $sdir/tmp_aggr/tmp_glac_$period $sdir/out_aggr/glac_${l_yyyy} || echo ERROR
    $cdo settunits,days -settaxis,${l_yyyy}-01-15,12:00:00,1mon -duplicate,12 $sdir/tmp_aggr/tmp_veg_ratio_max_mon_ini_$period $sdir/tmp_aggr/veg_ratio_max_mon_ini_${l_yyyy}.tmp || echo ERROR
    $cdo settunits,days -settaxis,${l_yyyy}-01-15,12:00:00,1mon -duplicate,12 $sdir/tmp_aggr/tmp_C3C4_crop_mask_$period $sdir/out_aggr/C3C4_crop_mask_${l_yyyy} || echo ERROR
    $cdo settunits,days -settaxis,${l_yyyy}-01-15,12:00:00,1mon -duplicate,12 -selname,slm $ifile $sdir/out_aggr/sftlf_${l_yyyy}.nc || echo ERROR
    $cdo settunits,days -settaxis,${l_yyyy}-01-15,12:00:00,1mon -duplicate,12 -selname,root_depth $ifile ${sdir}/out_aggr/rootd_${l_yyyy}.nc || echo ERROR
    $cdo settunits,days -settaxis,${l_yyyy}-01-15,12:00:00,1mon -duplicate,12 ${sdir}/tmp_aggr/tmp_mrsofc_${period}.nc ${sdir}/out_aggr/mrsofc_${l_yyyy}.nc || echo ERROR
  done

}; )&; }>$err.jsbachdecfx.input 2>&1

wait

#
#-- cover_fract, veg_ratio_max, and box_cover_fract (monthly mean)
#
{ (if_requested $member $srfmod jsbachdecfx input $chunk "$srfmod_input_dec_chunk" && {
    for l_yyyy in $(seq $((period - 9)) ${period}); do
        $cdo selcode,12 $sdir/${RAW_EXP_IDS[$y]}_jsbach_jsbach_mm_${l_yyyy}.$suf $sdir/tmp_aggr/cover_fract_mon_${l_yyyy}.tmp || echo ERROR
        if [[ "$(echo ${dynveg:-false} | tr [:upper:] [:lower:])" == "true" ]]; then
            $cdo selcode,20 $sdir/${RAW_EXP_IDS[$y]}_jsbach_jsbach_mm_${l_yyyy}.$suf $sdir/tmp_aggr/veg_ratio_max_mon_${l_yyyy}.tmp || echo ERROR
        else
            cp $sdir/tmp_aggr/veg_ratio_max_mon_ini_${l_yyyy}.tmp $sdir/tmp_aggr/veg_ratio_max_mon_${l_yyyy}.tmp || echo ERROR
        fi

        #  In grib format 3.05e-5 is the minimum value different from 0 for records with values between 0 and 1
        # we set all fractions below 5.e-5 to zero, to avoid errors with variables that need to be scaled.
        # It is important to use netCDF for these calculations!
        $cdo mul              $sdir/tmp_aggr/veg_ratio_max_mon_${l_yyyy}.tmp   $sdir/tmp_aggr/cover_fract_mon_${l_yyyy}.tmp      $sdir/tmp_aggr/box_cover_fract_mon_${l_yyyy}.tmp         || echo ERROR
        $cdo mul -gtc,5.e-5   $sdir/tmp_aggr/veg_ratio_max_mon_${l_yyyy}.tmp   $sdir/tmp_aggr/veg_ratio_max_mon_${l_yyyy}.tmp    $sdir/out_aggr/veg_ratio_max_mon_${l_yyyy}   || echo ERROR
        $cdo mul -gtc,5.e-5   $sdir/tmp_aggr/cover_fract_mon_${l_yyyy}.tmp     $sdir/tmp_aggr/cover_fract_mon_${l_yyyy}.tmp      $sdir/out_aggr/cover_fract_mon_${l_yyyy}     || echo ERROR
        $cdo mul -gtc,5.e-5   $sdir/tmp_aggr/box_cover_fract_mon_${l_yyyy}.tmp $sdir/tmp_aggr/box_cover_fract_mon_${l_yyyy}.tmp  $sdir/out_aggr/box_cover_fract_mon_${l_yyyy} || echo ERROR
        $cdo setname,bare -sub -addc,1 -mulc,-1 $sdir/tmp_aggr/veg_ratio_max_mon_${l_yyyy}.tmp $sdir/out_aggr/glac_${l_yyyy} $sdir/out_aggr/bare_mon_${l_yyyy}            || echo ERROR
        #
        #-- veg_ratio_max and box_cover_fract (annual mean)
        #
        $cdo yearmonmean $sdir/out_aggr/veg_ratio_max_mon_${l_yyyy}   $sdir/out_aggr/veg_ratio_max_yr_${l_yyyy} || echo ERROR
        $cdo yearmonmean $sdir/out_aggr/box_cover_fract_mon_${l_yyyy} $sdir/out_aggr/box_cover_fract_yr_${l_yyyy} || echo ERROR
        rm -f $sdir/tmp_aggr/*${l_yyyy}.tmp
    done
}; )&; }>$err.veg.type 2>&1

###########################################################################################################

wait

