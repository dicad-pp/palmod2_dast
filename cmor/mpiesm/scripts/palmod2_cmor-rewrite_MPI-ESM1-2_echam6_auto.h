#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) AERmon
cn='emilnox'
for var in $cn; do
  { (if_requested $member $atmmod AERmon $var $chunk || continue
  mkdir -p $dr/$submodel/AERmon_${var}
  ifile=${sdir}/out_diag/AERmon_${var}_$period.nc
  echo $cdo cmor,AERmon,i=$it,mt=$mt,dr=$dr/$submodel/AERmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,AERmon,i=$it,mt=$mt,dr=$dr/$submodel/AERmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.AERmon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) AERmon
cn='ua va'
find_file "$sdir" "*_echammon_after_${period}*" ifile >> $err.find_file.AERmon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod AERmon $var $chunk || continue
  mkdir -p $dr/$submodel/AERmon_${var}
  echo $cdo cmor,AERmon,i=$it,mt=$mt,dr=$dr/$submodel/AERmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,AERmon,i=$it,mt=$mt,dr=$dr/$submodel/AERmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.AERmon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) Amon
cn='evspsbl pr rlds rsds'
for var in $cn; do
  { (if_requested $member $atmmod Amon $var $chunk || continue
  mkdir -p $dr/$submodel/Amon_${var}
  ifile=${sdir}/out_diag/Amon_${var}_$period.nc
  echo $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Amon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) Amon
cn='cl clt hfls hfss hurs huss prc prsn ps rlus rlut rsdt rsus rsut sfcWind tas tasmax tasmin ts uas vas'
find_file "$sdir" "*_echam_${period}*" ifile >> $err.find_file.Amon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod Amon $var $chunk || continue
  mkdir -p $dr/$submodel/Amon_${var}
  echo $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Amon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) Amon
cn='psl'
find_file "$sdir" "*_echammon_after_${period}*" ifile >> $err.find_file.Amon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod Amon $var $chunk || continue
  mkdir -p $dr/$submodel/Amon_${var}
  echo $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Amon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) Amon
cn='hur hus ta ua va wap zg'
find_file "$sdir" "*_echammon_plev19_${period}*" ifile >> $err.find_file.Amon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod Amon $var $chunk || continue
  mkdir -p $dr/$submodel/Amon_${var}
  echo $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Amon,i=$it,mt=$mt,dr=$dr/$submodel/Amon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Amon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) Emon
cn='ch4'
for var in $cn; do
  { (if_requested $member $atmmod Emon $var $chunk || continue
  mkdir -p $dr/$submodel/Emon_${var}
  ifile=${sdir}/out_diag/Emon_${var}_$period.nc
  echo $cdo cmor,Emon,i=$it,mt=$mt,dr=$dr/$submodel/Emon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Emon,i=$it,mt=$mt,dr=$dr/$submodel/Emon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Emon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) Emon
cn='ch4brdn emich4 fSoilCH4 lossch4'
find_file "$sdir" "*_echam6_ch4_mm_${period}*" ifile >> $err.find_file.Emon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod Emon $var $chunk || continue
  mkdir -p $dr/$submodel/Emon_${var}
  echo $cdo cmor,Emon,i=$it,mt=$mt,dr=$dr/$submodel/Emon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Emon,i=$it,mt=$mt,dr=$dr/$submodel/Emon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Emon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) Emon
cn='tdps'
find_file "$sdir" "*_echam_${period}*" ifile >> $err.find_file.Emon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod Emon $var $chunk || continue
  mkdir -p $dr/$submodel/Emon_${var}
  echo $cdo cmor,Emon,i=$it,mt=$mt,dr=$dr/$submodel/Emon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Emon,i=$it,mt=$mt,dr=$dr/$submodel/Emon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Emon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) LImon
cn='snw'
for var in $cn; do
  { (if_requested $member $atmmod LImon $var $chunk || continue
  mkdir -p $dr/$submodel/LImon_${var}
  ifile=${sdir}/out_diag/LImon_${var}_$period.nc
  echo $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.LImon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) LImon
cn='snm'
find_file "$sdir" "*_accw_${period}*" ifile >> $err.find_file.LImon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod LImon $var $chunk || continue
  mkdir -p $dr/$submodel/LImon_${var}
  echo $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.LImon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) LImon
cn='snd'
find_file "$sdir" "*_echam_${period}*" ifile >> $err.find_file.LImon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod LImon $var $chunk || continue
  mkdir -p $dr/$submodel/LImon_${var}
  echo $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.LImon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) Lmon
cn='mrros'
for var in $cn; do
  { (if_requested $member $atmmod Lmon $var $chunk || continue
  mkdir -p $dr/$submodel/Lmon_${var}
  ifile=${sdir}/out_diag/Lmon_${var}_$period.nc
  echo $cdo cmor,Lmon,i=$it,mt=$mt,dr=$dr/$submodel/Lmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Lmon,i=$it,mt=$mt,dr=$dr/$submodel/Lmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Lmon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) Lmon
cn='mrro'
find_file "$sdir" "*_accw_${period}*" ifile >> $err.find_file.Lmon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod Lmon $var $chunk || continue
  mkdir -p $dr/$submodel/Lmon_${var}
  echo $cdo cmor,Lmon,i=$it,mt=$mt,dr=$dr/$submodel/Lmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Lmon,i=$it,mt=$mt,dr=$dr/$submodel/Lmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Lmon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) SImon
cn='sitemptop'
find_file "$sdir" "*_echam_${period}*" ifile >> $err.find_file.SImon 2>&1
for var in $cn; do
  { (if_requested $member $atmmod SImon $var $chunk || continue
  mkdir -p $dr/$submodel/SImon_${var}
  echo $cdo cmor,SImon,i=$it,mt=$mt,dr=$dr/$submodel/SImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,SImon,i=$it,mt=$mt,dr=$dr/$submodel/SImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.SImon 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) dec
cn='orog'
for var in $cn; do
  { (if_requested $member $atmmod dec $var $chunk '${atmmod_dec_chunk}' || continue
  mkdir -p $dr/$submodel/dec_${var}
  ifile=${sdir}/out_diag/dec_${var}_$period.nc
  echo $cdo cmor,dec,i=$it,mt=$mt,dr=$dr/$submodel/dec_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile
  $cdo cmor,dec,i=$it,mt=$mt,dr=$dr/$submodel/dec_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.dec 2>&1
done

#-- CMOR-rewrite for echam6 (ESM: MPI-ESM1-2) fx
cn='areacella'
for var in $cn; do
  { (if_requested $member $atmmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  ifile=${sdir}/out_diag/fx_$var.nc
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

