#-- Diagnostic for vilma (ESM: MPI-ESM1-2) variable rslc / table SLdec
{ (if_requested $member $slmod SLdec rslc $chunk '${slmod_dec_chunk}' && {
  find_file -e            "$sdir" "rsl_${period_bk}.nc" ifile
  echo "$cdo -f nc -O \
    -expr,'rslc=rsl' \
    -settaxis,\"$((period_bk + 5))-07-01,00:00:00\" \
    -settunits,'days' \
    $ifile ${sdir}/out_diag/SLdec_rslc_${period_bk}.nc"
  $cdo -f nc -O \
    -expr,'rslc=rsl;' \
    -settaxis,"$((period_bk + 5))-07-01,00:00:00" \
    -settunits,'days' \
    $ifile ${sdir}/out_diag/SLdec_rslc_${period}.nc || echo ERROR
}; )&; }>$err.rslc.SLdec 2>&1

#-- Diagnostic for vilma (ESM: MPI-ESM1-2) variable areacellsl / table fx
{ (if_requested $member $slmod fx areacellsl $chunk && {
  find_file -e            "$sdir" "rsl_${period_bk}.nc" ifile
  $cdo -f nc -O \
    expr,'areacellsl=gridarea(rsl);' \
    $ifile ${sdir}/out_diag/fx_areacellsl_${period_bk}.nc || echo ERROR
}; )&; }>$err.areacellsl.fx 2>&1

