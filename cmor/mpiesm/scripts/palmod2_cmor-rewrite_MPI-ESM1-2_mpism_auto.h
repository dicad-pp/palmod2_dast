#-- CMOR-rewrite for mpism (ESM: MPI-ESM1-2) IdecAnt
cn='sftgifIt'
for var in $cn; do
  { (if_requested $member $icemod IdecAnt $var $chunk '${icemod_dec_chunk}' || continue
  mkdir -p $dr/$submodel/IdecAnt_${var}
  ifile=${sdir}/out_diag/IdecAnt_${var}_$period.nc
  echo $cdo cmor,IdecAnt,i=$it,mt=$mt,dr=$dr/$submodel/IdecAnt_${var},vd=$vd,cn=$var,ta=cmip,di=$di $ifile
  $cdo cmor,IdecAnt,i=$it,mt=$mt,dr=$dr/$submodel/IdecAnt_${var},vd=$vd,cn=$var,ta=cmip,di=$di $ifile || echo ERROR
  )&; }>>$err.$var.IdecAnt 2>&1
done

#-- CMOR-rewrite for mpism (ESM: MPI-ESM1-2) IdecAnt
cn='libmassbffl'
for var in $cn; do
  { (if_requested $member $icemod IdecAnt $var $chunk '${icemod_dec_chunk}' || continue
  mkdir -p $dr/$submodel/IdecAnt_${var}
  ifile=${sdir}/out_diag/IdecAnt_${var}_$period.nc
  echo $cdo cmor,IdecAnt,i=$it,mt=$mt,dr=$dr/$submodel/IdecAnt_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile
  $cdo cmor,IdecAnt,i=$it,mt=$mt,dr=$dr/$submodel/IdecAnt_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.IdecAnt 2>&1
done

#-- CMOR-rewrite for mpism (ESM: MPI-ESM1-2) IdecGre
cn='sftgifIt'
for var in $cn; do
  { (if_requested $member $icemod IdecGre $var $chunk '${icemod_dec_chunk}' || continue
  mkdir -p $dr/$submodel/IdecGre_${var}
  ifile=${sdir}/out_diag/IdecGre_${var}_$period.nc
  echo $cdo cmor,IdecGre,i=$it,mt=$mt,dr=$dr/$submodel/IdecGre_${var},vd=$vd,cn=$var,ta=cmip,di=$di $ifile
  $cdo cmor,IdecGre,i=$it,mt=$mt,dr=$dr/$submodel/IdecGre_${var},vd=$vd,cn=$var,ta=cmip,di=$di $ifile || echo ERROR
  )&; }>>$err.$var.IdecGre 2>&1
done

#-- CMOR-rewrite for mpism (ESM: MPI-ESM1-2) IdecGre
cn='libmassbffl'
for var in $cn; do
  { (if_requested $member $icemod IdecGre $var $chunk '${icemod_dec_chunk}' || continue
  mkdir -p $dr/$submodel/IdecGre_${var}
  ifile=${sdir}/out_diag/IdecGre_${var}_$period.nc
  echo $cdo cmor,IdecGre,i=$it,mt=$mt,dr=$dr/$submodel/IdecGre_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile
  $cdo cmor,IdecGre,i=$it,mt=$mt,dr=$dr/$submodel/IdecGre_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.IdecGre 2>&1
done

#-- CMOR-rewrite for mpism (ESM: MPI-ESM1-2) IdecAnt
cn='acabfIs dtb orogIs tlIs tsIs'
for var in $cn; do
  { (if_requested $member $icemod IdecAnt $var $chunk '${icemod_dec_chunk}' || continue
  find_file "$sdir" "pism_ant_${period}.nc" ifile >> $err.find_file.IdecAnt 2>&1
  mkdir -p $dr/$submodel/IdecAnt_${var}
  echo $cdo cmor,IdecAnt,i=$it,mt=$mt,dr=$dr/$submodel/IdecAnt_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile
  $cdo cmor,IdecAnt,i=$it,mt=$mt,dr=$dr/$submodel/IdecAnt_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.IdecAnt 2>&1
done

#-- CMOR-rewrite for mpism (ESM: MPI-ESM1-2) IdecGre
cn='acabfIs dtb orogIs tlIs tsIs'
for var in $cn; do
  { (if_requested $member $icemod IdecGre $var $chunk '${icemod_dec_chunk}' || continue
  find_file "$sdir" "pism_nh_${period}.nc" ifile >> $err.find_file.IdecGre 2>&1
  mkdir -p $dr/$submodel/IdecGre_${var}
  echo $cdo cmor,IdecGre,i=$it,mt=$mt,dr=$dr/$submodel/IdecGre_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile
  $cdo cmor,IdecGre,i=$it,mt=$mt,dr=$dr/$submodel/IdecGre_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.IdecGre 2>&1
done

