#-- Diagnostic for echam6 (ESM: MPI-ESM1-2) variable emilnox / table AERmon
# Editor Note: emi_NO_lghtng_accu
{ (if_requested $member $atmmod AERmon emilnox $chunk && {
  find_file -e            "$sdir" "*_echam6_lghtng_${period}*" ifile
  $cdo -f nc -O \
    expr,'emilnox=var3/gridarea(var3);' \
    $ifile ${sdir}/out_diag/AERmon_emilnox_$period.nc || echo ERROR
}; )&; }>$err.emilnox.AERmon 2>&1

#-- Diagnostic for echam6 (ESM: MPI-ESM1-2) variable evspsbl / table Amon
{ (if_requested $member $atmmod Amon evspsbl $chunk && {
  find_file -e            "$sdir" "*_echam_${period}*" ifile
  $cdo -f nc -O \
    expr,'evspsbl=-1*var182;' \
    $ifile ${sdir}/out_diag/Amon_evspsbl_$period.nc || echo ERROR
}; )&; }>$err.evspsbl.Amon 2>&1

#-- Diagnostic for echam6 (ESM: MPI-ESM1-2) variable pr / table Amon
{ (if_requested $member $atmmod Amon pr $chunk && {
  find_file -e            "$sdir" "*_echam_${period}*" ifile
  $cdo -f nc -O \
    expr,'pr=var142+var143;' \
    $ifile ${sdir}/out_diag/Amon_pr_$period.nc || echo ERROR
}; )&; }>$err.pr.Amon 2>&1

#-- Diagnostic for echam6 (ESM: MPI-ESM1-2) variable rlds / table Amon
{ (if_requested $member $atmmod Amon rlds $chunk && {
  find_file -e            "$sdir" "*_echam_${period}*" ifile
  $cdo -f nc -O \
    expr,'rlds=var177-var205;' \
    $ifile ${sdir}/out_diag/Amon_rlds_$period.nc || echo ERROR
}; )&; }>$err.rlds.Amon 2>&1

#-- Diagnostic for echam6 (ESM: MPI-ESM1-2) variable rsds / table Amon
{ (if_requested $member $atmmod Amon rsds $chunk && {
  find_file -e            "$sdir" "*_echam_${period}*" ifile
  $cdo -f nc -O \
    expr,'rsds=var176-var204;' \
    $ifile ${sdir}/out_diag/Amon_rsds_$period.nc || echo ERROR
}; )&; }>$err.rsds.Amon 2>&1

#-- Diagnostic for echam6 (ESM: MPI-ESM1-2) variable ch4 / table Emon
# Editor Note: CH4
{ (if_requested $member $atmmod Emon ch4 $chunk && {
  find_file -e            "$sdir" "*_echam6_echam_${period}*" ifile1
  find_file -e            "$sdir" "*_echam6_tracer_mm_${period}*" ifile2
  $cdo -f nc -O \
    expr,'ch4=var3;ps=var134;' \
    -merge -selcode,134 $ifile1 -selcode,3 $ifile2 \
    ${sdir}/out_diag/Emon_ch4_$period.nc || echo ERROR
}; )&; }>$err.ch4.Emon 2>&1

#-- Diagnostic for echam6 (ESM: MPI-ESM1-2) variable snw / table LImon
{ (if_requested $member $atmmod LImon snw $chunk && {
  find_file -e            "$sdir" "*_echam_${period}*" ifile
  $cdo -f nc -O \
    expr,'snw=1000*var141;' \
    $ifile ${sdir}/out_diag/LImon_snw_$period.nc || echo ERROR
}; )&; }>$err.snw.LImon 2>&1

#-- Diagnostic for echam6 (ESM: MPI-ESM1-2) variable mrros / table Lmon
{ (if_requested $member $atmmod Lmon mrros $chunk && {
  find_file -e            "$sdir" "*_accw_${period}*" ifile
  $cdo -f nc -O \
    expr,'mrros=var160-var161;' \
    $ifile ${sdir}/out_diag/Lmon_mrros_$period.nc || echo ERROR
}; )&; }>$err.mrros.Lmon 2>&1

#-- Diagnostic for echam6 (ESM: MPI-ESM1-2) variable orog / table dec
# Editor Note: Alternatively, submit surface topography from the restart file. Updated every 10 years.
{ (if_requested $member $atmmod dec orog $chunk && {
  find_file -e            "$sdir" "*_echam6_echam_${period}*" ifile
  $cdo -f nc -O \
    yearmean \
    -expr,'orog=var129/9.81;' \
    $ifile ${sdir}/tmp_diag/dec_orog_$period.nc || echo ERROR
  if_requested $member $atmmod dec orog $chunk '${atmmod_dec_chunk}' && {
    eval $cdo -O ensmean \
      ${sdir}/tmp_diag/dec_orog_{$((period-9))..${period}}.nc ${sdir}/out_diag/dec_orog_${period}.nc && \
      eval rm ${sdir}/tmp_diag/dec_orog_{$((period-9))..${period}}.nc || echo ERROR
}; }; )&; }>$err.orog.dec 2>&1

#-- Diagnostic for echam6 (ESM: MPI-ESM1-2) variable areacella / table fx
{ (if_requested $member $atmmod fx areacella $chunk && {
  find_file -e            "$sdir" "*_echam6_echam_${period}*" ifile
  $cdo -f nc -O \
    expr,'areacella=gridarea(var129);' \
    $ifile ${sdir}/out_diag/fx_areacella.nc || echo ERROR
}; )&; }>$err.areacella.fx 2>&1

