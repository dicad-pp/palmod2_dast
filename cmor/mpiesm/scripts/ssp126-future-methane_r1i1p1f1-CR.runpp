#!/bin/ksh
#
# PalMod .runpp
#
# Wrapper Script Agg+Diag+CMOR MPI-ESM1-2 by Martin Schupfner, DKRZ
#
### Batch Queuing System is SLURM
#SBATCH --partition=compute
#SBATCH --nodes=1
#SBATCH --mem=100G
#SBATCH --time=01:30:00
#SBATCH --mail-type=none
#SBATCH --account=bk1192
##SBATCH --qos=esgf
#SBATCH --exclusive
#SBATCH --output=ssp126-future-methane_r1i1p1f1-CR.runpp_%j.log

DEBUG_LEVEL=${DEBUG_LEVEL:-0}

# Support log style output
export LANG=C
print () { command print "$(date +'%F %T'):" "$@"; }
print_re='^[0-9]+-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}'

warn () { print 'Hey:' "$@" >&2; }
die () { print 'Oops:' "$@" >&2; return 1; }

# Bail out on error
trap 'print Error at line $LINENO >&2' ERR
set -eu

# Disable core file creation
ulimit -c 0

# Print command info
[[ $DEBUG_LEVEL -ge 2 ]] && set -x

#module load nco || { echo "Oops: Could not load NCO!" && exit 1 ; }
alias ncatted=/sw/spack-levante/nco-5.0.6-3xkdth/bin/ncatted

#########################################
# Define specifics about the simulation
#########################################

#Root directory of all scripts, outdata, logs etc
EXP_ID=ssp126-future-methane_r1i1p1f1-CR
EXP_DIR_TRUNK=/work/kd1292/k204212/palmod_TK/ssp126-future-methane_r1i1p1f1-CR
#EXP_DIR_TRUNK=/work/bk1192/k204212/palmod_TK/ssp126-future-methane_r1i1p1f1-CR

#initial files for aggregation
INPUT_DIR_TRUNK=/work/bm1030/from_Mistral/bm1030/m300020/mpiesm-palmod_tk_r10186_megan_next/experiments

# RAW Data - path and sub-simulations
RAW_EXP_DIR_TRUNK=/work/bk1192/from_Mistral/bk1192/WG2/WP2.3/future_scenarios_2021/SSP1-2.6
# As usually the Experiment is composed of several sub-simulations, define them as array
RAW_EXP_IDS=( pmt0550_20RN_SSP126_r2 )
# Parent experiment that the first RAW_EXP_ID branched from
#  (used for input data of the parent that is required for aggregation)
# !! spinup - the aggregation will fail if the spinup period
# !!          is as long or longer than
# !!          the length of the first sub simulation!
spinup=0
RAW_EXP_ID_PARENT=( pmt0531_d )
# actually, there was a 300y spinup after the branch from pmt0531_d@5600
#  but for simplicity this is ignored here
#if [[ $spinup -ne 0 ]]; then
RAW_EXP_ID_PARENT[0]=${RAW_EXP_IDS[0]}
#fi

echo ${RAW_EXP_IDS[@]}
echo ${RAW_EXP_ID_PARENT[@]}

#Work dir
WORK_DIR_TRUNK=$EXP_DIR_TRUNK/work

#Where to find the scripts (runpp etc.)
SCRIPT_DIR=${EXP_DIR_TRUNK}/scripts
SCRIPT_DIR=/work/bm0021/PalMod2/cmor/mpiesm/scripts



############################
#Define necessary variables
############################

#Where to find cdo (incl. CMOR operator), eg.:
cdo="/work/bm0021/cdo_incl_cmor/cdo-2022-09-20_cmor3.6.0_gcc/bin/cdo -v" # latest version

# Base directory for DataRequest related headers/scripts
SCRIPT_ROOT=${SCRIPT_DIR}

#Where to find the functions
fpath=/work/bm0021/PalMod2/cmor/functions

#Where to find the configuration
cpath=${SCRIPT_ROOT}/conf

#Models
atmmod=echam6
ocemod=mpiom
srfmod=jsbach
esmod=MPI-ESM1-2

#Chunks
chunks+=([echam6]=" 0 1 2 3 4 5 6 7 8 9 ")
chunks+=([jsbach]=" 0 1 2 3 4 5 6 7 8 9 ")
chunks+=([mpiom]=" 0 ")
chunks+=([jsbachdec]=" 0 ")
atmmod_dec_chunk="???9123124"
srfmod_dec_chunk="???9123124"
srfmod_input_dec_chunk="???0123124"
ocemod_dec_chunk="???0123124"
icemod_dec_chunk="???0123124"
ocemod_fxyear=5900

#InfoTable(s)
# Define here the "cdocmorinfo"-File or "eum"-Files
# Multiple files possible with "," as delimiter, eg:
#  it=expinfo.txt,userinfo.txt,modelinfo.txt
it_temp="${SCRIPT_ROOT}/ssp126-future-methane_r1i1p1f1-CR_cdocmorinfo"
ca+=([mpiom]="${SCRIPT_ROOT}/cdocmorinfo_mpiom_CR")
ca+=([echam6]="${SCRIPT_ROOT}/cdocmorinfo_echam6_CR")
ca+=([jsbach]="${SCRIPT_ROOT}/cdocmorinfo_jsbach_CR")

#Experiment etc
experiment=ssp126-future-methane
member=r1i1p1f1
mip=PalMod2
vd=v20230201
#vd=v$(date '+%Y%m%d') #CMOR version directory name, should be a date in format YYYYMMDD

#Where to store errors
errdir=${EXP_DIR_TRUNK}/logs/errors
errdir_cmor=${errdir}_cmor
errdir_diag=${errdir}_diag
errdir_agg=${errdir}_agg

#initial and final year of the experiment
iniyeararr=($((5900+spinup))) # 1850-3049, after the 300y spinup
finyeararr=(7099)
iniyear=1850
finyear=3049
#offset compared to startyear
inioffsetarr=(-4050) # 1850-3049
#  -  5900-7099 - 1200 yrs - 1850-3049 - -4050

# Command line options
RUN_AGG=true
RUN_DIAGS=true
RUN_CMOR=true
SUFFIX=
CLIMSUFFIX=
ARCHIVE_SUFFIX=
USE_CHUNKS=false
REMOVE_CHUNKS=false
PROCESS_CLIM=false
while [[ "$1" == -* ]]
do
    OPT=
    ARG=
    case "$1" in
        -d|--diags|--diags-only) RUN_CMOR=false; RUN_AGG=false;;
        -c|--cmor|--cmor-only) RUN_DIAGS=false; RUN_AGG=false;;
        -A|--agg|--agg-only) RUN_DIAGS=false; RUN_CMOR=false;;
        -C|--clim) PROCESS_CLIM=true;;
        -s|--suffix) OPT=-s; ARG=SUFFIX;;
        -S|--archive-suffix) OPT=-S; ARG=ARCHIVE_SUFFIX;;
        -f|--force) REMOVE_CHUNKS=true;;
        -a|--append) USE_CHUNKS=true;;
        --) shift; break;;
        -*) die "invalid option '$1'";;
    esac
    if [[ -n "$OPT" ]]
    then
        shift
        [[ $# -lt 1 ]] && die "missing argument for option '$OPT'"
        eval "$ARG='$1'"
    fi
    shift
done

echo "-----------------"
echo $0 $1 $2 $3 $4
echo "-----------------"

#Write Output to:
dr_trunk=${EXP_DIR_TRUNK}/archive$ARCHIVE_SUFFIX



###############################################################################
# No user input beyond this line necessary (usually)
###############################################################################

# Store parameters
par1=$1
par2=$2
par3=$3
par4=$4
n_arr=${#inioffsetarr[@]}

#time interval of the experiment to standardize
if [[ ${#iniyeararr[@]} != ${#inioffsetarr[@]} ]] || [[ ${#iniyeararr[@]} != ${#finyeararr[@]} ]] || [[ ${#iniyeararr[@]} != ${#RAW_EXP_IDS[@]} ]]; then
  die 'Each sub-simulation needs a specified iniyear, finyear and inioffset!'
else
  for y in $(seq 0 $(( n_arr - 1 )) ); do
    SDIR+=( ${EXP_DIR_TRUNK}/${RAW_EXP_IDS[$y]} )
    RAWSDIR+=( ${RAW_EXP_DIR_TRUNK} )
    WORK_DIR+=( ${WORK_DIR_TRUNK}/${RAW_EXP_IDS[$y]} )
  done
  [[ -z "$2" ]] && [[ "$PROCESS_CLIM" == "false" ]] && die 'invalid number of parameters; need index of start and end simulations, start year (of first index) and end year (of second index) as YYYY'
  [[ -z "$3" ]] && [[ "$PROCESS_CLIM" == "false" ]] && die 'invalid number of parameters; need index of start and end simulations, start year (of first index) and end year (of second index) as YYYY'
  [[ -z "$4" ]] && [[ "$PROCESS_CLIM" == "false" ]] && die 'invalid number of parameters; need index of start and end simulations, start year (of first index) and end year (of second index) as YYYY'
  for y in $(seq 0 $(( n_arr - 1 )) ); do
    if [[ $y -lt $par1 ]] || [[ $y -gt $par2 ]]; then
      lrel=-1
      urel=-1
      labs=-1
      uabs=-1
    elif [[ $y -eq $1 ]] && [[ $1 -eq $2 ]]; then
      lrel=$3
      urel=$4
      labs=$(( $3 + ${inioffsetarr[$y]} - $spinup ))
      uabs=$(( $4 + ${inioffsetarr[$y]} - $spinup ))
    elif [[ $y -eq $1 ]]; then
      lrel=$3
      urel=${finyeararr[$y]}
      labs=$(( $3 + ${inioffsetarr[$y]} - $spinup ))
      uabs=$(( ${finyeararr[$y]} + ${inioffsetarr[$y]} - $spinup ))
    elif [[ $y -eq $2 ]]; then
      lrel=${iniyeararr[$y]}
      urel=$4
      labs=$(( ${iniyeararr[$y]} + ${inioffsetarr[$y]} - $spinup ))
      uabs=$(( $4 + ${inioffsetarr[$y]} - $spinup ))
    else
      lrel=${iniyeararr[$y]}
      urel=${finyeararr[$y]}
      labs=$(( ${iniyeararr[$y]} + ${inioffsetarr[$y]} - $spinup ))
      uabs=$(( ${finyeararr[$y]} + ${inioffsetarr[$y]} - $spinup ))
    fi
    [[ $urel -lt $lrel ]] && die 'iniyear, finyear arrays not properly set up!'
    [[ $uabs -lt $labs ]] && die 'iniyear, finyear arrays not properly set up!'
    SUB_SIM_REL_L+=( $lrel )
    SUB_SIM_REL_U+=( $urel )
    SUB_SIM_ABS_L+=( $labs )
    SUB_SIM_ABS_U+=( $uabs )
    [[ $lrel -eq -1 ]] && continue
    [[ "$PROCESS_CLIM" == "false" ]] && { [[ ${SUB_SIM_REL_L[$y]} -lt ${iniyeararr[$y]} ]] || [[ ${SUB_SIM_REL_L[$y]} -gt ${finyeararr[$y]} ]] ; } && die 'erroneous input - input does not make sense along with the configured iniyear, finyear arrays'
    [[ "$PROCESS_CLIM" == "false" ]] && { [[ ${SUB_SIM_REL_U[$y]} -lt ${iniyeararr[$y]} ]] || [[ ${SUB_SIM_REL_U[$y]} -gt ${finyeararr[$y]} ]] ; } && die 'erroneous input - input does not make sense along with the configured iniyear, finyear arrays'
  done
  echo ${SUB_SIM_REL_L[@]}
  echo ${SUB_SIM_REL_U[@]}
  echo ${SUB_SIM_ABS_L[@]}
  echo ${SUB_SIM_ABS_U[@]}
  [[ $1 -ge ${#iniyeararr[@]} ]] && die 'erroneous input - input does not make sense along with the configured iniyear, finyear arrays'
  [[ $2 -ge ${#iniyeararr[@]} ]] && die 'erroneous input - input does not make sense along with the configured iniyear, finyear arrays'
  [[ $1 -gt $2 ]] && die 'erroneous input - input does not make sense along with the configured iniyear, finyear arrays'
  cmorstart=$(( $3 + ${inioffsetarr[$1]} - $spinup ))
  cmorend=$(( $4 + ${inioffsetarr[$2]} -spinup ))
fi
MESSAGE='PalMod2'
$RUN_AGG && MESSAGE+=' - Aggregation - '
$RUN_DIAGS && MESSAGE+=' - Diagnostics - '
$RUN_CMOR && MESSAGE+=" - CMOR rewriting - "
$PROCESS_CLIM && MESSAGE+=' for only climatological variables'
print "$(date +%Y-%m-%dT%H:%M:%S): $MESSAGE started for ${cmorstart}-${cmorend} $SUFFIX"
print "  ... translating to ${RAW_EXP_IDS[$1]}@$3 to ${RAW_EXP_IDS[$2]}@$4"

#exit 0



##############################
# Load the required functions
#   function_Read_request_config: read data request / user configuration and initialize the if_requested check
#   function_if_requested: checks if the currently processed variable is requested for the current timestep
#   function_find_file: searches for inputfiles
##############################
. $fpath/function_Read_request_config_palmod
. $fpath/function_if_requested
. $fpath/function_find_file

#Load predefined timeslices (depends on $iniyear/$finyear)
. $cpath/TimeSlices.h

##Additionally define custom TimeSlices
## Example on how to define TimeSlices
## format: YYYYMMDDHH-YYYYMMDDHH
##
#TimeSlices+=([custom1]=(1850010100-1850010100))
#TimeSlices+=([custom2]=(1850010100-1850123124))
##TimeSlice dependend on variable
#TimeSlices+=([custom3]=(${iniyear}010100-${iniyear}123124))
##TimeSlice dependend on arithmetics using variable
#TimeSlices+=([custom4]=($((iniyear+121))010100-$((iniyear+150))123124))

##Define SettingsContainer
## The following Option 3hrtest will activate the listed 3hr variables and deactivate any other variable for the given member (= realisation)
## The UserSettings for the given experiment have to contain the line:
## Option: 3hrtest = True
## or the setting will not be active!
#SettingsContainer+=([3hrtest]=([3hr]=(clt hfls hfss pr prc ps rlds rlus rsds rsus tas tos uas vas)))
#SettingsContainer[3hrtest]+=([${member}]=(${member}:False))

#Initialize DataRequest/User Configuration for the if_requested function
# This will read all SettingsContainers, TimeSlices and the configuration file
rrc_option=-s
[[ $DEBUG_LEVEL -ge 1 ]] && rrc_option=
[[ $DEBUG_LEVEL -ge 2 ]] && rrc_option=-v



#################################
# Run agg, diag and cmor-rewrite
#################################

function run_agg
{
    # Loop over file output periods and load diagnostic script fragment
    if $RUN_AGG; then

    for y in $(seq ${par1} ${par2}); do

    # perform time shift for selected variables
    cdochain="-shifttime,$((inioffsetarr[$y] - spinup))year -shifttime,-3day"

    for period in $(seq ${SUB_SIM_REL_L[$y]} ${SUB_SIM_REL_U[$y]}); do
        # Define the chunk that is tested by if_requested (YYYYMMDDHH-YYYYMMDDHH)
        chunk=$( echo $(( ${period} + ${inioffsetarr[$y]} - $spinup )) | awk '{ printf "%06d", $0 }')010100-$( echo $(( ${period} + ${inioffsetarr[$y]} - $spinup )) | awk '{ printf "%06d", $0 }')123124
        l_chunk=$(echo $chunk | cut -d "-" -f1)
        r_chunk=$(echo $chunk | cut -d "-" -f2)

        l_chunk_year=${l_chunk%??????}
        r_chunk_year=${r_chunk%??????}

        inv_chunk_year=$(( 1 + ${finyear} - ${l_chunk_year##+(0)} ))
        print "Aggregation $chunk ($period): Reading forcing/input of year $inv_chunk_year"

        # Clean up stdout/stderr from previous run of this script
        errtemp_agg="${errdir_agg}${SUFFIX}${CLIMSUFFIX}/${RAW_EXP_IDS[$y]}/${RAW_EXP_IDS[$y]}_${period}"
        mkdir -p ${errtemp_agg}
        err="${errtemp_agg}/err${period}"
        rm -f ${err}*

        # Read input file - for SSP* runs the jsbach input is time invariant
        echo "Reading from ${RAW_EXP_IDS[$y]} jsbach_T31GR30_11tiles_5layers_natural-veg_100k.nc"
        input_file_jsbach=$INPUT_DIR_TRUNK/${RAW_EXP_IDS[$y]}/restart/topo/jsbach_T31GR30_11tiles_5layers_natural-veg_100k.nc

        # Perform aggregation for each submodel
        for submodel in $atmmod $ocemod $srfmod; do # $bgcmod; do

            # Chunking
            [[ "${chunks[$submodel]}" == *" ${period: -1} "* ]] && {

            # Show progress
            print "$(date +%Y-%m-%dT%H:%M:%S): Aggregation started for ${RAW_EXP_IDS[$y]} $submodel $chunk ($period) ..."
            sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment aggr/${RAW_EXP_IDS[$y]}/$submodel/$period

            # Location of input/output
            sdir=${SDIR[$y]}/outdata/$submodel
            rawsdir=${RAWSDIR[$y]}/$submodel
            echo "... writing output to $sdir/out_aggr"
            echo "... writing log to $errtemp_agg"

            # Create output dirs
            mkdir -p ${sdir}/tmp_aggr
            mkdir -p ${sdir}/out_aggr

            # Create softlinks to raw model output
            echo "... creating links from '$rawsdir' for $period, $submodel"
            ln -sf $rawsdir/*_${period}* $sdir/ 2>/dev/null 1>&2 || { echo "ERROR creating links for $period ${RAW_EXP_IDS[$y]} $submodel" && exit 1 ; }

            # Load and run the aggr ScriptFragment
            . ${SCRIPT_DIR}/palmod2_aggregation_${esmod}_${submodel}.h
            wait
            print "$(date +%Y-%m-%dT%H:%M:%S): Aggregation ended for ${RAW_EXP_IDS[$y]} $submodel $chunk ($period) ..."
            }

        done # submodel
    done # period
    done # EXP ID Index y
    fi # if RUN_AGG
}

function run_diag
{
    # Loop over file output periods and load diagnostic script fragment
    if $RUN_DIAGS; then
    for y in $(seq ${par1} ${par2}); do

    for period in $(seq ${SUB_SIM_REL_L[$y]} ${SUB_SIM_REL_U[$y]}); do
        # Define the chunk that is tested by if_requested (YYYYMMDDHH-YYYYMMDDHH)
        chunk=$( echo $(( ${period} + ${inioffsetarr[$y]} - $spinup )) | awk '{ printf "%06d", $0 }')010100-$( echo $(( ${period} + ${inioffsetarr[$y]} - $spinup )) | awk '{ printf "%06d", $0 }')123124

        # Clean up stdout/stderr from previous run of this script
        errtemp_diag="${errdir_diag}${SUFFIX}${CLIMSUFFIX}/${RAW_EXP_IDS[$y]}/${RAW_EXP_IDS[$y]}_${period}"
        mkdir -p ${errtemp_diag}
        err="${errtemp_diag}/err${period}"
        rm -f ${err}*

        # Perform diagnostic for each submodel
        for submodel in $atmmod $ocemod $srfmod; do # $bgcmod; do

            # Chunking
            [[ "${chunks[$submodel]}" == *" ${period: -1} "* ]] && {

            # Show progress
            print "$(date +%Y-%m-%dT%H:%M:%S): Diagnostic started for ${RAW_EXP_IDS[$y]} $submodel $chunk ($period) ..."
            sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment diag/${RAW_EXP_IDS[$y]}/$submodel/$period

            #Location of input/output
            sdir=${SDIR[$y]}/outdata/$submodel
            rawsdir=${RAWSDIR[$y]}/$submodel
            echo "... writing output to $sdir/out_diag"
            echo "... writing log to $errtemp_diag"

            # Diagnostic Output
            mkdir -p ${sdir}/tmp_diag
            mkdir -p ${sdir}/out_diag

            # Load and run the diag ScriptFragment
            . ${SCRIPT_DIR}/palmod2_diagnostic_${esmod}_${submodel}_auto.h
            wait
            print "$(date +%Y-%m-%dT%H:%M:%S): Diagnostic ended for ${RAW_EXP_IDS[$y]} $submodel $chunk ($period) ..."
            }

        done # submodel
    done # period
    done # EXP ID Index y
    fi # if RUN_DIAG
}

function run_cmor
{
    # Loop over file output periods and load CMOR-Rewrite script fragment
    if $RUN_CMOR; then

    for y in $(seq ${par1} ${par2}); do

    # Use working directory to hold .CHUNK and other tmp files
    current_dir=${WORK_DIR[$y]}/cmor${SUFFIX}_${cmorstart}-${cmorend}${CLIMSUFFIX}
    if [[ -e $current_dir ]] && [[ "$PROCESS_CLIM" == "false" ]]
    then
        if $USE_CHUNKS
        then
            warn "appending to existing chunk"
        elif $REMOVE_CHUNKS
        then
            warn "moving chunk info to backup"
            mkdir -p ${WORK_DIR[$y]}/backup
            mv -v --backup=numbered $current_dir ${WORK_DIR[$y]}/backup
        else
            die "please check if cmor is running or has already been run for this chunk. Use --force to re-run"
        fi
    else
        if $USE_CHUNKS
        then
            die "cannot find chunk info for appending"
        fi
    fi
    mkdir -p $current_dir
    previous_dir=$PWD
    # Link gridinfo files
    ln -sf $SCRIPT_DIR/gridinfo*nc $current_dir/
    cd $current_dir

    # perform time shift
    cdochain="-shifttime,$((inioffsetarr[$y] - spinup))year -shifttime,-3day"

    # Define DRS root dir
    dr=$dr_trunk/cmor${SUFFIX}_${cmorstart}-${cmorend}${CLIMSUFFIX}
    mkdir -p $dr

    for period in $(seq ${SUB_SIM_REL_L[$y]} ${SUB_SIM_REL_U[$y]}); do
        # Define the chunk that is tested by if_requested (YYYYMMDDHH-YYYYMMDDHH)
        chunk=$( echo $(( ${period} + ${inioffsetarr[$y]} - $spinup )) | awk '{ printf "%06d", $0 }')010100-$( echo $(( ${period} + ${inioffsetarr[$y]} - $spinup )) | awk '{ printf "%06d", $0 }')123124
        # Define decadal interval
        di="$(( ${period} + ${inioffsetarr[$y]} - 9 - $spinup )),$(( ${period} + ${inioffsetarr[$y]} - $spinup ))"
        dioce="$(( ${period} + ${inioffsetarr[$y]} - $spinup )),$(( ${period} + ${inioffsetarr[$y]} - $spinup + 9 ))"

        # Clean up stdout/stderr from previous run of this script
        errtemp_cmor="${errdir_cmor}${SUFFIX}${CLIMSUFFIX}/${RAW_EXP_IDS[$y]}/${RAW_EXP_IDS[$y]}_${period}"
        mkdir -p ${errtemp_cmor}
        err="${errtemp_cmor}/err${period}"
        rm -f ${err}*

        # Perform CMOR rewrite for each submodel
        for submodel in $atmmod $ocemod $srfmod; do # $bgcmod; do

            # Chunking
            [[ "${chunks[$submodel]}" == *" ${period: -1} "* ]] && {

            # Show progress
            print "$(date +%Y-%m-%dT%H:%M:%S): CMORisation started for ${RAW_EXP_IDS[$y]} $submodel $chunk ($period) ..."
            sh -c 'scontrol update JobId=$SLURM_JOB_ID Comment="$*"' scomment cmor/${RAW_EXP_IDS[$y]}/$submodel/$period

            #Location of input/output
            sdir=${SDIR[$y]}/outdata/$submodel
            rawsdir=${RAWSDIR[$y]}/$submodel
            echo "... writing output to $dr/$submodel"
            echo "... writing log to $errtemp_cmor"

            # Output
            mkdir -p $dr/$submodel

            #Location of Mapping table:
            mt="${SCRIPT_DIR}/tables/${esmod}_${submodel}_${mip}_mapping.txt"

            #Define cdocmorinfo
            it="${it_temp},${ca[${submodel}]}"

            # Load and run the CMOR-Rewrite ScriptFragment
            . ${SCRIPT_DIR}/palmod2_cmor-rewrite_${esmod}_${submodel}_auto.h
            wait
            print "$(date +%Y-%m-%dT%H:%M:%S): CMORisation ended for ${RAW_EXP_IDS[$y]} $submodel $chunk ($period) ..."
            }

        done # submodel
    done # period
    cd $previous_dir
    done # EXP ID Index y

    # Run NCO ncatted cleanup of unappropriate/unnecessary attributes
    #  this is necessary since CMOR sets attributes necessary only for CMIP6
    #  that are unnecessary/wrong for PalMod
    post_nco

    fi # if RUN_CMOR
}

function post_nco {

print "$(date +%Y-%m-%dT%H:%M:%S): NCO cleanup started for $dr"
# Load nco
#module load nco || { echo "Oops: Could not load NCO!" && exit 1 ; }

# Post Processing with ncatted
#Find netCDF files in DRS directory
flist=( $( find $dr -name "*.nc" -type f | sort ) )

#Loop and run ncatted
echo "-> Looping over ${#flist[@]} netCDF files!"
for f in ${flist[@]}
do
    echo "... editing $f"
    ncatted -O -h -a further_info_url,global,d,, $f && echo "... deleted 'further_info_url'." || echo "... failed (ERROR)"
    [[ "$(basename $f)" == *"cen"* ]] && {
        ncatted -O -h -a frequency,global,m,c,"cen" $f && echo "... changed frequency to 'cen'." || echo "... failed (ERROR)"
    }
done

print "$(date +%Y-%m-%dT%H:%M:%S): NCO cleanup ended for $dr"

}



#Initialize DataRequest/User Configuration for the if_requested function
# This will read all SettingsContainers, TimeSlices and the configuration file
# Thereafter run aggregation, diagnostic, cmorization
if $PROCESS_CLIM
then
    # Overwrite cmorstart/cmorend:
    cmorstart=$iniyear
    cmorend=$finyear

    # Read settings for clim1
    CLIMSUFFIX=_clim
    Read_request_config $rrc_option AllExp ${cpath}/${mip}_requested_vars_AllExp${SUFFIX}${CLIMSUFFIX}.conf || die "error while reading request config"
    # Run diag&cmor
    run_diag
    run_cmor

    if [[ "$experiment" != "ssp"* ]]
    then
        # Reset Read_requested_config
        l_linu=0
        unset DreqSettings
        unset SliceSettings
        unset UserSettings
        unset TimeSlicesBounds
        unset PriorityOrder

        # Read settings for clim2
        CLIMSUFFIX=_clim2
        Read_request_config $rrc_option AllExp ${cpath}/${mip}_requested_vars_AllExp${SUFFIX}${CLIMSUFFIX}.conf || die "error while reading request config"
        # Run diag&cmor
        run_diag
        run_cmor
    fi
else
    Read_request_config $rrc_option AllExp ${cpath}/${mip}_requested_vars_AllExpTK$SUFFIX.conf || die "error while reading request config"
    $RUN_AGG && run_agg || echo "Could not run agg"
    $RUN_DIAGS && run_diag || echo "Could not run diag"
    $RUN_CMOR && run_cmor || echo "Could not run cmor"
    wait
fi



print "$(date +%Y-%m-%dT%H:%M:%S): $MESSAGE finished for $cmorstart-$cmorend$SUFFIX"

# Update run dates and submit job scripts

# $Id$


