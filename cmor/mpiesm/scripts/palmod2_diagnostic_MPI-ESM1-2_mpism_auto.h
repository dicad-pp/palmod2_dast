#-- Diagnostic for mpism (ESM: MPI-ESM1-2) variable libmassbffl / table IdecAnt
# Editor Note: This variable needs to be converted to the correct units. So it should bmelt*ice_density (917)/secondsperyear. Requires aggregation to account for leap years. Added recipe just for testing.
{ (if_requested $member $icemod IdecAnt libmassbffl $chunk '${icemod_dec_chunk}' && {
  find_file -e            "$sdir" "pism_ant_${period}.nc" ifile
  echo "$cdo -f nc -O \
    -expr,'libmassbffl=bmelt*917/86400/365;' \
       $ifile ${sdir}/out_diag/IdecAnt_libmassbffl_${period}.nc"
  $cdo -f nc -O \
    -expr,'libmassbffl=bmelt*917/86400/365;' \
       $ifile ${sdir}/out_diag/IdecAnt_libmassbffl_${period}.nc || echo ERROR
}; )&; }>$err.libmassbffl.IdecAnt 2>&1

#-- Diagnostic for mpism (ESM: MPI-ESM1-2) variable libmassbffl / table IdecGre
# Editor Note: This variable needs to be converted to the correct units. So it should bmelt*ice_density (917)/secondsperyear. Requires aggregation to account for leap years. Added recipe just for testing.
{ (if_requested $member $icemod IdecGre libmassbffl $chunk '${icemod_dec_chunk}' && {
  find_file -e            "$sdir" "pism_nh_${period}.nc" ifile
  echo "$cdo -f nc -O \
    -expr,'libmassbffl=bmelt*917/86400/365;' \
       $ifile ${sdir}/out_diag/IdecGre_libmassbffl_${period}.nc"
  $cdo -f nc -O \
    -expr,'libmassbffl=bmelt*917/86400/365;' \
       $ifile ${sdir}/out_diag/IdecGre_libmassbffl_${period}.nc || echo ERROR
}; )&; }>$err.libmassbffl.IdecGre 2>&1

#-- Diagnostic for mpism (ESM: MPI-ESM1-2) variable sftgifIt / table IdecAnt
{ (if_requested $member $icemod IdecAnt sftgifIt $chunk '${icemod_dec_chunk}' && {
  find_file -e            "$sdir" "pism_ant_${period}.nc" ifile
  echo "$cdo -f nc -O \
    ${cdochain-} -expr,'ifl=mask==0;gi=mask==2;fi=mask==3;o=mask==4;' \
    $ifile ${sdir}/out_diag/IdecAnt_sftgifIt_$period.nc"
  $cdo -f nc -O \
    ${cdochain-} -expr,'ifl=mask==0;gi=mask==2;fi=mask==3;o=mask==4;' \
    $ifile ${sdir}/out_diag/IdecAnt_sftgifIt_$period.nc || echo ERROR
}; )&; }>$err.sftgifIt.IdecAnt 2>&1

#-- Diagnostic for mpism (ESM: MPI-ESM1-2) variable sftgifIt / table IdecGre
{ (if_requested $member $icemod IdecGre sftgifIt $chunk '${icemod_dec_chunk}' && {
  find_file -e            "$sdir" "pism_nh_${period}.nc" ifile
  echo "$cdo -f nc -O \
    ${cdochain-} -expr,'ifl=mask==0;gi=mask==2;fi=mask==3;o=mask==4;' \
    $ifile ${sdir}/out_diag/IdecGre_sftgifIt_$period.nc"
  $cdo -f nc -O \
    ${cdochain-} -expr,'ifl=mask==0;gi=mask==2;fi=mask==3;o=mask==4;' \
    $ifile ${sdir}/out_diag/IdecGre_sftgifIt_$period.nc || echo ERROR
}; )&; }>$err.sftgifIt.IdecGre 2>&1
