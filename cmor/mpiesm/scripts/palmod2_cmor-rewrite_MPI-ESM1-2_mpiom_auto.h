#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) EmonZ
cn='sltbasin'
find_file "$sdir" "*_mpiom_data_moc_mm_${period}*" ifile >> $err.find_file.EmonZ 2>&1
for var in $cn; do
  { (if_requested $member $ocemod EmonZ $var $chunk || continue
  mkdir -p $dr/$submodel/EmonZ_${var}
  echo $cdo cmor,EmonZ,i=$it,mt=$mt,dr=$dr/$submodel/EmonZ_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,EmonZ,i=$it,mt=$mt,dr=$dr/$submodel/EmonZ_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.EmonZ 2>&1
done

#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) Odec
cn='sftof'
for var in $cn; do
  { (if_requested $member $ocemod Odec $var $chunk '${ocemod_dec_chunk}' || continue
  mkdir -p $dr/$submodel/Odec_${var}
  ifile=${sdir}/out_diag/Odec_${var}_$period.nc
  echo $cdo cmor,Odec,i=$it,mt=$mt,dr=$dr/$submodel/Odec_${var},vd=$vd,cn=$var,ta=cmip,di=$dioce ${cdochain-} $ifile
  $cdo cmor,Odec,i=$it,mt=$mt,dr=$dr/$submodel/Odec_${var},vd=$vd,cn=$var,ta=cmip,di=$dioce ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Odec 2>&1
done

#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) Ofx
cn='volcello'
for var in $cn; do
  { (if_requested $member $ocemod Ofx $var $chunk || continue
  mkdir -p $dr/$submodel/Ofx_${var}
  ifile=${sdir}/out_diag/Ofx_$var.nc
  echo $cdo cmor,Ofx,i=$it,mt=$mt,dr=$dr/$submodel/Ofx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Ofx,i=$it,mt=$mt,dr=$dr/$submodel/Ofx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Ofx 2>&1
done

#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) Ofx
cn='basin'
find_file "$sdir" "*_basin.nc" ifile >> $err.find_file.Ofx 2>&1
for var in $cn; do
  { (if_requested $member $ocemod Ofx $var $chunk || continue
  mkdir -p $dr/$submodel/Ofx_${var}
  echo $cdo cmor,Ofx,i=$it,mt=$mt,dr=$dr/$submodel/Ofx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Ofx,i=$it,mt=$mt,dr=$dr/$submodel/Ofx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Ofx 2>&1
done

#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) Ofx
cn='thkcello'
find_file "$sdir" "mpiom_fx.nc" ifile >> $err.find_file.Ofx 2>&1
for var in $cn; do
  { (if_requested $member $ocemod Ofx $var $chunk || continue
  mkdir -p $dr/$submodel/Ofx_${var}
  echo $cdo cmor,Ofx,i=$it,mt=$mt,dr=$dr/$submodel/Ofx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Ofx,i=$it,mt=$mt,dr=$dr/$submodel/Ofx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Ofx 2>&1
done

#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) Ofx
cn='deptho'
find_file "$sdir" "mpiom_fx.nc" ifile >> $err.find_file.Ofx 2>&1
for var in $cn; do
  { (if_requested $member $ocemod Ofx $var $chunk || continue
  mkdir -p $dr/$submodel/Ofx_${var}
  echo $cdo cmor,Ofx,i=$it,mt=$mt,dr=$dr/$submodel/Ofx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Ofx,i=$it,mt=$mt,dr=$dr/$submodel/Ofx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Ofx 2>&1
done

#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) Ofx
cn='areacello'
find_file "$sdir" "Ofx_areacello.nc" ifile >> $err.find_file.Ofx 2>&1
for var in $cn; do
  { (if_requested $member $ocemod Ofx $var $chunk || continue
  mkdir -p $dr/$submodel/Ofx_${var}
  echo $cdo cmor,Ofx,i=$it,mt=$mt,dr=$dr/$submodel/Ofx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Ofx,i=$it,mt=$mt,dr=$dr/$submodel/Ofx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Ofx 2>&1
done

#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) Ofx
cn='sftof'
find_file "$sdir" "weto_${period}.nc" ifile >> $err.find_file.Ofx 2>&1
for var in $cn; do
  { (if_requested $member $ocemod Ofx $var $chunk || continue
  mkdir -p $dr/$submodel/Ofx_${var}
  echo $cdo cmor,Ofx,i=$it,mt=$mt,dr=$dr/$submodel/Ofx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Ofx,i=$it,mt=$mt,dr=$dr/$submodel/Ofx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Ofx 2>&1
done

#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) Omon
cn='hfx hfy volcello'
for var in $cn; do
  { (if_requested $member $ocemod Omon $var $chunk || continue
  mkdir -p $dr/$submodel/Omon_${var}
  ifile=${sdir}/out_diag/Omon_${var}_$period.nc
  echo $cdo cmor,Omon,i=$it,mt=$mt,dr=$dr/$submodel/Omon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Omon,i=$it,mt=$mt,dr=$dr/$submodel/Omon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Omon 2>&1
done

#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) Omon
cn='deptho hfds mlotst msftbarot sos tauuo tauvo tos zos'
find_file "$sdir" "*_mpiom_data_2d_mm_${period}*" ifile >> $err.find_file.Omon 2>&1
for var in $cn; do
  { (if_requested $member $ocemod Omon $var $chunk || continue
  mkdir -p $dr/$submodel/Omon_${var}
  echo $cdo cmor,Omon,i=$it,mt=$mt,dr=$dr/$submodel/Omon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Omon,i=$it,mt=$mt,dr=$dr/$submodel/Omon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Omon 2>&1
done

#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) Omon
cn='so thetao thkcello umo uo vmo vo wmo'
find_file "$sdir" "*_mpiom_data_3d_mm_${period}*.nc" ifile >> $err.find_file.Omon 2>&1
for var in $cn; do
  { (if_requested $member $ocemod Omon $var $chunk || continue
  mkdir -p $dr/$submodel/Omon_${var}
  echo $cdo cmor,Omon,i=$it,mt=$mt,dr=$dr/$submodel/Omon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Omon,i=$it,mt=$mt,dr=$dr/$submodel/Omon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Omon 2>&1
done

#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) Omon
cn='tosga tosnaa wfogiwosi'
find_file "$sdir" "*_mpiom_timeser_mm_${period}*" ifile >> $err.find_file.Omon 2>&1
for var in $cn; do
  { (if_requested $member $ocemod Omon $var $chunk || continue
  mkdir -p $dr/$submodel/Omon_${var}
  echo $cdo cmor,Omon,i=$it,mt=$mt,dr=$dr/$submodel/Omon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Omon,i=$it,mt=$mt,dr=$dr/$submodel/Omon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Omon 2>&1
done

#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) Omon
cn='msftmz'
find_file "$sdir" "msftmz_Omon_${period}*" ifile >> $err.find_file.Omon 2>&1
for var in $cn; do
  { (if_requested $member $ocemod Omon $var $chunk || continue
  mkdir -p $dr/$submodel/Omon_${var}
  echo $cdo cmor,Omon,i=$it,mt=$mt,dr=$dr/$submodel/Omon_${var},vd=$vd,cn=$var $ifile
  $cdo cmor,Omon,i=$it,mt=$mt,dr=$dr/$submodel/Omon_${var},vd=$vd,cn=$var $ifile || echo ERROR
  )&; }>>$err.$var.Omon 2>&1
done

#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) SImon
cn='simass sisnmass sisnthick sithick'
for var in $cn; do
  { (if_requested $member $ocemod SImon $var $chunk || continue
  mkdir -p $dr/$submodel/SImon_${var}
  ifile=${sdir}/out_diag/SImon_${var}_$period.nc
  echo $cdo cmor,SImon,i=$it,mt=$mt,dr=$dr/$submodel/SImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,SImon,i=$it,mt=$mt,dr=$dr/$submodel/SImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.SImon 2>&1
done

#-- CMOR-rewrite for mpiom (ESM: MPI-ESM1-2) SImon
cn='siconc sisnconc siu siv'
find_file "$sdir" "*_mpiom_data_2d_mm_${period}*" ifile >> $err.find_file.SImon 2>&1
for var in $cn; do
  { (if_requested $member $ocemod SImon $var $chunk || continue
  mkdir -p $dr/$submodel/SImon_${var}
  echo $cdo cmor,SImon,i=$it,mt=$mt,dr=$dr/$submodel/SImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,SImon,i=$it,mt=$mt,dr=$dr/$submodel/SImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.SImon 2>&1
done

