#-- CMOR-rewrite for jsbach (ESM: MPI-ESM1-2) Emon
cn='fVegFireCH4 herbivoreCH4 termiteCH4 vegFrac wetlandCH4'
for var in $cn; do
  { (if_requested $member $srfmod Emon $var $chunk || continue
  mkdir -p $dr/$submodel/Emon_${var}
  ifile=${sdir}/out_diag/Emon_${var}_$period.nc
  echo $cdo cmor,Emon,i=$it,mt=$mt,dr=$dr/$submodel/Emon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Emon,i=$it,mt=$mt,dr=$dr/$submodel/Emon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Emon 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: MPI-ESM1-2) Emon
cn='wetlandFrac'
find_file "$sdir" "*_jsbach_topmod_mm_${period}*" ifile >> $err.find_file.Emon 2>&1
for var in $cn; do
  { (if_requested $member $srfmod Emon $var $chunk || continue
  mkdir -p $dr/$submodel/Emon_${var}
  echo $cdo cmor,Emon,i=$it,mt=$mt,dr=$dr/$submodel/Emon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Emon,i=$it,mt=$mt,dr=$dr/$submodel/Emon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Emon 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: MPI-ESM1-2) LImon
cn='sbl'
for var in $cn; do
  { (if_requested $member $srfmod LImon $var $chunk || continue
  mkdir -p $dr/$submodel/LImon_${var}
  ifile=${sdir}/out_diag/LImon_${var}_$period.nc
  echo $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.LImon 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: MPI-ESM1-2) LImon
cn='snc'
find_file "$sdir" "*_land_mm_${period}*" ifile >> $err.find_file.LImon 2>&1
for var in $cn; do
  { (if_requested $member $srfmod LImon $var $chunk || continue
  mkdir -p $dr/$submodel/LImon_${var}
  echo $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.LImon 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: MPI-ESM1-2) LImon
cn='sftgif'
find_file "$sdir" "glac_${period}" ifile >> $err.find_file.LImon 2>&1
for var in $cn; do
  { (if_requested $member $srfmod LImon $var $chunk || continue
  mkdir -p $dr/$submodel/LImon_${var}
  echo $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,LImon,i=$it,mt=$mt,dr=$dr/$submodel/LImon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.LImon 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: MPI-ESM1-2) Lmon
cn='baresoilFrac c3PftFrac c4PftFrac cLitter cVeg fFire fVegLitter frivera gpp grassFrac landCoverFrac mrso npp rh shrubFrac treeFrac'
for var in $cn; do
  { (if_requested $member $srfmod Lmon $var $chunk || continue
  mkdir -p $dr/$submodel/Lmon_${var}
  ifile=${sdir}/out_diag/Lmon_${var}_$period.nc
  echo $cdo cmor,Lmon,i=$it,mt=$mt,dr=$dr/$submodel/Lmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Lmon,i=$it,mt=$mt,dr=$dr/$submodel/Lmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Lmon 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: MPI-ESM1-2) Lmon
[[ "${chunks[${submodel}dec]}" == *" ${period: -1} "* ]] && {
cn='rivo'
find_file "$sdir" "*_hd_higres_mon_${period}*" ifile >> $err.find_file.Lmon 2>&1
for var in $cn; do
  { (if_requested $member $srfmod Lmon $var $chunk || continue
  mkdir -p $dr/$submodel/Lmon_${var}
  echo $cdo cmor,Lmon,i=$it,mt=$mt,dr=$dr/$submodel/Lmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Lmon,i=$it,mt=$mt,dr=$dr/$submodel/Lmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Lmon 2>&1
done
}

#-- CMOR-rewrite for jsbach (ESM: MPI-ESM1-2) Lmon
cn='lai tran tsl'
find_file "$sdir" "*_land_mm_${period}*" ifile >> $err.find_file.Lmon 2>&1
for var in $cn; do
  { (if_requested $member $srfmod Lmon $var $chunk || continue
  mkdir -p $dr/$submodel/Lmon_${var}
  echo $cdo cmor,Lmon,i=$it,mt=$mt,dr=$dr/$submodel/Lmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,Lmon,i=$it,mt=$mt,dr=$dr/$submodel/Lmon_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.Lmon 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: MPI-ESM1-2) dec
cn='mrsofc rootd sftgif sftlf'
for var in $cn; do
  { (if_requested $member $srfmod dec $var $chunk '${srfmod_dec_chunk}' || continue
  mkdir -p $dr/$submodel/dec_${var}
  ifile=${sdir}/out_diag/dec_${var}_$period.nc
  echo $cdo cmor,dec,i=$it,mt=$mt,dr=$dr/$submodel/dec_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile
  $cdo cmor,dec,i=$it,mt=$mt,dr=$dr/$submodel/dec_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.dec 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: MPI-ESM1-2) fx
cn='areacellr'
for var in $cn; do
  { (if_requested $member $srfmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  ifile=${sdir}/out_diag/fx_$var.nc
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: MPI-ESM1-2) fx
cn='sftgif'
find_file "$sdir" "glac_${period}" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $srfmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: MPI-ESM1-2) fx
cn='mrsofc'
find_file "$sdir" "mrsofc_${period}.nc" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $srfmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: MPI-ESM1-2) fx
cn='rootd'
find_file "$sdir" "rootd_${period}.nc" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $srfmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

#-- CMOR-rewrite for jsbach (ESM: MPI-ESM1-2) fx
cn='sftlf'
find_file "$sdir" "sftlf_${period}.nc" ifile >> $err.find_file.fx 2>&1
for var in $cn; do
  { (if_requested $member $srfmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

