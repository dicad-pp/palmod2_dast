#
# experiment info:
#
_CONTROLLED_VOCABULARY_FILE=PalMod2_CV.json
_FORMULA_VAR_FILE=PalMod2_formula_terms.json
_AXIS_ENTRY_FILE=PalMod2_coordinate.json
CONVENTIONS="CF-1.7 PalMod-2.0"
TITLE="MPI-ESM1-2-1 output prepared for PalMod2"
_history_template="%s ; CMOR rewrote data to be consistent with <activity_id>, <Conventions> and CF standards."

EXPERIMENT_ID=transient-deglaciation-interactive
EXPERIMENT="synchronously coupled transient deglaciation with interactive ice sheets and solid earth"
#Specify VARIANT_LABEL explicitly or realization_index, physics_index, initialization_index and forcing_index:
VARIANT_LABEL=r1i1p1f2
REALIZATION_INDEX=1
INITIALIZATION_INDEX=1
PHYSICS_INDEX=1
FORCING_INDEX=2
VARIANT_INFO="The data are from a synchronously coupled model simulation of the last deglaciation with the MPI-ESM1.2.1-CR-mPISM-VILMA setup, hence, including interactive ice sheets and solid earth components. GHG forcing is after Koehler et al. (2017), Orbit after Berger (1978). Equivalent to p1 but forced with time-varying volcanic forcing (Schindlbeck-Belo et al., 2024)."
COMMENT="Earth viscosity configuration for the solid earth model VILMA. The vertical viscosity structure is piecewise constant for the following specified layers of the earth: (1) 0 to 3.480000e+06 m from earth core: 1.000000e+19 Pa s. (2) 3.480000e+06 to 5.701000e+06 m from earth core: 1.000000e+22 Pa s. (3) 5.701000e+06 to 6.291000e+06 m from earth core: 4.000000e+20 Pa s. (4) 6.291000e+06 to 6.371000e+06 m from earth core: 1.000000e+30 Pa s."
ACTIVITY_ID=PalMod2
#MIP_ERA=PalMod2                   #ONLY CMIP6
PROJECT_ID=PalMod2
REQUIRED_TIME_UNITS="days since 1-1-1 00:00:00"
#FURTHER_INFO_URL="https://furtherinfo.es-doc.org/<mip_era>.<institution_id>.<source_id>.<experiment_id>.<sub_experiment_id>.<variant_label>
#Branch Info: If needed use the following 2 attributes to define the branch time
#  in Parent_Time_Units and Required_Time_Units (=child)
#  else use BRANCH_DATES to give the dates in YYYYMMDD,YYYYMMDD format
#BRANCH_TIME_IN_PARENT=
#BRANCH_TIME_IN_CHILD=
#BRANCH_DATES=19000101,18500101
#BRANCH_METHOD="standard"
#PARENT_MIP_ERA=none              #ONLY CMIP6
#PARENT_ACTIVITY_ID=none
#PARENT_EXPERIMENT=none
#cmip6-spinup-HR
#PARENT_EXPERIMENT_ID=none
#PARENT_SOURCE_ID=none
#PARENT_TIME_UNITS="days since 1850-1-1 00:00:00"
#PARENT_VARIANT_LABEL=none
SUB_EXPERIMENT_ID=none
SUB_EXPERIMENT=none
#
#model info:
#
SOURCE_ID=MPI-ESM1-2-1-CR
#MODEL_ID=MPI-ESM1-2-1-CR
REFERENCES="The paper is currently in open discussion under the following citation:\nMikolajewicz, U., Kapsch, M.-L., Schannwell, C., Six, K. D., Ziemen, F. A., Bagge, M., Baudouin, J.-P., Erokhina, O., Gayler, V., Klemann, V., Meccia, V. L., Mouchet, A., and Riddick, T.: Deglaciation and abrupt events in a coupled comprehensive atmosphere–ocean–ice sheet–solid earth model, Clim. Past Discuss. [preprint], https://doi.org/10.5194/cp-2024-55, in review, 2024"
SOURCE="MPI-ESM1.2.1-CR (2022): \naerosol: none, prescribed Kinne (2010)\natmos: ECHAM6.3 (spectral T31; 96 x 48 longitude/latitude; 31 levels; top level 10 hPa)\natmosChem: none, prescribed\nland: JSBACH3.20, River Transport Model\nlandIce: none / mPISM 0.7 (10 km x 10 km (NH), 15 km x 15 km (SH), 121 levels)\nocean: MPIOM1.63 (bipolar GR3.0, approximately 300km; 122 x 101 longitude/latitude; 40 levels; top grid cell 0-15 m)\nocnBgchem: none, prescribed\nseaIce: unnamed (thermodynamic (Semtner zero-layer) dynamic (Hibler 79) sea ice model)\nsolidLand: none / VILMA-1D"
SOURCE_TYPE="AOGCM ISM SLM"
#CALENDAR=proleptic_gregorian
#MAPPING_TABLE_DIR=cmip6_mapping_tables
#MAPPING_TABLE=MPIESM1_CMIP6.txt
#GRID_FILE_DIR=__GRID_FILE_DIR
#GRID_FILE=__GRID_FILE
GRID=gn
GRID_LABEL=gn
#NOMINAL_RESOLUTION read from external file
#For MPIOM/HAMOCC CR / GR30
#NOMINAL_RESOLUTION="500 km"
#For ECHAM6/JSBACH CR / T31
#NOMINAL_RESOLUTION="500 km"
CHAR_AXIS_TYPELICE="ice_free_land","ice_sheet","floating_ice","ice_free_sea"
CHAR_AXIS_VEGTYPE=bare_land,glacier,tropical_evergreen_trees,tropical_deciduous_trees,extra-tropical_evergreen_trees,extra-tropical_deciduous_trees,raingreen_shrubs,deciduous_shrubs,C3_grass,C4_grass,C3_pasture,C4_pasture,C3_crops,C4_crops
CHAR_AXIS_SOILPOOLS=acid-soluble_below_ground_leaf_litter,acid-soluble_below_ground_woody_litter,water-soluble_below_ground_leaf_litter,water-soluble_below_ground_woody_litter,ethanol-soluble_below_ground_leaf_litter,ethanol-soluble_below_ground_woody_litter,non-soluble_below_ground_leaf_litter,non-soluble_below_ground_woody_litter,humus_from_leaf_litter,humus_from_woody_litter
CHAR_AXIS_LANDUSE=primary_and_secondary_land,crops,pastures,urban
CHAR_AXIS_BASIN=global_ocean,atlantic_arctic_ocean,indian_pacific_ocean
CHAR_AXIS_OLINE=barents_opening,bering_strait,canadian_archipelago,denmark_strait,drake_passage,english_channel,pacific_equatorial_undercurrent,faroe_scotland_channel,florida_bahamas_strait,fram_strait,iceland_faroe_channel,indonesian_throughflow,mozambique_channel,taiwan_luzon_straits,windward_passage
CHAR_AXIS_SILINE=barents_opening,bering_strait,canadian_archipelago,fram_strait
#spectband for the 3 aeropt variables
CHAR_AXIS_SPECTBAND_aeroptbnd=1818182
CHAR_AXIS_SPECTBAND_aeroptbnd_BOUNDS=2262443,1600000
CHAR_AXIS_SPECTBAND_aeroptbnd_UNITS="m-1"
CHAR_AXIS_SPECTBAND_aerssabnd=1818182
CHAR_AXIS_SPECTBAND_aerssabnd_BOUNDS=2262443,1600000
CHAR_AXIS_SPECTBAND_aerssabnd_UNITS="m-1"
CHAR_AXIS_SPECTBAND_aerasymbnd=1818182
CHAR_AXIS_SPECTBAND_aerasymbnd_BOUNDS=2262443,1600000
CHAR_AXIS_SPECTBAND_aerasymbnd_UNITS="m-1"
# 2d-spectband for the albedos
CHAR_AXIS_SPECTBAND_albdirbnd=3225689,766690
CHAR_AXIS_SPECTBAND_albdirbnd_BOUNDS=5000000,1451379,1451379,82001
CHAR_AXIS_SPECTBAND_albdirbnd_UNITS="m-1"
CHAR_AXIS_SPECTBAND_albdiffbnd=3225689,766690
CHAR_AXIS_SPECTBAND_albdiffbnd_BOUNDS=5000000,1451379,1451379,82001
CHAR_AXIS_SPECTBAND_albdiffbnd_UNITS="m-1"
CHAR_AXIS_SPECTBAND_solbnd=1818182
CHAR_AXIS_SPECTBAND_solbnd_BOUNDS=2262443,1600000
CHAR_AXIS_SPECTBAND_solbnd_UNITS="m-1"
#T_AXIS="cmip" causes the time axis to be rewritten according to the requested time stamps
T_AXIS="cmip"
#OUTPUT_MODE="r"
OUTPUT_MODE="a"
MAX_SIZE=0
SAVE_CHUNK="n"
#For files without time axis, if one is requested:
#firsttimeval=1 (1850-01-02 00:00:00 wenn days since 1850-01-01 00:00:00)
FIRSTTIMEVAL=1
#
# institution and contact info:
#
# none of these can be set in the command line
#
DEFLATE_LEVEL=1
CONTACT=palmod2-mpi-esm@dkrz.de
#
# MPI-M
#
INSTITUTION_ID=MPI-M
INSTITUTION="Max Planck Institute for Meteorology, Hamburg 20146, Germany"
LICENSE="PalMod2 model data produced by MPI-M is licensed under a Creative Commons Attribution ShareAlike 4.0 International License (https://creativecommons.org/licenses). The data producers and data providers make no warranty, either express or implied, including, but not limited to, warranties of merchantability and fitness for a particular purpose. All liabilities arising from the supply of the information (including any liability arising in negligence) are excluded to the fullest extent permitted by law."
#MIP_TABLE_DIR="/pool/data/CMIP6/cmip6-cmor-tables/Tables/"
MIP_TABLE_DIR="/work/bm0021/PalMod2/cmor_tables/"
