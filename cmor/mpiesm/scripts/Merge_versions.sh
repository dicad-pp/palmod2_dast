#!/bin/bash

drs_root="/work/bk1192/k204212/palmod/transient-deglaciation-prescribed-*/archive/PalMod2"
#drs_root=/scratch/m/m300749/mpiesm-1.2.01-release/experiments/past2k_r1i1p1f1-LR/archive/CMIP6
new_version=v20220222

cd $drs_root
vars=( $(find $drs_root -maxdepth 7 -mindepth 7 -type d) )

## Check?:
## It should find folders à la
##  /work/mh0287/m300749/past2k_2_backup/archive/CMIP6/PMIP/MPI-M/MPI-ESM1-2-LR/past2k/r1i1p1f1/E3hrPt/va/gn
## or ./PMIP/MPI-M/MPI-ESM1-2-LR/past2k/r1i1p1f1/E3hrPt/va/gn
#
#echo ${vars[@]}
#exit 0



for var in ${vars[@]}; do
  cd $drs_root
  cd $var
  # merge
  #mkdir -p $new_version
  #mv -v v????????/*.nc $new_version
  # simple rename
  mv -v v???????? $new_version
done



# Delete empty directories afterwards:
# find $drs_root -type d -empty -delete
