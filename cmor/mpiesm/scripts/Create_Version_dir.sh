#!/bin/bash

#drs_root="/work/kd1292/ESGF_Buff/k204212/palmod/transient-deglaciation-prescribed-*/archive/PalMod2"
#drs_root="/work/kd1292/k204212/palmod_TK/transient-deglaciation-prescribed-*/archive/PalMod2"
#drs_root="/work/kd1292/k204212/palmod_TK/ssp*/archive/PalMod2"
drs_root="/work/kd1292/k204212/palmod_MK/transient-deglaciation-prescribed-*/archive/PalMod2"
new_version=v20230201

vars=( $(find $drs_root -maxdepth 7 -mindepth 7 -type d) )
verdirs=( $(find $drs_root -maxdepth 8 -mindepth 8 -type d -name "v????????") )

## Check?:
## It should find folders à la
##  /work/mh0287/m300749/past2k_2_backup/archive/CMIP6/PMIP/MPI-M/MPI-ESM1-2-LR/past2k/r1i1p1f1/E3hrPt/va/gn
## or ./PMIP/MPI-M/MPI-ESM1-2-LR/past2k/r1i1p1f1/E3hrPt/va/gn
#
#echo ${vars[@]}
#exit 0



for var in ${vars[@]}; do
  echo $var
  cd $var || exit 1
  # merge
  #mkdir -p $new_version
  #mv -v v????????/*.nc $new_version
  # simple rename
  mv -v v???????? $new_version
  # Create version dir
  #mkdir -p $new_version
  #for i in *.nc ; do mv $i $new_version ; done
done

echo ${#vars[@]} = ${#verdirs[@]} ?

# Delete empty directories afterwards:
# find $drs_root -type d -empty -delete
