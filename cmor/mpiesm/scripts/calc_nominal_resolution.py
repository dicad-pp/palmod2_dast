### Set up conda environment
# module load python3
# mamba create --prefix /work/bm0021/conda-envs-public/nominal_resolution -c pcmdi nominal_resolution xarray netCDF4
### Load conda environment
# source activate /work/bm0021/conda-envs-public/nominal_resolution
### Calculate cell area
# cdo gridarea in.nc out.nc
### Calculate nominal resolution
import nominal_resolution, xarray
ds=xarray.open_dataset("out2.nc")
# mpism
#a=nominal_resolution.mean_resolution(ds.cell_area.values.ravel(), ds.latitude_bnds.values.ravel().reshape((160000,4)), ds.longitude_bnds.values.ravel().reshape((160000,4)), convertdeg2rad=True, returnMaxDistance=False)
# mpiom/hamocc
a=nominal_resolution.mean_resolution(ds.cell_area.values.ravel(), ds.latitude_bnds.values.ravel().reshape((12322,4)), ds.longitude_bnds.values.ravel().reshape((12322,4)), convertdeg2rad=True, returnMaxDistance=False)
#>>> a
#20.951768033878334
nominal_resolution.nominal_resolution(a)
#'25 km' # mpism
#'500 km' # mpiom/hamocc
#a=nominal_resolution.mean_resolution(ds.cell_area.values.ravel(), ds.latitude_bnds.values.ravel().reshape((160000,4)), ds.longitude_bnds.values.ravel().reshape((160000,4)), convertdeg2rad=True, returnMaxDistance=True)
