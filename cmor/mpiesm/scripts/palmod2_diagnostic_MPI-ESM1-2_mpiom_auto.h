#-- Diagnostic for mpiom (ESM: MPI-ESM1-2) variable sftof / table Odec
# Editor Note: Calculated by div sao sao or div sos sos
{ (if_requested $member $ocemod Odec sftof $chunk '${ocemod_dec_chunk}' && {
  find_file -e            "$sdir" "weto_${period}.nc" ifile
  $cdo -f nc -O \
    timmean \
    -selname,weto $ifile \
    ${sdir}/out_diag/Odec_sftof_${period}.nc || echo ERROR
}; )&; }>$err.sftof.Odec 2>&1

#-- Diagnostic for mpiom (ESM: MPI-ESM1-2) variable volcello / table Ofx
# Editor Note: codes:87,197
{ (if_requested $member $ocemod Ofx volcello $chunk && {
  find_file -e            "$sdir" "mpiom_fx.nc" ifile1
  find_file -e            "$sdir" "Ofx_areacello.nc" ifile2
  $cdo -f nc -O \
    expr,'volcello=areacello*thkcello;' \
    -merge -selname,thkcello $ifile1 -selname,areacello $ifile2 \
    ${sdir}/out_diag/Ofx_volcello.nc || echo ERROR
}; )&; }>$err.volcello.Ofx 2>&1

#-- Diagnostic for mpiom (ESM: MPI-ESM1-2) variable hfx / table Omon
# Editor Note: codes:123,125,127
{ (if_requested $member $ocemod Omon hfx $chunk && {
  find_file -e            "$sdir" "*_mpiom_data_2d_mm_${period}*" ifile
  $cdo -f nc -O \
    expr,'hfx=oxhtradv+oxhtrdif+hfxba;' \
    $ifile ${sdir}/out_diag/Omon_hfx_$period.nc || echo ERROR
}; )&; }>$err.hfx.Omon 2>&1

#-- Diagnostic for mpiom (ESM: MPI-ESM1-2) variable hfy / table Omon
# Editor Note: codes:124,126,128
{ (if_requested $member $ocemod Omon hfy $chunk && {
  find_file -e            "$sdir" "*_mpiom_data_2d_mm_${period}*" ifile
  $cdo -f nc -O \
    expr,'hfy=oyhtradv+oyhtrdif+hfyba;' \
    $ifile ${sdir}/out_diag/Omon_hfy_$period.nc || echo ERROR
}; )&; }>$err.hfy.Omon 2>&1

#-- Diagnostic for mpiom (ESM: MPI-ESM1-2) variable volcello / table Omon
# Editor Note: codes:87,197
{ (if_requested $member $ocemod Omon volcello $chunk && {
  find_file -e            "$sdir" "*_mpiom_data_3d_mm_${period}*.nc" ifile1
  find_file -e            "$sdir" "Omon_areacello_${period}*.nc" ifile2
  $cdo -f nc -O \
    expr,'volcello=areacello*thkcello;' \
    -merge -selname,thkcello $ifile1 -selname,areacello $ifile2 \
    ${sdir}/out_diag/Omon_volcello_$period.nc || echo ERROR
}; )&; }>$err.volcello.Omon 2>&1

#-- Diagnostic for mpiom (ESM: MPI-ESM1-2) variable simass / table SImon
# Editor Note: codes:13
{ (if_requested $member $ocemod SImon simass $chunk && {
  find_file -e            "$sdir" "*_mpiom_data_2d_mm_${period}*" ifile
  $cdo -f nc -O \
    expr,'simass=0.9*sictho;' \
    $ifile ${sdir}/out_diag/SImon_simass_$period.nc || echo ERROR
}; )&; }>$err.simass.SImon 2>&1

#-- Diagnostic for mpiom (ESM: MPI-ESM1-2) variable sisnmass / table SImon
# Editor Note: codes:141
{ (if_requested $member $ocemod SImon sisnmass $chunk && {
  find_file -e            "$sdir" "*_mpiom_data_2d_mm_${period}*" ifile
  $cdo -f nc -O \
    expr,'sisnmass=sicsno*0.3;' \
    $ifile ${sdir}/out_diag/SImon_sisnmass_$period.nc || echo ERROR
}; )&; }>$err.sisnmass.SImon 2>&1

#-- Diagnostic for mpiom (ESM: MPI-ESM1-2) variable sisnthick / table SImon
# Editor Note: codes:15,141
{ (if_requested $member $ocemod SImon sisnthick $chunk && {
  find_file -e            "$sdir" "*_mpiom_data_2d_mm_${period}*" ifile
  $cdo -f nc -O \
    expr,'sisnthick=sicsno/sicomo;' \
    $ifile ${sdir}/out_diag/SImon_sisnthick_$period.nc || echo ERROR
}; )&; }>$err.sisnthick.SImon 2>&1

#-- Diagnostic for mpiom (ESM: MPI-ESM1-2) variable sithick / table SImon
# Editor Note: codes:13,15
{ (if_requested $member $ocemod SImon sithick $chunk && {
  find_file -e            "$sdir" "*_mpiom_data_2d_mm_${period}*" ifile
  $cdo -f nc -O \
    expr,'sithick=sictho/sicomo;' \
    $ifile ${sdir}/out_diag/SImon_sithick_$period.nc || echo ERROR
}; )&; }>$err.sithick.SImon 2>&1

