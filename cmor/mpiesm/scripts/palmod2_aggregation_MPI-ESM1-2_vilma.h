################################
# VILMA ########################
################################

# Not required - instead the gridarea of the direct VILMA output will be calculated in the
#  diagnostic script
#{ (if_requested $member $slmod fx areacellsl $chunk && {
#  # Create fx variable
#  $cdo setname,glac $input_file_glac_fx $sdir/out_aggr/SLfx_glac_${period_bk}.nc || echo ERROR
#}; )&; }>$err.fx.areacellsl 2>&1


{ (if_requested $member $slmod vilmadecfx input $chunk "$slmod_input_dec_chunk" && {

  # Create decadal "fx"-variables
  ifile_topo=$input_file_topo
  ifile_glac=$input_file_glac

  #-- orog
  $cdo setname,orog $input_file_topo $sdir/out_aggr/SLdec_orog_${period_bk}.nc || echo ERROR

  #-- glac
  $cdo setname,sftgif $input_file_glac $sdir/out_aggr/SLdec_sftgif_${period_bk}.nc || echo ERROR

}; )&; }>$err.vilmadecfx.input 2>&1


###########################################################################################################

wait

