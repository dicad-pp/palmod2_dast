#-- CMOR-rewrite for vilma (ESM: MPI-ESM1-2) SLdec
cn='orog sftgif'
for var in $cn; do
  { (if_requested $member $slmod SLdec $var $chunk '${slmod_input_dec_chunk}' || continue
  mkdir -p $dr/$submodel/SLdec_${var}
  find_file "$sdir" "SLdec_${var}_${period_bk}.nc" ifile >> $err.find_file.SLdec 2>&1
  echo $cdo cmor,SLdec,i=$it,mt=$mt,dr=$dr/$submodel/SLdec_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile
  $cdo cmor,SLdec,i=$it,mt=$mt,dr=$dr/$submodel/SLdec_${var},vd=$vd,cn=$var,ta=cmip,di=$di ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.SLdec 2>&1
done

#-- CMOR-rewrite for vilma (ESM: MPI-ESM1-2) SLdec
cn='rslc'
for var in $cn; do
  { (if_requested $member $slmod SLdec $var $chunk '${slmod_dec_chunk}' || continue
  mkdir -p $dr/$submodel/SLdec_${var}
  ifile=${sdir}/out_diag/SLdec_${var}_${period_bk}.nc
  echo $cdo cmor,SLdec,i=$it,mt=$mt,dr=$dr/$submodel/SLdec_${var},vd=$vd,cn=$var,ta=cmip,di=$disl ${cdochain-} $ifile
  $cdo cmor,SLdec,i=$it,mt=$mt,dr=$dr/$submodel/SLdec_${var},vd=$vd,cn=$var,ta=cmip,di=$disl ${cdochain-} $ifile || echo ERROR
  )&; }>>$err.$var.SLdec 2>&1
done

#-- CMOR-rewrite for vilma (ESM: MPI-ESM1-2) fx
cn='areacellsl'
for var in $cn; do
  { (if_requested $member $slmod fx $var $chunk || continue
  mkdir -p $dr/$submodel/fx_${var}
  ifile=${sdir}/out_diag/fx_${var}_${period_bk}.nc
  echo $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var,ta=cmip,di=$di $ifile
  $cdo cmor,fx,i=$it,mt=$mt,dr=$dr/$submodel/fx_${var},vd=$vd,cn=$var,ta=cmip,di=$di $ifile || echo ERROR
  )&; }>>$err.$var.fx 2>&1
done

